from setuptools import setup, find_packages

setup(
    name="Magnetic Oscillation Analyser",
    version="1.1.0",
    scripts=["Magnetic Oscillation Analyser.py"],
    packages=find_packages(),
    # dependencies
    install_requires=[
        'QtPy>=1.4.2', 'pyFFTW>=0.11.1', 'matplotlib>=2.2.2', 'numpy>=1.14.5', 'scipy>=1.3.0', 'packaging'
    ],
    python_requires='>3.6.0',

    # metadata
    author="Maxime Gidding",
    description="GUI tool for analysing magnetic oscillations",
    license="GPLv3",
    keywords="magnetic oscillation analysis qt physics",
)
