#!/usr/bin/env python

import sys
from qtpy.QtWidgets import QApplication
from UI.InitUI import Wizard
from packaging import version
import qtpy
import pyfftw
import numpy
import scipy
import matplotlib
matplotlib.use('QT5Agg')


def main():
	if version.parse(qtpy.__version__) < version.parse('1.4.2'):
		print("Warning: qtpy is outdated. Version 1.4.2 or higher is required. Try running 'pip3 install qtpy'")
	if version.parse(pyfftw.__version__) < version.parse('0.11.1'):
		print("Warning: pyfftw is outdated. Version 0.11.1 or higher is required. Try running 'pip3 install pyfftw'")
	if version.parse(numpy.__version__) < version.parse('1.14.5'):
		print("Warning: numpy is outdated. Version 1.14.5 or higher is required. Try running 'pip3 install numpy'")
	if version.parse(scipy.__version__) < version.parse('1.3.0'):
		print("Warning: scipy is outdated. Version 1.3.0 or higher is required. Try running 'pip3 install scipy'")
	if version.parse(matplotlib.__version__) < version.parse('2.2.2'):
		print("Warning: matplotlib is outdated. Version 2.2.2 or higher is required. Try running 'pip3 install matplotlib'")

	app = QApplication(sys.argv)
	window = Wizard()
	window.show()
	sys.exit(app.exec_())


if __name__ == "__main__":
	main()
