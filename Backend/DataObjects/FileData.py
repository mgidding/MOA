from re import findall
from qtpy import QtCore
from os.path import splitext
from typing import List, Tuple, Optional
from Backend.DataObjects.MOData import MOData, FileError
from numpy import genfromtxt, isnan, ndarray


class FileData:
	"""
	Load a file, used by UI.ChoicePage
	"""

	def __init__(self, file_location: str, mode: str = "temperature"):
		self.headers: Optional[List[str]] = None
		self.angle = 0.0
		self.temperature = 0.0
		self.magnetic_field_column = 0
		self.amplitude_column = 1

		self.__comment_rows = 0
		self.__file: Optional['ndarray'] = None
		self.__file_location = file_location

		# Try reading the file 5 times, increasing the number of comment lines to skip by 1 each loop:
		while self.__comment_rows <= 5:
			try:
				# noinspection PyTypeChecker
				self.__file = genfromtxt(file_location, max_rows=3, skip_header=self.__comment_rows, names=True)
				break
			except ValueError:
				self.__comment_rows += 1

		if self.__comment_rows > 5:
			# Exit. User should try removing long headers manually.
			raise FileError("Could not read file.")

		if len(self.__file) == 0 or len(self.__file[0]) == 1:
			raise FileError("File dimensions wrong.")

		# If the first header item is not a number, we guess the file has a header:
		try:
			float(self.__file.dtype.names[0])
			header = False
		except ValueError:
			header = True

		if header:
			# Extract the header contents to a separate file as headers mess up array dtype.
			self.headers = list(self.__file.dtype.names)

			# Add column numbering:
			for _ in range(len(self.headers)):
				self.headers[_] = str(_ + 1) + " - " + self.headers[_]

		# Reload the file so we have the proper dtype
		try:
			self.__file = genfromtxt(file_location, max_rows=10, skip_header=self.__comment_rows)
		except ValueError:
			raise FileError("Undetermined error.")

		if not header:
			# Simply put numbers as headers:
			self.headers = []
			for _ in range(len(self.__file[0])):
				self.headers += [str(_ + 1) + " - " + str(self.__file[0][_]) + ", " + str(self.__file[1][_]) + ", ..."]

		# Reload the file so we have the proper dtype
		self.__file = genfromtxt(file_location, max_rows=10, skip_header=self.__comment_rows)

		self.__file = self.__file[~isnan(self.__file).any(axis=1)]  # remove NaN rows

		self.name = file_location.rsplit('/', 1)[-1]  # strip path from file name
		self.name = splitext(self.name)[0]  # remove file extension
		if mode is "temperature":
			# regexp by James Southern
			try:
				self.temperature = float(
					(findall(r"(\d+([.p,]\d*)?) ?[kK]", self.name)[0][0]).replace("p", QtCore.QLocale().decimalPoint())
				)
			except IndexError:
				pass

	def preview(self) -> 'ndarray':
		return self.__file[:3]

	@property
	def columns(self) -> Tuple[int, int]:
		return self.magnetic_field_column, self.amplitude_column

	@columns.setter
	def columns(self, columns: Tuple[int, int]):
		self.magnetic_field_column = columns[1]
		self.amplitude_column = columns[2]

	def MOData(self) -> MOData:
		return MOData.from_file(
			self.__file_location, self.columns, True, self.name,
			self.temperature, self.angle, self.__comment_rows)
