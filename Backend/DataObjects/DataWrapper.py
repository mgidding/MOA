from Backend.DataObjects.MOData import MOData
from Backend.DataObjects.LKFParameters import LKFParameters
from typing import List


class DataWrapper:
	"""
	Wrapper that tracks a list of MOData objects and a list of lists of LKFParameter objects.

	Notes
	-----
	The index is updated globally so that both lists return the same index.
	This is done because a list of LKFParameter objects is always associated with one MOData object.
	"""

	def __init__(self):
		self.__index = 0
		self.mo_data_list: List[MOData] = []
		self.list_of_lkf_parameters_lists: List[List[LKFParameters]] = [[]]
		self.mode = "temperature"

# Get and set data:
	@property
	def index(self) -> int:
		"""
		Returns the index of both mo_data_list and list_of_lkf_parameters_lists.
		"""

		return self.__index

	@index.setter
	def index(self, index: int):
		"""
		Sets the index of both mo_data_list and list_of_lkf_parameters_lists.
		"""

		if index < 0:
			raise ValueError
		if index != 0 and index >= len(self.mo_data_list):
			raise ValueError

		self.__index = index

	@property
	def mo_data(self) -> MOData:
		"""
		Returns the MOData object in mo_data_list at index 'index'.

		Notes
		-----
		Identical to mo_data_list[index]. Implemented in this way to avoid having to specify index with every call.
		"""

		return self.mo_data_list[self.index]

	@mo_data.setter
	def mo_data(self, mo_data: MOData):
		"""
		Sets the MOData object in mo_data_list at index 'index'.

		Notes
		-----
		Identical to mo_data_list[index]. Implemented in this way to avoid having to specify index with every call.
		"""

		self.mo_data_list[self.index] = mo_data

	@property
	def lkf_parameters_list(self) -> List[LKFParameters]:
		"""
		Returns the list of LKFParameters objects in list_of_lkf_parameters_lists at index 'index'.

		Notes
		-----
		Identical to list_of_lkf_parameters_lists[index].
		Implemented in this way to avoid having to specify index with every call.
		"""

		return self.list_of_lkf_parameters_lists[self.index]

	@lkf_parameters_list.setter
	def lkf_parameters_list(self, lkf_parameters_list: List[LKFParameters]):
		"""
		Sets the list of LKFParameters objects in list_of_lkf_parameters_lists at index 'index'.

		Notes
		-----
		Identical to list_of_lkf_parameters_lists[index].
		Implemented in this way to avoid having to specify index with every call.
		"""

		self.list_of_lkf_parameters_lists[self.index] = lkf_parameters_list

	def sortMOData(self, mode: str = None):
		"""
		Sort MO Data by the requested specifier (temperature or angle).

		Notes
		-----
		"mode" is either "temperature" or "angle", optional.
		Resets 'index' to 0.
		"""

		if mode is None:
			mode = self.mode
		elif mode not in ["temperature", "angle"]:
			raise ValueError("Mode not recognised")

		quantifier_list = []
		for modata in self.mo_data_list:
			quantifier = modata.temperature if mode == "temperature" else modata.angle
			quantifier_list += [(quantifier, modata)]

		quantifier_list = sorted(quantifier_list, key=lambda sorting_element: sorting_element[0])

		sorted_mo_data_list = [modata for _, modata in quantifier_list]

		self.mo_data_list = sorted_mo_data_list
		self.__index = 0
