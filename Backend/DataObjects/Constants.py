from numpy import pi as np_pi


class Constants:
	"""
	Constants manages physical constants used in the simulation. All units are in SI.

	Notes
	----------
	e: electron charge

	c: speed of light

	hbar: reduced Planck constant

	m_e: electron mass

	k_b: Boltzmann constant

	pi: ...pi!
	"""

	e = 1.60217646*10**-19
	c = 299792458
	hbar = 1.05457148*10**-34
	m_e = 9.10938188*10**-31
	k_b = 1.3806503*10**-23
	pi = np_pi
