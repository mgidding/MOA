from Backend.DataObjects.Constants import Constants
import Backend.DataObjects.MOData
from typing import Optional


class LKFParameters:
	"""
	LKFParameters models one set of LKF (Lifshitz-Kosevich Formula) parameters.

	Notes
	-----
	frequency: The dHvA frequency, default is 100 [T].

	extremal_area_type: The extremal area type of the magnetic oscillation, '3D Maximal', '3D Minimal', or '2D'.
	Default is '3D Maximal'.

	max_harmonic: The maximum harmonic to use in LKF calculations. Default is '1'.

	amplitude_harmonic_dep: The power (x) with which the amplitude (A) depends on the harmonic (p), i.e. A ~ p^x.
	Default is -3/2.

	g_factor: The electron spin g-factor. Default is 2.0023.

	m_c: The cyclotron effective mass. Default is the free electron mass.

	T: The temperature. Default is 0[K].

	T_D: Dingle temperature. Default is 1[K].

	raw_amplitude: The raw (non-normalised) FFT amplitude of the magnetic oscillation. Defaults to 0.

	identifier: A string to identify the LKFParameter object with. Default is None.

	pocket_type: A string to identify the pocket type with. Default is None.

	H_min: The minimum magnetic field value of the range over which the magnetic oscillation exists.

	H_max: The maximum magnetic field value of the range over which the magnetic oscillation exists.

	Related values are updated consistently, e.g. if the frequency is updated then the extremal area is updated too.
	"""

	def __init__(
			self, frequency: float = 100, extremal_area_type: str = "3D Maximal", max_harmonic: int = 1,
			amplitude_harmonic_dep: float = -3/2, g_factor: float = 2.0023, m_c: float = None, temperature: float = 0,
			dingle_temperature: float = 1.0, raw_amplitude: float = 0, identifier: str = None, pocket_type: str = None,
			H_min: float = None, H_max: float = None, onset_field: float = None, active: bool = True, phase: float = 0,
			mo_data: 'Backend.DataObjects.MOData.MOData' = None
	):

		# initialising property variables:
		self.__dingle_temperature: Optional[float] = None
		self.__tau: Optional[float] = None
		self.__extremal_area: Optional[float] = None
		self.__dhva_freq: Optional[float] = None
		self.__extremal_area_type: Optional[str] = None

		# assigning variables:
		if m_c is None:
			self.m_c = Constants.m_e
		else:
			self.m_c = m_c
		self.frequency: float = frequency
		self.extremal_area_type: str = extremal_area_type
		self.identifier: str = identifier
		self.max_harmonic: int = max_harmonic
		self.amplitude_harmonic_dependence: float = amplitude_harmonic_dep
		self.g_factor: float = g_factor
		self.temperature: float = temperature
		self.dingle_temperature: float = dingle_temperature
		self.raw_amplitude: float = raw_amplitude
		self.pocket_type: str = pocket_type
		self.manually_typed: bool = True if pocket_type is not None else False
		self.H_min: float = H_min
		self.H_max: float = H_max
		self.onset_field: float = onset_field
		self.active: bool = active
		self.phase: float = phase
		self.mo_data: Optional[Backend.DataObjects.MOData.MOData] = mo_data

	@property
	def extremal_area(self) -> float:
		"""
		Returns the extremal area of the magnetic oscillation in [s/m^3].
		"""

		return self.__extremal_area

	@extremal_area.setter
	def extremal_area(self, extremal_area: float):
		"""
		Set the extremal area of the magnetic oscillation in [s/m^3].

		Notes
		-----
		Updates the dHvA frequency.
		"""

		self.__extremal_area = extremal_area
		self.__dhva_freq = ((extremal_area * Constants.c * Constants.hbar) / (2 * Constants.pi * Constants.e))

	@property
	def frequency(self) -> float:
		"""
		Returns the dHvA frequency, in [T].
		"""

		return self.__dhva_freq

	@frequency.setter
	def frequency(self, dhva_freq: float):
		"""
		Set the dHvA frequency, in [T].

		Notes
		-----
		Updates the extremal area.
		"""

		self.__dhva_freq = dhva_freq
		self.__extremal_area = ((dhva_freq * 2 * Constants.pi * Constants.e) / (Constants.c * Constants.hbar))

	@property
	def extremal_area_type(self) -> str:
		"""
		Returns the extremal area type of the magnetic oscillation.
		"""

		return self.__extremal_area_type

	@extremal_area_type.setter
	def extremal_area_type(self, extremal_area_type: str):
		"""
		Set the extremal area type of the magnetic oscillation.

		Notes
		-----
		Takes '3D maximal', '3D minimal', or '2D'.
		"""

		if extremal_area_type in ["3D Maximal", "3D Minimal", "2D"]:
			self.__extremal_area_type = extremal_area_type
		else:
			raise ValueError("Maximal area must be 3D Maximal, 3D Minimal or 2D")

	@property
	def dingle_temperature(self) -> float:
		"""
		Returns the Dingle temperature
		"""

		return self.__dingle_temperature

	@dingle_temperature.setter
	def dingle_temperature(self, dingle_temperature: float):
		"""
		Set the Dingle temperature.

		Notes
		-----
		Updates the relaxation time (tau).
		"""

		self.__dingle_temperature = dingle_temperature
		if dingle_temperature == 0:
			self.__tau = 4.3*10**17  # lifetime of the universe in seconds :D
		else:
			self.__tau = (Constants.hbar / (2 * Constants.pi * Constants.k_b * dingle_temperature))

	@property
	def relaxation_time(self) -> float:
		"""
		Returns the relaxation time (tau).
		"""

		return self.__tau

	@relaxation_time.setter
	def relaxation_time(self, tau: float):
		"""
		Set the relaxation time (tau).

		Notes
		-----
		Updates the Dingle temperature.
		"""

		if tau == 0:
			raise ValueError("Lifetime tau cannot be zero")
		self.__tau = tau
		self.__dingle_temperature = (Constants.hbar / (2 * Constants.pi * Constants.k_b * tau))
