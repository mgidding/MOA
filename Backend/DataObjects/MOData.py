import numpy as np
from qtpy import QtCore
import re
from scipy.interpolate import UnivariateSpline
from scipy.stats import binned_statistic
from scipy.signal import get_window, medfilt, savgol_filter
from Backend.DataObjects.LKFParameters import LKFParameters
from pyfftw import empty_aligned, export_wisdom, import_wisdom, FFTW
from pyfftw.builders import rfft
from pyfftw.interfaces.scipy_fftpack import rfftfreq
from scipy.fftpack import next_fast_len
from scipy.signal import hilbert
from pickle import dump, load
from pathlib import Path
from os.path import splitext
from typing import Union, Iterable, Tuple, List, Optional


class MOData:
	"""
	MOData takes a set of amplitudes and magnetic field (H) strengths as input and manages all further data analysis.
	MOData will interpolate the given arrays to get equally spaced data in 1/H,
		and can Fourier transform (FFT) the interpolated arrays.
	MOData can also identify and return magnetic oscillation peaks in the FFT spectrum.

	Notes
	----------
	amplitudes: A set of measured magnetic oscillation amplitudes.
	H_range: A set of magnetic field strengths over which amplitudes was measured.
	H_scaling: The scaling factor used for the raw magnetic field.
	univariate_spline_degree: The univariate spline kind. Can be an integer between 1 and 5.
	interpolation_derivative: Set the order of the derivative.
	fft_filter: Set the FFT filter, a value between 0 and 1 (relative to the maximum FFT signal) below which
		all FFT data is regarded as noise and set to 0.
	fft_window: Set the FFT window type, defaults to 'hann'. Options are 'boxcar', 'triang', 'blackman', 'hann',
		'bartlett', 'flattop', 'parzen', 'blackmanharris', 'nuttall', 'barthann', 'kaiser', 'gaussian', 'general_gaussian',
		'slepian', 'dpss', 'chebwin', 'tukey' and 'cosine'.
	fft_bin_size: Set the FFT bin size, default is to have as many bins as interpolation points.
	fft_optimal_length: Enable or disable rounding of bin size to nearest optimal length.
	mo_frequency_width: Set the frequency width of the MOs used by the MO detection algorithm.
	mo_min_relamp: Set the minimum amplitude that can still be considered a MO, relative to the maximum amplitude found.
	mo_harmonic_comp: Toggle whether harmonic compensation should be enabled in the MO detection algorithm.
	mo_max_harmonic: Set the maximum harmonic to consider when harmonic compensation is enabled.
	temperature: Set the temperature of the data set.
	angle: Set the angle of the data set.
	name: Set the name (identifier) of the data set.

	Can also load amplitudes and H_range from a file, using MOData.from_file()
	"""

	# Globals that allow for sharing of the FFT object, this speeds up FFTs:
	fft_object: FFTW = None
	fft_array_size: int = None
	padded_length: int = None
	warned_parameters: bool = False
	warned_data: bool = False

	def __init__(
		self, H_scaling: float, univariate_spline_degree: Union[str, int], interpolation_derivative: int, fft_filter: float,
		fft_window: str, fft_window_parameter_1: Optional[str], fft_window_parameter_2: Optional[str],
		fft_bin_size: Union[float, str], fft_optimal_length: bool, mo_frequency_width: float, mo_min_relamp: float,
		mo_harmonic_comp: bool, mo_max_harmonic: int, temperature: float, angle: float, name: str, mo_cap: int,
		interpolated_inverse_H_range: Union[np.ndarray, Iterable] = None,
		interpolated_amplitudes: Union[np.ndarray, Iterable] = None, amplitudes: Union[np.ndarray, Iterable] = None,
		H_range: Union[np.ndarray, Iterable] = None, lkf_parameters: str = None
	):

		if interpolated_inverse_H_range is None and H_range is None:
			raise ValueError("Specify at least interpolated_inverse_H_range or H_range.")
		if interpolated_amplitudes is None and amplitudes is None:
			raise ValueError("Specify at least interpolated_amplitudes or amplitudes")
		if interpolated_amplitudes is not None and interpolated_inverse_H_range is not None:
			self.__from_interpolated_data_flag = True
			if amplitudes is not None:
				if MOData.warned_parameters is False:
					print(
						"You have supplied both interpolated and raw data. "
						"The interpolated data will be used for FFTs and further analysis."
					)
					MOData.warned_parameters = True
		else:
			self.__from_interpolated_data_flag = False

		# initialising variables:
		self.__H_scaling: Optional[float] = None
		self.__interpolation_degree: Optional[int] = None
		self.__H_min_index: Optional[int] = None
		self.__H_max_index: Optional[int] = None
		self.__interpolation_inverse_H_range: Optional[np.ndarray] = None
		self.__interpolation_H_min: Optional[float] = None
		self.__interpolation_H_max: Optional[float] = None
		self.__interpolation_H_min_index: Optional[int] = None
		self.__interpolation_H_max_index: Optional[int] = None
		self.__interpolation_smoothing_type: Optional[str] = None
		self.__interpolation_spline_smoothing: Optional[float] = None
		self.__interpolation_medfilt_kernel_size: Optional[int] = None
		self.__interpolation_savgol_mode: Optional[str] = None
		self.__interpolation_savgol_deriv: Optional[int] = None
		self.__interpolation_savgol_poly_order: Optional[int] = None
		self.__interpolation_savgol_window_length: Optional[int] = None

		self.__fft_filter: Optional[float] = None
		self.__fft_window: Optional[Union[str, Tuple[str, float], Tuple[str, float, float]]] = None
		self.__fft_zeropad: Optional[int] = None
		self.__fft_freq_range: np.array = None
		self.__fft_optimal_length: Optional[bool] = None
		self.__fft_windowed_amplitudes: Optional[np.ndarray] = None
		self.__fft_bin_size: Optional[float] = None

		self.__oscillation_max_harmonic: Optional[int] = None
		self.__oscillation_min_freq: Optional[float] = None
		self.__oscillation_max_freq: Optional[float] = None
		self.__oscillation_cap: Optional[int] = None
		self.__oscillation_frequency_width: Optional[float] = None
		self.__oscillation_min_relative_amplitude: Optional[float] = None

		# initialising labels:
		self.temperature: float = temperature
		self.angle: float = angle
		self.name: str = name
		self.filtered_data_list: List['MOData'] = []
		self.lkf_parameters: Optional[LKFParameters] = lkf_parameters

		# initialising interpolation parameters:
		self.__interpolation_amplitudes = np.array(interpolated_amplitudes)
		self.interpolation_error: bool = False
		self.__amplitude_range: np.ndarray = amplitudes
		self.__H_range: np.ndarray = H_range
		self.__interpolation_derivative: int = interpolation_derivative
		if interpolated_inverse_H_range is None:
			self.__interpolation_steps: int = 10 * len(H_range)
		self.raw_magnetic_field_scaling_factor: float = H_scaling
		self.__interpolation_smoothing_type: str = "None"
		self.interpolation_univariate_spline_degree: Union[int, str] = univariate_spline_degree
		if interpolated_inverse_H_range is None:
			self.interpolation_magnetic_field_range(H_range[0], H_range[-1], self.__interpolation_steps)
		else:
			self.__interpolation_inverse_H_range = np.array(interpolated_inverse_H_range)
			self.interpolation_magnetic_field_range(1/interpolated_inverse_H_range[-1], 1/interpolated_inverse_H_range[0])
			self.__interpolation_steps = len(self.interpolation_inverse_magnetic_field)
		self.__interpolation_spline_smoothing = 1.0
		self.__interpolation_medfilt_kernel_size = 3
		self.__interpolation_savgol_window_length = 5
		self.__interpolation_savgol_poly_order = 3
		self.__interpolation_savgol_deriv = 0
		self.__interpolation_savgol_mode = "interp"

		# initialising fourier transform and oscillation analysis parameters:
		self.__fft_rerun: bool = True
		self.__oscillation_rerun: bool = True
		self.__padded_length: int = self.__interpolation_steps
		self.fft_window(fft_window, fft_window_parameter_1, fft_window_parameter_2)
		self.fft_filter: float = fft_filter
		self.fft_optimal_length: bool = fft_optimal_length
		self.fft_bin_size: float = fft_bin_size
		self.oscillation_frequency_width: float = mo_frequency_width
		self.oscillation_min_relative_amplitude: float = mo_min_relamp
		self.oscillation_max_harmonic: int = mo_max_harmonic
		self.oscillation_cap: int = mo_cap
		self.__set_fft_frequency_spacing()
		self.__oscillation_harmonic_compensation: bool = mo_harmonic_comp
		self.__fft_min_freq: float = 0
		self.__fft_max_freq: float = self.__fft_freq_range[-1]
		self.oscillation_frequency_limits("default", "default")
		self.__update_fft_limits()

		# Load existing wisdom:
		wisdom_file = Path('wisdom_file.pyfftw')
		if wisdom_file.is_file():
			with open('wisdom_file.pyfftw', 'rb') as file:
				import_wisdom(load(file))
		# If wisdom is not found, create a wisdom file:
		else:
			with open('wisdom_file.pyfftw', 'wb') as file:
				dump(export_wisdom(), file)

	@classmethod
	def from_file(
		cls, location: str, usecols: tuple = (0, 1), first_column_is_H: bool = True, name: str = "None",
		temperature: float = 0.0, angle: float = 0.0, skip_rows: int = 0
	):
		"""
		Create a MOData object from a file.

		Notes
		----------
		location: Location of the file on disk.
		usecols: Which file columns to read, starting at 0. usecols=(2, 3, 4) will read the 3rd, 4th and 5th columns.
			Default is to read the first two columns (e.g. usecols=(0, 1)).
		first_column_is_H:  If True, the first column as selected by usecols is taken to be the magnetic field values.
			If False, the first column is taken to be the amplitudes.
		skip_rows: Number of rows to skip at the start of the file.
		"""

		try:
			data = np.genfromtxt(location, usecols=usecols, skip_header=skip_rows, invalid_raise=False)
			data = data[~np.isnan(data).any(axis=1)]  # remove NaN rows
			data = data[np.where(data[:, 0] > 0)]  # remove rows where H=0
		except (np.AxisError, ValueError):
			raise FileError

		file_name = location.rsplit('/', 1)[-1]  # strip path from file name
		file_name = splitext(file_name)[0]  # remove file extension

		if name is None:
			name = file_name

		if temperature is None:
			# regex by James Southern
			try:
				temperature = float(
					(re.findall(r"(\d+([.p,]\d*)?) ?[kK]", file_name)[0][0]).replace("p", QtCore.QLocale().decimalPoint())
				)
			except IndexError:
				pass

		if first_column_is_H:
			H_range: np.ndarray = data[:, 0]
			amplitudes: np.ndarray = data[:, 1]
		else:
			amplitudes: np.ndarray = data[:, 0]
			H_range: np.ndarray = data[:, 1]

		# Remove duplicate H values so spline interpolation works
		H_range, indices, inverse = np.unique(
			H_range, return_index=True, return_inverse=True
		)
		averaged_amplitudes, _, __ = binned_statistic(
			x=inverse, values=amplitudes, statistic='mean', bins=inverse.max()+1
		)

		return cls.from_raw_data(averaged_amplitudes, H_range, name=name, temperature=temperature, angle=angle)

	@classmethod
	def from_raw_data(
		cls, amplitudes: Union[np.ndarray, Iterable], H_range: Union[np.ndarray, Iterable], H_scaling: float = 1.0,
		univariate_spline_degree: str = "cubic", interpolation_derivative: int = 1, fft_filter: float = 0.0,
		fft_window: str = 'hann', fft_window_parameter_1: Optional[float] = None,
		fft_window_parameter_2: Optional[float] = None, fft_bin_size: Union[float, str] = "default",
		fft_optimal_length: bool = True, mo_frequency_width: float = 10.0, mo_min_relamp: float = 0.01,
		mo_harmonic_comp: bool = False, mo_max_harmonic: int = 3, temperature: float = 0.0,
		angle: float = 0.0, name: str = "None", mo_cap: int = 10, lkf_parameters: Optional[LKFParameters] = None
	):
		return cls(
			amplitudes=amplitudes, H_range=H_range, H_scaling=H_scaling, univariate_spline_degree=univariate_spline_degree,
			interpolation_derivative=interpolation_derivative, fft_filter=fft_filter, fft_window=fft_window,
			fft_window_parameter_1=fft_window_parameter_1, fft_window_parameter_2=fft_window_parameter_2,
			fft_bin_size=fft_bin_size, fft_optimal_length=fft_optimal_length, mo_frequency_width=mo_frequency_width,
			mo_min_relamp=mo_min_relamp, mo_harmonic_comp=mo_harmonic_comp, mo_max_harmonic=mo_max_harmonic,
			temperature=temperature, angle=angle, name=name, mo_cap=mo_cap, lkf_parameters=lkf_parameters
		)

	@classmethod
	def from_interpolated_data(
		cls, interpolated_inverse_H_range: Union[np.ndarray, Iterable] = None,
		interpolated_amplitudes: Union[np.ndarray, Iterable] = None, fft_filter: float = 0.0,
		fft_window: str = 'hann', fft_window_parameter_1: Optional[float] = None,
		fft_window_parameter_2: Optional[float] = None, fft_bin_size: Union[float, str] = "default",
		fft_optimal_length: bool = True, mo_frequency_width: float = 10.0, mo_min_relamp: float = 0.01,
		mo_harmonic_comp: bool = False, mo_max_harmonic: int = 3, temperature: float = 0.0,
		angle: float = 0.0, name: str = "None", parent: 'MOData' = None, mo_cap: int = 10,
		lkf_parameters: Optional[LKFParameters] = None
	):

		if parent is None:
			return cls(
				interpolated_inverse_H_range=interpolated_inverse_H_range, interpolated_amplitudes=interpolated_amplitudes,
				H_scaling=1.0, univariate_spline_degree="cubic", interpolation_derivative=1, fft_filter=fft_filter,
				fft_window=fft_window, fft_window_parameter_1=fft_window_parameter_1, fft_window_parameter_2=fft_window_parameter_2,
				fft_bin_size=fft_bin_size, fft_optimal_length=fft_optimal_length,
				mo_frequency_width=mo_frequency_width, mo_min_relamp=mo_min_relamp, mo_harmonic_comp=mo_harmonic_comp,
				mo_max_harmonic=mo_max_harmonic, temperature=temperature, angle=angle, name=name, mo_cap=mo_cap,
				lkf_parameters=lkf_parameters
			)
		else:
			if MOData.warned_data is False:
				print("Warning: Using a parent MOData means all other parameters will be ignored.")
				MOData.warned_data = True
			interpolated_inverse_H_range = parent.interpolation_inverse_magnetic_field if \
				interpolated_inverse_H_range is None else interpolated_inverse_H_range
			return cls(
				interpolated_inverse_H_range=interpolated_inverse_H_range, interpolated_amplitudes=interpolated_amplitudes,
				H_scaling=parent.raw_magnetic_field_scaling_factor,
				univariate_spline_degree=parent.interpolation_univariate_spline_degree,
				interpolation_derivative=parent.interpolation_derivative, fft_filter=parent.fft_filter,
				fft_window=parent.fft_window(), fft_window_parameter_1=parent.fft_window_parameter_1,
				fft_window_parameter_2=parent.fft_window_parameter_2, fft_bin_size=parent.fft_bin_size,
				fft_optimal_length=parent.fft_optimal_length, mo_frequency_width=parent.oscillation_frequency_width,
				mo_min_relamp=parent.oscillation_min_relative_amplitude, mo_harmonic_comp=parent.oscillation_harmonic_compensation,
				mo_max_harmonic=parent.oscillation_max_harmonic, temperature=parent.temperature, angle=parent.angle,
				name=parent.name, H_range=parent.raw_full_magnetic_field, amplitudes=parent.raw_full_amplitudes,
				mo_cap=parent.oscillation_cap, lkf_parameters=parent.lkf_parameters
			)

	@property
	def raw_magnetic_field_scaling_factor(self) -> float:
		"""
		Returns the magnetic field scaling factor applied to the input data.
		"""

		return self.__H_scaling

	@raw_magnetic_field_scaling_factor.setter
	def raw_magnetic_field_scaling_factor(self, H_scaling: float):
		"""
		Sets the magnetic field scaling factor applied to the input data.
		"""

		if H_scaling == 0:
			raise ValueError("Zero scaling not allowed.")
		self.__H_scaling = H_scaling

	@property
	def raw_amplitudes(self) -> np.ndarray:
		"""
		Returns the raw (non-interpolated) amplitudes.
		"""

		if self.__amplitude_range is not None:
			return self.__amplitude_range[self.__H_min_index:self.__H_max_index]
		else:
			raise ValueError("MOData was created without providing raw amplitudes.")

	@property
	def raw_magnetic_field(self) -> np.ndarray:
		"""
		Returns the raw (non-interpolated) magnetic field values.
		"""

		if self.__H_range is not None:
			return self.__H_scaling * self.__H_range[self.__H_min_index:self.__H_max_index]
		else:
			raise ValueError("MOData was created without providing raw magnetic field values.")

	@property
	def raw_full_amplitudes(self) -> np.ndarray:
		"""
		Returns the full range of raw (non-interpolated) amplitudes.
		"""

		if self.__amplitude_range is not None:
			return self.__amplitude_range
		else:
			raise ValueError("MOData was created without providing raw amplitudes.")

	@property
	def raw_full_magnetic_field(self) -> np.ndarray:
		"""
		Returns the full range of raw (non-interpolated) magnetic field values.
		"""

		if self.__H_range is not None:
			return self.__H_scaling * self.__H_range
		else:
			raise ValueError("MOData was created without providing raw magnetic field values..")

	@property
	def interpolation_univariate_spline_degree(self) -> int:
		"""
		Returns the univariate spline degree as an integer.
		"""

		return self.__interpolation_degree

	@interpolation_univariate_spline_degree.setter
	def interpolation_univariate_spline_degree(self, degree: Union[str, int]):
		"""
		Set the univariate spline degree. This is the degree of the spline used by the interpolation function.

		Notes
		----------
		degree can be 'linear', 'quadratic', 'cubic', 'quartic', 'quintic' or an integer between 1 and 5.
		"""

		orders = {'linear': 1, 'quadratic': 2, 'cubic': 3, 'quartic': 4, 'quintic': 5}
		if degree in orders:
			self.__interpolation_degree = orders[degree]
		elif type(degree) is int:
			if 0 > degree <= 5:
				self.__interpolation_degree = degree
		else:
			raise ValueError("UnivariateSpline degree not recognised")
		self.__fft_rerun = True

	@property
	def interpolation_derivative(self) -> int:
		"""
		Returns the order of the derivative taken after the data interpolation.
		"""

		return self.__interpolation_derivative

	@interpolation_derivative.setter
	def interpolation_derivative(self, order: int):
		"""
		Set the order of the derivative taken after the data interpolation.
		"""

		self.__interpolation_derivative = order
		self.__fft_rerun = True

	@property
	def interpolation_steps(self) -> int:
		"""
		Set the number of steps to be used by the interpolation.
		"""

		return self.__interpolation_steps

	@interpolation_steps.setter
	def interpolation_steps(self, steps: int):
		"""
		Set the number of steps to be used by the interpolation.
		"""

		self.interpolation_magnetic_field_range(self.__interpolation_H_min, self.__interpolation_H_max, steps)

	def interpolation_magnetic_field_range(
		self, H_min: Union[float, str] = None, H_max: Union[float, str] = None, steps: int = None
	) -> Tuple[float, float]:
		"""
		Set and returns the range over which the interpolation is performed.
		Optionally sets the number of steps to be used by the interpolation.
		"""

		if H_min is None and H_max is None and steps is None:
			return self.__interpolation_H_min, self.__interpolation_H_max

		if self.__from_interpolated_data_flag:
			if steps is not None:
				print("Cannot change (interpolation) steps when MOData object is generated from interpolated data.")
			if H_min is not None:
				H_min = 1/self.__interpolation_inverse_H_range[-1] if H_min == "default" else H_min
				self.__interpolation_H_max_index = (np.abs(self.__interpolation_inverse_H_range - 1/H_min)).argmin()
				self.__interpolation_H_min = H_min
			if H_max is not None:
				H_max = 1/self.__interpolation_inverse_H_range[0] if H_max == "default" else H_max
				self.__interpolation_H_min_index = (np.abs(self.__interpolation_inverse_H_range - 1/H_max)).argmin()
				self.__interpolation_H_max = H_max
			self.__interpolation_steps = len(
				self.__interpolation_inverse_H_range[self.__interpolation_H_min_index:self.__interpolation_H_max_index]
			)
			# when the interpolation range is changed, we must recalculate the FFT range and reset the FFT limits
			if self.__fft_freq_range is not None:
				self.__set_fft_frequency_spacing()
				self.fft_frequency_limits("default", "default")
			self.__fft_rerun = True
			return self.__interpolation_H_min, self.__interpolation_H_max

		if H_min is None:
			H_min = self.__interpolation_H_min
		if H_max is None:
			H_max = self.__interpolation_H_max

		scaled_H_range = self.raw_magnetic_field_scaling_factor * self.__H_range

		if H_min == "default" or H_min < scaled_H_range[0]:
			H_min = scaled_H_range[0]
		if H_max == "default" or H_max > scaled_H_range[-1]:
			H_max = scaled_H_range[-1]

		H_min_index = (np.abs(scaled_H_range - H_min)).argmin()
		H_max_index = (np.abs(scaled_H_range - H_max)).argmin()

		if H_max_index - H_min_index < self.interpolation_univariate_spline_degree:
			print("Chosen H range is too small to interpolate!")
			self.interpolation_error = True
		else:
			self.interpolation_error = False

		self.__H_min_index = H_min_index
		self.__H_max_index = H_max_index

		self.__interpolation_H_min = H_min
		self.__interpolation_H_max = H_max
		if steps is not None:
			self.__interpolation_steps = steps

		H_min = scaled_H_range[self.__H_min_index]
		H_max = scaled_H_range[self.__H_max_index]
		if H_min < 0 < H_max:
			H_min = 0
		self.__interpolation_inverse_H_range = np.linspace(1 / H_max, 1 / H_min, self.__interpolation_steps)

		# when the interpolation range is changed, we must recalculate the FFT range and reset the FFT limits
		if self.__fft_freq_range is not None:
			self.__set_fft_frequency_spacing()
			self.fft_frequency_limits("default", "default")
		self.__fft_rerun = True

		return self.__interpolation_H_min, self.__interpolation_H_max

	@property
	def interpolation_inverse_magnetic_field(self) -> np.ndarray:
		"""
		Returns the interpolated magnetic field values.
		"""

		if self.__from_interpolated_data_flag:
			return self.__interpolation_inverse_H_range[self.__interpolation_H_min_index:self.__interpolation_H_max_index]
		else:
			return self.__interpolation_inverse_H_range

	@property
	def interpolation_amplitudes(self) -> np.ndarray:
		"""
		Returns the interpolated amplitudes.
		"""

		if self.__from_interpolated_data_flag:
			return self.__interpolation_amplitudes[self.__interpolation_H_min_index:self.__interpolation_H_max_index]

		# recalculate the interpolation function whenever the interpolated data is requested.
		self.__set_interpolation_function()

		if self.__interpolation_derivative == 0:
			interpolation_amplitudes = self.__interpolation_function(self.__interpolation_inverse_H_range)
		else:
			if self.__interpolation_derivative > self.__interpolation_degree:
				order = self.__interpolation_degree
			else:
				order = self.__interpolation_derivative
			interpolation_amplitudes = self.__interpolation_function.derivative(order)(self.__interpolation_inverse_H_range)

		if self.__interpolation_smoothing_type == "medfilt":
			interpolation_amplitudes = medfilt(interpolation_amplitudes, self.__interpolation_medfilt_kernel_size)

		if self.__interpolation_smoothing_type == "savgol":
			interpolation_amplitudes = savgol_filter(
				interpolation_amplitudes, self.__interpolation_savgol_window_length, self.__interpolation_savgol_poly_order,
				deriv=self.__interpolation_savgol_deriv, mode=self.__interpolation_savgol_mode
			)

		return interpolation_amplitudes

	def interpolation_smoothing(
		self, smoothing_type: str = None, spline_smoothing: float = None, medfilt_kernel_size: int = None,
		savgol_window_length: int = None, savgol_poly_order: int = None, savgol_deriv: int = None, savgol_mode: str = None
	) -> Union[Tuple, str]:
		"""
		Sets and returns the interpolation smoothing type and options.
		Allowed smoothing types are None, spline, medfilt, savgol. Default smoothing type is None.
		Spline smoothing uses the spline_smoothing option, with default value 1.0.
		Medfilt smoothing uses the medfilt_kernel_size option, with default value 3.
		Savgol smoothing uses the savgol_window_length option (default value 5),
		the savgol_poly_order option (default value 3),
		the savgol_deriv option (default value 0)
		and the savgol_mode option which takes the values interp (default), mirror, nearest, wrap.
		"""

		if smoothing_type is None:
			smoothing_type = self.__interpolation_smoothing_type
		elif smoothing_type in ["None", "none", "spline", "medfilt", "savgol"]:
			self.__interpolation_smoothing_type = smoothing_type
		else:
			raise ValueError("smoothing_type not recognised")

		if smoothing_type in ["None", "none"]:
			self.__interpolation_smoothing_type = "none"
			return self.__interpolation_smoothing_type

		if smoothing_type == "spline":
			if spline_smoothing is not None:
				self.__interpolation_spline_smoothing = spline_smoothing
			return self.__interpolation_smoothing_type, self.__interpolation_spline_smoothing

		if smoothing_type == "medfilt":
			if medfilt_kernel_size is not None:
				self.__interpolation_medfilt_kernel_size = medfilt_kernel_size
			return self.__interpolation_smoothing_type, self.__interpolation_medfilt_kernel_size

		if smoothing_type == "savgol":
			if savgol_mode is not None:
				self.__interpolation_savgol_mode = savgol_mode
			if savgol_deriv is not None:
				self.__interpolation_savgol_deriv = savgol_deriv
			if savgol_poly_order is not None:
				self.__interpolation_savgol_poly_order = savgol_poly_order
			if savgol_window_length is not None:
				self.__interpolation_savgol_window_length = savgol_window_length
			return (
				self.__interpolation_savgol_window_length, self.__interpolation_savgol_poly_order,
				self.__interpolation_savgol_deriv, self.__interpolation_savgol_mode
			)

	@property
	def interpolation_envelope(self) -> np.ndarray:
		"""
		Returns the envelope of the interpolated signal.
		"""

		return np.abs(hilbert(self.interpolation_amplitudes))

	@property
	def fft_filter(self) -> float:
		"""
		Returns the value between 0 and 1 (relative to the maximum FFT signal) below which
		all FFT data is regarded as noise and set to 0.
		"""

		return self.__fft_filter

	@fft_filter.setter
	def fft_filter(self, fft_filter: float):
		"""
		Set the FFT filter, a value between 0 and 1 (relative to the maximum FFT signal) below which
		all FFT data is regarded as noise and set to 0.
		"""

		if fft_filter is None:
			fft_filter = 0
		if 0 <= fft_filter < 1:
			try:
				if self.__fft_filter == fft_filter:
					return
			except AttributeError:
				pass
			self.__fft_filter = fft_filter
			self.__fft_rerun = True
		else:
			raise ValueError("Fourier frequency filter should be between 0 and 1")

	def fft_window(self, window: str = None, param1: float = None, param2: float = None) -> str:
		"""
		Set and return the FFT window type.

		Notes
		-----
		window takes the following types:
		boxcar, triang, blackman, hamming, hann, bartlett, flattop, parzen, bohman, blackmanharris, nuttall, barthann,
		kaiser, gaussian, general_gaussian, chebwin, tukey, cosine.

		param1 is used by the following window types:
		kaiser (beta), gaussian (standard deviation), chebwin (attenuation), tukey (taper fraction),
		general_cosine (weighing coefficients), general_hamming (window coefficient), general_gaussian (shape parameter).

		param2 is used by the following window types:
		general_gaussian (standard deviation)

		Window symmetry is always set to False, to generate a periodic window.
		For more information on window types, check the scipy.signal documentation.
		"""

		if window is not None:
			try:
				if self.__fft_window == window:
					return window
			except AttributeError:
				pass
			self.__fft_rerun = True

			windows = (
				'boxcar', 'triang', 'blackman', 'hamming', 'hann', 'bartlett', 'flattop', 'parzen', 'bohman',
				'blackmanharris', 'nuttall', 'barthann', 'kaiser', 'gaussian', 'general_gaussian', 'slepian',
				'dpss', 'chebwin', 'tukey', 'cosine')

			if window not in windows:
				raise ValueError("Window function not recognised")

			if window in [
				'kaiser', 'gaussian', 'slepian', 'dpss', 'chebwin',
				'tukey', 'general_cosine', 'general_hamming']:
				if param1 is None:
					raise ValueError("param1 must be specified for", window)
				self.__fft_window = (window, param1)
			elif window == 'general_gaussian':
				if param1 is None or param2 is None:
					raise ValueError("param1 and param2 must be specified for", window)
				self.__fft_window = (window, param1, param2)
			else:
				self.__fft_window = window

		if type(self.__fft_window) is str:
			return self.__fft_window
		else:
			return self.__fft_window[0]

	@property
	def fft_window_parameter_1(self) -> [float, None]:
		try:
			return self.__fft_window[1]
		except (TypeError, IndexError):
			return None

	@property
	def fft_window_parameter_2(self) -> [float, None]:
		try:
			return self.__fft_window[2]
		except (TypeError, IndexError):
			return None

	@property
	def fft_optimal_length(self) -> bool:
		"""
		Returns true if optimal length rounding is active.
		"""

		return self.__fft_optimal_length

	@fft_optimal_length.setter
	def fft_optimal_length(self, optimal_length: bool):
		"""
		Toggle rounding to the nearest optimal length when calculating zero padding.
		"""

		if type(optimal_length) is bool:
			self.__fft_optimal_length = optimal_length
		else:
			raise TypeError("Expected a bool type.")

	@property
	def fft_bin_size(self) -> float:
		"""
		Return the bin size used by the FFT.
		"""

		return self.fft_frequency_spacing

	@fft_bin_size.setter
	def fft_bin_size(self, bin_size: Union[float, str]):
		"""
		Set the bin size used by the FFT. If 'default' is supplied, set to default.

		Notes
		-----
		The default bin size is calculated as 1/(1/H_max - 1/H_min),
		"""

		if type(bin_size) is not str:
			default_spacing = (
				1 /
				(self.interpolation_inverse_magnetic_field[-1] - self.interpolation_inverse_magnetic_field[0]))

			if bin_size < default_spacing:
				self.__fft_zeropad = int(np.round(
					1 / bin_size * self.__interpolation_steps * default_spacing
					- self.__interpolation_steps))
			else:
				self.__fft_zeropad = 0
		elif bin_size == "default":
			self.__fft_zeropad = 0
		else:
			raise ValueError

		padded_length = self.__interpolation_steps + self.__fft_zeropad

		if self.__fft_optimal_length:
			padded_length = next_fast_len(padded_length)

		if self.__padded_length != padded_length:
			self.__fft_rerun = True
			self.__fft_bin_size = bin_size
			self.__padded_length = padded_length

	def fft_frequency_limits(
			self, fft_min: Union[str, float] = None, fft_max: Union[str, float] = None
	) -> Tuple[float, float]:
		"""
		Set the FFT frequency limits.

		Notes
		-----
		If fft_min and/or fft_max are set to "default", the values are reset to the min and/or max FFT values.
		"""

		if fft_min is not None:
			if fft_min == "default":
				self.__fft_min_freq = 0
			elif type(fft_min) is str:
				raise ValueError
			else:
				self.__fft_min_freq = fft_min

		if fft_max is not None:
			if fft_max == "default":
				self.__fft_max_freq = self.__fft_freq_range[-1]
			elif type(fft_max) is str:
				raise ValueError
			else:
				self.__fft_max_freq = fft_max

		if fft_min is not None or fft_max is not None:
			self.__update_fft_limits()

		try:
			return (
				self.oscillation_frequency_range[0],
				self.oscillation_frequency_range[-1]
			)
		except IndexError:
			print("Chosen magnetic field range too small!")
			self.interpolation_error = True
			return -1, -1

	@property
	def fft_full_amplitude_range(self) -> np.ndarray:
		"""
		Returns the full amplitude range of the FFT.
		"""

		self.__fft()
		return self.__fft_amplitude_range

	@property
	def fft_full_frequency_range(self) -> np.ndarray:
		"""
		Returns the full frequency range of the FFT.
		"""

		self.__fft()
		return self.__fft_freq_range

	@property
	def fft_amplitude_range(self) -> np.ndarray:
		"""
		Returns the amplitude range of the FFT, limited by the frequencies set with fft_frequency_limits().
		"""

		self.__fft()
		return self.__fft_amplitude_range[self.__fft_min_freq_index:self.__fft_max_freq_index]

	@property
	def fft_phase_range(self) -> np.ndarray:
		"""
		Returns the phase range of the FFT, limited by the frequencies set with fft_frequency_limits().
		"""

		self.__fft()
		return self.__fft_phase_range[self.__fft_min_freq_index:self.__fft_max_freq_index]

	@property
	def fft_frequency_range(self) -> np.ndarray:
		"""
		Returns the frequency range of the FFT, limited by the frequencies set with fft_frequency_limits().
		"""

		self.__fft()
		return self.__fft_freq_range[self.__fft_min_freq_index:self.__fft_max_freq_index]

	@property
	def fft_frequency_spacing(self) -> float:
		"""
		Returns the FFT frequency spacing.
		"""

		self.__fft()
		return self.__fft_freq_range[1]

	@property
	def oscillation_harmonic_compensation(self) -> bool:
		"""
		Returns a boolean indicating whether harmonic compensation is enabled or disabled.

		Notes
		-----
		Harmonic compensation will make the MO finding algorithm ignore higher harmonics of a base-frequency MO.
		The width in which a MO is considered to be a higher harmonic is set by setOscillationFrequencyWidth().
		"""

		return self.__oscillation_harmonic_compensation

	@oscillation_harmonic_compensation.setter
	def oscillation_harmonic_compensation(self, mo_harmonic_comp: bool):
		"""
		Set harmonic compensation to True or False.

		Notes
		-----
		Harmonic compensation will make the MO finding algorithm ignore higher harmonics of a base-frequency MO.
		The width in which a MO is considered to be a higher harmonic is set by setOscillationFrequencyWidth().
		"""

		if type(mo_harmonic_comp) is bool:
			self.__oscillation_harmonic_compensation = mo_harmonic_comp
		else:
			raise TypeError("Expected a bool type.")

	@property
	def oscillation_frequency_width(self) -> float:
		"""
		Returns the oscillation frequency width for the harmonic compensation routine.
		"""

		return self.__oscillation_frequency_width

	@oscillation_frequency_width.setter
	def oscillation_frequency_width(self, mo_frequency_width: float):
		"""
		Set the oscillation frequency width for the harmonic compensation routine.
		"""

		try:
			if self.__oscillation_frequency_width == mo_frequency_width:
				return
		except AttributeError:
				pass
		self.__oscillation_frequency_width = mo_frequency_width
		self.__oscillation_rerun = True

	@property
	def oscillation_min_relative_amplitude(self) -> float:
		"""
		Returns the minimum amplitude that can still be considered a MO,
		relative to the maximum amplitude found by the MO detection algorithm.
		"""

		return self.__oscillation_min_relative_amplitude

	@oscillation_min_relative_amplitude.setter
	def oscillation_min_relative_amplitude(self, mo_min_relamp: float):
		"""
		Set the minimum amplitude that can still be considered a MO,
		relative to the maximum amplitude found by the MO detection algorithm.
		"""

		try:
			if self.__oscillation_min_relative_amplitude == mo_min_relamp:
				return
		except AttributeError:
			pass
		self.__oscillation_min_relative_amplitude = mo_min_relamp
		self.__oscillation_rerun = True

	@property
	def oscillation_max_harmonic(self) -> int:
		"""
		Returns the maximum harmonic used by the MO analysis when harmonic compensation is enabled.
		"""

		return self.__oscillation_max_harmonic

	@oscillation_max_harmonic.setter
	def oscillation_max_harmonic(self, mo_max_harmonic: int):
		"""
		Set the maximum harmonic used by the MO analysis when harmonic compensation is enabled
		"""

		try:
			if self.__oscillation_max_harmonic == mo_max_harmonic:
				return
		except AttributeError:
			pass
		self.__oscillation_max_harmonic = mo_max_harmonic
		self.__oscillation_rerun = True

	def oscillation_frequency_limits(
		self, oscillation_min: Union[float, str] = None, oscillation_max: Union[float, str] = None
	) -> Tuple[float, float]:
		"""
		Set and return the frequency limits within which oscillations should be detected by the MO detection algorithm.

		Notes
		-----
		If oscillation_min and/or oscillation_max are not supplied,
		the values are reset to the respective fft frequency limits.
		"""

		if oscillation_min is not None:
			if oscillation_min == "default":
				self.__oscillation_min_freq = self.__fft_min_freq
			elif type(oscillation_min) is str:
				raise ValueError
			else:
				self.__oscillation_min_freq = oscillation_min

		if oscillation_max is not None:
			if oscillation_max == "default":
				self.__oscillation_max_freq = self.__fft_max_freq
			elif type(oscillation_max) is str:
				raise ValueError
			else:
				self.__oscillation_max_freq = oscillation_max

		if oscillation_min is not None or oscillation_max is not None:
			self.__update_fft_limits()

		return (
			self.oscillation_frequency_range[0],
			self.oscillation_frequency_range[-1]
		)

	@property
	def oscillation_cap(self) -> int:
		"""
		Returns the number of oscillations that the OQ detection algorithm should look for.
		"""

		return self.__oscillation_cap

	@oscillation_cap.setter
	def oscillation_cap(self, n: int):
		"""
		Set the number of oscillations that the OQ detection algorithm should look for.

		Notes
		-----
		If more than n MOs are found, the n MOs with the highest amplitude are considered.
		"""

		self.__oscillation_cap = n
		self.__oscillation_rerun = True

	@property
	def oscillation_frequency_range(self) -> np.ndarray:
		"""
		Returns the frequency range within which oscillations should be detected by the MO detection algorithm.
		"""

		self.__oscillation_analysis()
		return self.__fft_freq_range[self.__oscillation_min_freq_index:self.__oscillation_max_freq_index]

	@property
	def oscillation_amplitude_range(self) -> np.ndarray:
		"""
		Returns the amplitude range within which oscillations should be detected by the MO detection algorithm.
		"""

		self.__oscillation_analysis()
		return self.__fft_amplitude_range[self.__oscillation_min_freq_index:self.__oscillation_max_freq_index]

	@property
	def oscillation_analysis(self) -> List[LKFParameters]:
		"""
		Returns a set of Parameter objects.

		Notes
		-----
		Returns a list of, at most, n Parameter objects where n was set by oscillation_cap.
		Each object is initialised with the default values of Parameter, except for the amplitude and frequency
		which are set as the FFT amplitude and frequency of the magnetic oscillation.
		"""

		self.__oscillation_analysis()
		return self.__magnetic_oscillations

	def save_raw_data(self, filename: str):
		"""
		Save the raw data to disk at location 'filename'.

		Notes
		-----
		Column 1 is the magnetic field values, column 2 the amplitudes. Tab separated.
		"""

		outdata = np.column_stack((
			self.raw_magnetic_field,
			self.raw_amplitudes))
		np.savetxt(
			filename, outdata, delimiter="	", fmt="%f	%f",
			header="Magnetic Field [T]  Amplitudes")

	def save_interpolated_data(self, filename: str):
		"""
		Save the interpolated data to disk at location 'filename'.

		Notes
		-----
		Column 1 is the inverse magnetic field values, column 2 the amplitudes. Tab separated.
		"""

		outdata = np.column_stack((
			self.interpolation_inverse_magnetic_field,
			self.interpolation_amplitudes))
		np.savetxt(
			filename, outdata, delimiter="	", fmt="%f	%f",
			header="Inverse Magnetic Field [1/T]    Interpolated Amplitudes")

	def save_fft_data(self, filename: str):
		"""
		Save the FFT data to disk at location 'filename'.

		Notes
		-----
		Column 1 is the FFT frequencies, column 2 the FFT amplitudes. Tab separated.
		"""

		outdata = np.column_stack((
				self.fft_frequency_range,
				self.fft_amplitude_range))
		np.savetxt(
			filename, outdata, delimiter="	", fmt="%f	%f",
			header="FFT Frequencies [1/T]   FFT Amplitudes")

	def __set_interpolation_function(self):
		"""
		Sets the interpolation function using SciPy's univariate spline library.

		Notes
		-----
		The x (inverse H range) and y (amplitude range) values and degree used by the interpolation function are set here too.
		This means this function needs to be called whenever x, y, or degree is updated.
		"""

		spline_smoothing = self.__interpolation_spline_smoothing if self.__interpolation_smoothing_type == "spline" else 0

		x = 1/(self.__H_scaling * self.__H_range[self.__H_min_index:self.__H_max_index + 1])
		y = self.__amplitude_range[self.__H_min_index:self.__H_max_index + 1]
		self.__interpolation_function = UnivariateSpline(x[::-1], y[::-1], k=self.__interpolation_degree, s=spline_smoothing)

	def __update_fft_limits(self):
		"""
		Updates the frequency limits used by the FFT and OQ finding algorithm.
		"""

		self.__fft_min_freq_index = (
			abs(self.__fft_freq_range - self.__fft_min_freq)).argmin()
		self.__fft_max_freq_index = (
			abs(self.__fft_freq_range - self.__fft_max_freq)).argmin()
		self.__oscillation_min_freq_index = (
			np.abs(self.__fft_freq_range - self.__oscillation_min_freq)).argmin()
		self.__oscillation_max_freq_index = (
			np.abs(self.__fft_freq_range - self.__oscillation_max_freq)).argmin()

	def __fft(self):
		"""Do FFT pre-processing, perform FFT on a separate thread and do post-processing.

		Notes
		-----
		If the FFT was successful save the data, else set failed.
		Will not not run unnecessarily to avoid computational overhead.
		"""

		if not self.__fft_rerun:
			return

		self.__initialise_fft()
		spectrum = MOData.fft_object(self.__fft_windowed_amplitudes)
		self.__fft_amplitude_range = abs(spectrum)
		self.__fft_phase_range = np.angle(spectrum)
		self.__fft_post_processing()
		self.__set_fft_frequency_spacing()
		self.__update_fft_limits()
		self.__fft_rerun = False

	def __initialise_fft(self):
		"""
		Set up the FFT object and the input array as needed.

		Notes
		-----
		This methods handles changing the FFT array and FFT object.
		Changing the number of interpolation steps requires changing the size of the array that will hold the FFT.
		Changing the bin size (i.e. zero padding) requires recreating the FFT object.
		"""

		# Explicitly set fftbins to True even though it is the default. This way window periodicity is guaranteed.
		window = get_window(self.__fft_window, self.__interpolation_steps, fftbins=True)

		# If there's a mismatch between the interpolation steps and the windowed amplitudes, we need to update
		# both the windowed amplitudes array length and the FFT object's array length.
		update = False
		if self.__fft_windowed_amplitudes is not None and len(self.__fft_windowed_amplitudes) != self.__interpolation_steps:
			update = True

		# If we changed the interpolation steps or do not yet have a windowed amplitudes array, we need to
		# (re)create the array.
		if MOData.fft_array_size != self.__interpolation_steps or self.__fft_windowed_amplitudes is None or update:
			MOData.fft_array_size = self.__interpolation_steps
			self.__fft_windowed_amplitudes = empty_aligned(self.__interpolation_steps, dtype='float64')
			update = True

		# If the padded length of the FFT object is different from the internal padded length, recreate the FFT object.
		if MOData.padded_length != self.__padded_length or update or self.__from_interpolated_data_flag:
			MOData.padded_length = self.__padded_length
			MOData.fft_object = rfft(self.__fft_windowed_amplitudes, n=self.__padded_length)

		self.__fft_windowed_amplitudes[:] = window * self.interpolation_amplitudes

	def __set_fft_frequency_spacing(self):
		"""
		Sets the FFT frequency range.

		Notes
		-----
		Calculation depends on the interpolated inverse magnetic field values and interpolation steps.
		So whenever these are updated this function should be called.
		"""

		delta = (
			(self.interpolation_inverse_magnetic_field[-1] - self.interpolation_inverse_magnetic_field[0])
			/ self.__interpolation_steps)
		if self.__padded_length % 2 == 0:
			length = int(self.__padded_length/2 + 1)
		else:
			length = int((self.__padded_length + 1) / 2)
		self.__fft_freq_range = rfftfreq(length, d=delta)

	def __fft_post_processing(self):
		"""
		Apply the FFT filter that sets all values below the filter value * maximum amplitude to 0."""

		if self.__fft_filter > 0:
			fft_max = np.amax(self.__fft_amplitude_range)
			self.__fft_amplitude_range[
				self.__fft_amplitude_range < self.__fft_filter * fft_max] = 0

	def __oscillation_analysis(self):
		"""
		Run the MO analysis algorithm.
		"""

		if not self.__oscillation_rerun and not self.__fft_rerun:
			return

		self.__fft()

		# Create arrays that hold the FFT amplitude and frequency ranges with the correct cut-offs.
		fft_amplitudes = self.__fft_amplitude_range[
			self.__oscillation_min_freq_index:self.__oscillation_max_freq_index
		]
		freq_range = self.__fft_freq_range[
			self.__oscillation_min_freq_index:self.__oscillation_max_freq_index
		]
		fft_phases = self.__fft_phase_range[
			self.__oscillation_min_freq_index:self.__oscillation_max_freq_index
		]

		# Later on we assume that the fft_amplitudes list has at least 1 entry, so if it doesn't exit here.
		if len(fft_amplitudes) <= 1:
			self.__magnetic_oscillations = []
			return

		# Find all frequencies where the amplitude is greater than the
		# max amplitude times self.__mo_min_relamp (between 0 and 1).
		indices = np.where(
			fft_amplitudes > self.__oscillation_min_relative_amplitude * np.amax(fft_amplitudes))[0]

		# Create an array (max_oscillations) that will hold the MO frequencies, MO amplitudes, MO angle
		# and a boolean that tracks if we have corrected each amplitude already
		# when doing higher harmonic correction.
		max_oscillations = np.zeros(
			len(indices),
			dtype={'names': ['frequency', 'amplitude', 'phase', 'corrected'], 'formats': [np.float, np.float, np.float, np.bool]}
		)
		for _ in range(len(indices)):
			max_oscillations[_]['corrected'] = False

		# Identify which values are peaks by checking if both neighbouring amplitude values are smaller.
		# Write these values to max_amplitudes.
		found_values = 0
		for i in indices:
			if i == 0:
				if fft_amplitudes[i+1] > fft_amplitudes[i]:
					continue
			elif i == len(fft_amplitudes)-1:
				if fft_amplitudes[i-1] > fft_amplitudes[i]:
					continue
			elif (
				fft_amplitudes[i-1] > fft_amplitudes[i]
				or fft_amplitudes[i+1] > fft_amplitudes[i]):
				continue
			max_oscillations[found_values]['frequency'] = freq_range[i]
			max_oscillations[found_values]['amplitude'] = fft_amplitudes[i]
			max_oscillations[found_values]['phase'] = fft_phases[i]
			found_values += 1
		max_oscillations = max_oscillations[:found_values]
		max_oscillations.sort(order='frequency', axis=0)
		max_oscillations = max_oscillations[::-1]

		# Check if a higher harmonic (p>1) of one MO (e.g. F=5)
		# overlaps a base harmonic (p=1) of another MO (e.g. F=10).
		# If it does, reduce its amplitude according to the LKF.
		power = (-3/2+self.__interpolation_derivative)
		if self.__oscillation_harmonic_compensation:
			for _ in range(found_values):
				# Check if the amplitude was already corrected for.
				if max_oscillations[_]['corrected']:
					continue
				for __ in range(_+1, found_values):
					for p in range(1, self.__oscillation_max_harmonic + 1):
						if np.isclose(
							p*max_oscillations[_]['frequency'], max_oscillations[__]['frequency'],
							atol=self.__oscillation_frequency_width):
							# Correct the amplitude.
							max_oscillations[__]['amplitude'] -= p**power*max_oscillations[_]['amplitude']
							if max_oscillations[__]['amplitude'] < 0:
								# Negative amplitudes aren't physical, so set to 0.
								max_oscillations[__]['amplitude'] = 0
							max_oscillations[__]['corrected'] = True

		# Sort the array by amplitude.
		max_oscillations.sort(order='amplitude', axis=0)
		max_oscillations = max_oscillations[::-1]

		# Now consider only the n largest amplitudes found.
		if found_values > self.__oscillation_cap:
			max_oscillations = max_oscillations[:self.__oscillation_cap]

		# Generate a Parameters object for each oscillation.
		magnetic_oscillations = []

		for i in range(len(max_oscillations)):
			if max_oscillations[i]['amplitude'] > 0:
				magnetic_oscillations += [
					LKFParameters(
						frequency=max_oscillations[i]['frequency'], raw_amplitude=max_oscillations[i]['amplitude'],
						temperature=self.temperature, H_min=self.interpolation_magnetic_field_range()[0],
						H_max=self.interpolation_magnetic_field_range()[1], phase=max_oscillations[i]['phase'], mo_data=self
					)
				]

		self.__magnetic_oscillations = magnetic_oscillations
		self.__oscillation_rerun = False


class FileError(Exception):
	"""Custom error to indicate file loading has failed."""
	def __init__(self, message=None):
		super().__init__(message)
