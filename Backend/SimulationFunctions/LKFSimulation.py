from numpy import sin, cos, sinh, exp, zeros, linspace, append
from numpy.random import normal
from Backend.DataObjects.MOData import MOData
from Backend.DataObjects.LKFParameters import LKFParameters
from Backend.DataObjects.Constants import Constants
from typing import List, Union


class OscillatingQuantity:
	"""
	Abstract parent class for all oscillating quantities, used to track the LKF factors and identifiers.
	"""

	def __init__(self, lkf_parameters: LKFParameters, identifier: str = "None"):
		# initialising variables:
		self.identifier: str = identifier

		# assigning variables:
		self.params = lkf_parameters

	def spinFactor(self, harmonic: int) -> float:
		"""
		Return the LKF spin factor for the given harmonic.
		"""

		return cos((harmonic * Constants.pi * self.params.g_factor * self.params.m_c) / (2*Constants.m_e))

	def temperatureFactor(self, H: float, harmonic: int) -> float:
		"""
		Return the LKF temperature factor for the given magnetic field strength and harmonic.
		"""

		if self.params.temperature == 0:
			return 1.0  # lim_{x->0} x/sinh(x) = 1

		alpha = ((2 * Constants.pi**2 * Constants.k_b) / (Constants.e * Constants.hbar))

		X = ((harmonic * alpha * self.params.temperature * self.params.m_c) / H)

		return X/sinh(X)

	def dingleFactor(self, H: float, harmonic: int) -> float:
		"""
		Return the LKF dingle factor for the given magnetic field strength and harmonic.
		"""

		alpha = ((2 * Constants.pi**2 * Constants.k_b) / (Constants.e * Constants.hbar))

		return exp((-harmonic * alpha * self.params.m_c * self.params.dingle_temperature) / H)

	def amplitude(self, quantity: float) -> float:
		"""
		Return the LKF amplitude for a given value of the oscillating quantity.
		"""

		return self.preFactor(quantity) * self.calculateHarmonicSum(quantity)

	def harmonicTerm(self, quantity: float, harmonic: int) -> float:
		"""
		Return the LKF harmonic term for a given value of the oscillating quantity and the harmonic.

		Notes
		-----
		Must be overwritten in child class.
		"""

		raise NotImplementedError("Must override harmonicTerm() in child class")

	def preFactor(self, quantity: float) -> float:
		"""
		Return the LKF pre-factor for a given value of the oscillating quantity.

		Notes
		-----
		Must be overwritten in child class.
		"""

		raise NotImplementedError("Must override preFactor() in child class")

	def calculateHarmonicSum(self, quantity: float) -> float:
		"""
		Return the LKF spin factor for the given harmonic.
		"""

		harmonic_sum = 0
		for harmonic in range(1, self.params.max_harmonic + 1):
			harmonic_sum += self.harmonicTerm(quantity, harmonic)
		return harmonic_sum


class Magnetisation(OscillatingQuantity):
	"""
	Abstract parent class used by all magnetisation type oscillating quantities.
	"""

	def magnetisation(self, H: float) -> float:
		"""
		Returns the magnetisation for a given magnetic field strength.
		"""

		return self.amplitude(H)

	def preFactor(self, H: float) -> float:
		"""
		Return the LKF pre-factor for a given value of the magnetic field.
		"""

		raise NotImplementedError("Must override preFactor() in child class")

	def harmonicTerm(self, H: float, harmonic: int) -> float:
		"""
		Return the LKF harmonic term for a given value of the magnetic field and the harmonic.

		Notes
		-----
		Must be overwritten in child class.
		"""

		raise NotImplementedError("Must override harmonicTerm() in child class")


class DimensionlessIdealMagnetisation(Magnetisation):
	"""
	Calculates the dimensionless magnetisation as function of the
	dimensionless magnetic field for the ideal case without phase smearing.
	"""

	def harmonicTerm(self, h: float, harmonic: int) -> float:
		"""
		Return the LKF harmonic term for a given value of the dimensionless magnetic field and the harmonic.
		"""

		if self.params.extremal_area_type == "3D Maximal":
			phase = -Constants.pi/4
		elif self.params.extremal_area_type == "3D Minimal":
			phase = Constants.pi/4
		else:
			phase = 0

		return harmonic**(-3/2) * sin(2 * Constants.pi * harmonic * ((1/h - 0.5) + phase))

	def preFactor(self, h: float) -> float:
		"""
		Return the LKF pre-factor for a given value of the dimensionless magnetic field.
		"""

		return -abs(h)**(1/2)


class MagneticOscillation(OscillatingQuantity):
	"""
	Calculates the amplitude of a magnetic oscillation as function of the magnetic field
	"""

	def harmonicTerm(self, H: float, harmonic: int) -> float:
		"""
		Return the LKF harmonic term for a given value of the magnetic field and the harmonic.
		"""

		if self.params.extremal_area_type == "3D Maximal":
			phase = -Constants.pi/4
		elif self.params.extremal_area_type == "3D Minimal":
			phase = Constants.pi/4
		else:
			phase = 0
		harmonic_term = (
			harmonic**self.params.amplitude_harmonic_dependence *
			cos(2 * Constants.pi * harmonic * (self.params.frequency / H - 0.5) + phase)
		)
		return (
				harmonic_term * self.spinFactor(harmonic) * self.temperatureFactor(H, harmonic) * self.dingleFactor(H, harmonic)
		)

	def preFactor(self, H: float) -> float:
		"""
		Return the LKF pre-factor for a given value of the magnetic field.
		"""

		return abs(H)**(1/2)


def LKFSimulation(
		lkf_parameters: Union[LKFParameters, List[LKFParameters]], H_min: float, H_max: float, steps: int,
		sim_type: OscillatingQuantity = MagneticOscillation, name: str = None, temperature: float = None,
		noise_scale: float = 0) -> MOData:
	"""

	Notes
	-----
	sim_type is the type of simulation to perform, should be an OscillatingQuantity class object.

	name is the name given to the resulting MOData object.

	temperature is the temperature given to the resulting MOData object.
	"""

	amplitudes = zeros(steps)
	if H_min > 0 and H_min > H_max:
		H_min, H_max = H_max, H_min

	if H_min == 0:
		H_min = H_max/steps

	if H_min < 0 < H_max:
		H_range = append(
			linspace(H_min, H_min/(steps/2), int(steps/2)),
			linspace(H_max/(steps/2), H_max, int(steps/2)))
	else:
		H_range = linspace(H_min, H_max, steps)

	lkf_parameters = [lkf_parameters] if type(lkf_parameters) is not list else lkf_parameters
	for parameters in lkf_parameters:
		_ = 0
		if callable(sim_type):
			simulation = sim_type(parameters)
			for H in H_range:
				amplitudes[_] += simulation.amplitude(H) + normal(0, noise_scale)
				_ += 1
		else:
			raise(TypeError("sim_type should be a callable OscillatingQuantity type."))
	return MOData.from_raw_data(amplitudes, H_range, name=name, temperature=temperature)
