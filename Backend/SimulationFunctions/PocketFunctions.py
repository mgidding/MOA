from numpy import isclose
from Backend.DataObjects.LKFParameters import LKFParameters
from typing import List


def generatePocketName(alpha_harmonic: int, beta_harmonic: int) -> str:
	"""
	Generate human-readable pocket name.

	Example
	-------
	>>> generatePocketName(1,2)
	'α+2β'
	"""

	alpha_string = "α"
	if abs(alpha_harmonic) != 1:
		alpha_string = str(abs(alpha_harmonic)) + alpha_string
	beta_string = "β"
	if abs(beta_harmonic) != 1:
		beta_string = str(abs(beta_harmonic)) + beta_string

	if alpha_harmonic == 0:
		if beta_harmonic != 0:
			# case alpha_harmonic = 0, beta_harmonic != 0
			pocket_string = beta_string
		else:
			# case alpha_harmonic = beta_harmonic = 0
			pocket_string = None
	else:
		if beta_harmonic != 0:
			if beta_harmonic > 0:
				if alpha_harmonic > 0:
					# case alpha_harmonic > 0, beta_harmonic > 0
					pocket_string = alpha_string + "+" + beta_string
				else:
					# case alpha_harmonic < 0, beta_harmonic > 0
					pocket_string = beta_string + "-" + alpha_string
			else:
				# case alpha_harmonic != 0, beta_harmonic < 0
				beta_string = "-" + beta_string
				pocket_string = alpha_string + beta_string
		else:
			# case alpha_harmonic != 0, beta_harmonic = 0
			pocket_string = alpha_string
	return pocket_string


def setPocketTypes(
		magnetic_oscillations: List[LKFParameters], alpha_index: int = None, beta_index: int = None, max_harmonic: int = 5,
		frequency_width: float = 10):
	"""
	Calculates and sets the pocket types for all LKFParameters objects in the supplied list.

	Notes
	----------
	alpha_index is the list index of alpha pocket magnetic oscillation.

	beta_index is the list index of beta pocket magnetic oscillation.

	max_harmonic gives the maximum harmonic to consider when calculating pocket names.

	frequency_width sets the frequency width in which a MO is considered
	to be a higher harmonic of the alpha or beta harmonics.

	The index of the alpha (and optionally beta) pocket(s) must be supplied
	in order to calculate the names of the other pockets.

	Unmatched pockets will be set to None.

	Supports linear combinations of the alpha and beta pockets.

	Will not touch pockets with names set manually by the user.
	"""

	if len(magnetic_oscillations) == 0:
		return

	if alpha_index is not None and alpha_index >= len(magnetic_oscillations):
		print("alpha_index out of range of magnetic_oscillation list. Exiting.")
		return

	if alpha_index is None:
		alpha_frequency = 0
		alpha_range = [0]
	else:
		alpha_frequency = magnetic_oscillations[alpha_index].frequency
		alpha_range = [0]
		for alpha in range(1, max_harmonic+1):
			alpha_range += [alpha, -alpha]

	if beta_index is None:
		beta_frequency = 0
		beta_range = [0]  # If beta was not supplied, only consider 0 beta.
	else:
		beta_frequency = magnetic_oscillations[beta_index].frequency
		if alpha_index is not None:
			beta_range = alpha_range
		else:
			beta_range = [0]
			for beta in reversed(range(1, max_harmonic + 1)):
				beta_range += [-beta, beta]

	# Iterate over _*alpha_frequency and __*beta_frequency so that each combination of alpha and beta is tested for with
	# each magnetic_oscillation.
	for magnetic_oscillation in magnetic_oscillations:
		# Skip over manually named MOs
		if magnetic_oscillation.manually_typed:
			continue

		# Reset MO name:
		magnetic_oscillation.pocket_type = None
		magnetic_oscillation.manually_typed = False

		frequency = magnetic_oscillation.frequency
		named = False
		for _ in beta_range:
			if named:
				break
			for __ in alpha_range:
				if isclose(frequency, __*alpha_frequency + _*beta_frequency, atol=frequency_width):
					magnetic_oscillation.pocket_type = generatePocketName(__, _)
					magnetic_oscillation.manually_typed = False
					named = True
					break


def resetPocketTypes(
		magnetic_oscillation_list_of_lists: List[List[LKFParameters]], ignore_list: List[int] = None, force: bool = False):
	"""
	Reset the pocket types of the LKFParameters objects in the supplied list of lists.

	Notes
	----------
	ignore_list gives the list of magnetic_oscillation_list indices that should be ignored.

	When force is set to true, overwrite all manually set names too.
	"""

	if ignore_list is None:
		ignore_list = []

	_ = 0
	for magnetic_oscillation_list in magnetic_oscillation_list_of_lists:
		if _ in ignore_list:
			continue
		for magnetic_oscillation in magnetic_oscillation_list:
			if (not magnetic_oscillation.manually_typed) or force:
				magnetic_oscillation.pocket_type = None
				magnetic_oscillation.manually_typed = False
				magnetic_oscillation.active = False
		_ += 1


def applyPocketTypes(
		magnetic_oscillations_list_of_lists: List[List[LKFParameters]], index: int,
		frequency_width: float = 10, max_harmonic: int = 5
):
	"""
	Apply pocket types of one list to the LKFParameters objects in the other lists in the supplied list of lists.

	Notes
	----------
	index sets the index of the list to use as basis for calculating the other lists' pocket types.

	frequency_width sets the frequency width in which a MO is considered
	to be a higher harmonic of the alpha or beta harmonics.

	max_harmonic gives the maximum harmonic to consider when calculating pocket names.
	"""

	alpha_index: [int, None] = None
	beta_index: [int, None] = None
	F_alpha: float = 0
	F_beta: float = 0

	# find out which inner list members are alpha and beta
	_ = 0
	for magnetic_oscillation in magnetic_oscillations_list_of_lists[index]:
		if magnetic_oscillation.pocket_type == "α":
			alpha_index = _
			F_alpha = magnetic_oscillations_list_of_lists[index][alpha_index].frequency
		elif magnetic_oscillation.pocket_type == "β":
			beta_index = _
			F_beta = magnetic_oscillations_list_of_lists[index][beta_index].frequency
		if alpha_index is not None and beta_index is not None:
			break
		_ += 1

	# If alpha was found, check the magnetic oscillations in the other magnetic_oscillation_lists.
	# If an oscillation at the same frequency as alpha (or optionally beta) is found, label them alpha (or beta).
	# If an oscillation was labeled as alpha, call setPocketTypes() to calculate the linear combinations of
	# alpha (and potentially beta).
	__ = 0
	for magnetic_oscillation_list in magnetic_oscillations_list_of_lists:
		if __ == index:  # Skip the mo list that contains the values we are copying from.
			__ += 1
			continue
		_ = 0
		new_alpha_index: [int, None] = None
		new_beta_index: [int, None] = None
		for magnetic_oscillation in magnetic_oscillation_list:
			if alpha_index is not None:
				if isclose(F_alpha, magnetic_oscillation.frequency, atol=frequency_width):
					if (
						new_alpha_index is not None
						and (
							abs(F_alpha - magnetic_oscillation.frequency) <
							abs(F_alpha - magnetic_oscillation_list[new_alpha_index].frequency)
						)
						or new_alpha_index is None
					):
						new_alpha_index = _
			if beta_index is not None:
				if isclose(F_beta, magnetic_oscillation.frequency, atol=frequency_width):
					if (
						new_beta_index is not None
						and (
							abs(F_beta - magnetic_oscillation.frequency) <
							abs(F_beta - magnetic_oscillation_list[new_beta_index].frequency)
						)
						or new_beta_index is None
					):
						if new_alpha_index != _:
							new_beta_index = _

			for orig_magnetic_oscillation in magnetic_oscillations_list_of_lists[index]:
				if isclose(magnetic_oscillation.frequency, orig_magnetic_oscillation.frequency, atol=frequency_width):
					magnetic_oscillation.active = orig_magnetic_oscillation.active
					if orig_magnetic_oscillation.manually_typed:
						magnetic_oscillation.pocket_type = orig_magnetic_oscillation.pocket_type
						magnetic_oscillation.manually_typed = True
			_ += 1

		if new_alpha_index is not None or new_beta_index is not None:
			setPocketTypes(
				magnetic_oscillation_list, new_alpha_index, new_beta_index,
				frequency_width=frequency_width, max_harmonic=max_harmonic
			)
		__ += 1


def extractPockets(list_of_lkf_parameters_lists: List[List[LKFParameters]], pocket_type: str) -> List[LKFParameters]:
	"""
	Extracts each LKFParameter object with the same frequency from a list of LKFParameter lists.
	"""

	pocket_lkf_parameters_list = []
	for lkf_parameters_list in list_of_lkf_parameters_lists:
		for lkf_parameters in lkf_parameters_list:
			if lkf_parameters.pocket_type == pocket_type:
				pocket_lkf_parameters_list += [lkf_parameters]

	return pocket_lkf_parameters_list
