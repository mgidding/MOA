from copy import deepcopy
from numpy import arange, full, isclose
from typing import List, Callable
from Backend.DataObjects.MOData import MOData
from Backend.DataObjects.LKFParameters import LKFParameters


def calculateOnsetField(
		mo_data: MOData, lkf_parameters_list: List[LKFParameters], pocket_type: str, H_min: float, H_max: float,
		H_step: float, min_rel_A: float, bin_size: float, calculation_type: str = 'fixed_interval',
		isCanceled: Callable[[None], bool] = None) -> (
		bool, List[float], List[MOData], List[float], List[float]
):
	"""
	Function to calculate the cyclotron mass as function of the average magnetic field.

	Notes
	----------
	calculation_type is 'fixed_interval' or 'fixed_minimum'.

	If mo_detected is True, then the last entry in each list corresponds to the field range for which the MO was detected.

	Will also set the onset field in the supplied mo_data.
	"""

	mo_data_copy = deepcopy(mo_data)
	# Set this high so that we are sure to detect the oscillation:
	mo_data_copy.oscillation_cap = 100
	mo_data_copy.oscillation_min_relative_amplitude = 0.01
	mo_data_copy.fft_bin_size = bin_size

	# define H range and other parameters (add H_step/2 to make sure all ranges are inclusive):
	if calculation_type == "fixed_minimum":
		H_max_range = arange(H_min+H_step, H_max+H_step/2, H_step)
		H_min_range = full((1, len(H_max_range)), H_min)[0]
	elif calculation_type == "fixed_interval":
		H_max_range = arange(H_min+H_step, H_max+H_step/2, H_step)
		H_min_range = arange(H_min, H_max-H_step+H_step/2, H_step)
	else:
		raise ValueError("type", calculation_type, "was not recognised.")

	frequency_width = mo_data_copy.oscillation_frequency_width
	mo_data_list: List[MOData] = []
	H_min_list: List[float] = []
	H_max_list: List[float] = []
	H_avg_list: List[float] = []
	found = False

	# Iterate over H_min and calculate m_c for each iteration:
	for _ in range(len(H_min_range)):

		# Apply new H range and retrieve the newly created lkf_parameter lists:
		mo_data_copy.interpolation_magnetic_field_range(H_min_range[_], H_max_range[_])
		if mo_data_copy.interpolation_error:
			continue  # if the H range is too small, skip this data point.

		new_lkf_parameters_list = mo_data_copy.oscillation_analysis

		# Apply pocket names to newly created lkf parameters:
		for lkf_parameters in new_lkf_parameters_list:
			for orig_lkf_parameters in lkf_parameters_list:
				if isCanceled is not None and isCanceled():
						return False, H_avg_list, mo_data_list, H_min_list, H_max_list
				if (
					isclose(lkf_parameters.frequency, orig_lkf_parameters.frequency, atol=frequency_width) and
					lkf_parameters.raw_amplitude >= min_rel_A * orig_lkf_parameters.raw_amplitude
				):
					lkf_parameters.pocket_type = orig_lkf_parameters.pocket_type
					if lkf_parameters.pocket_type == pocket_type:
						found = True

		# Retrieve amplitude and H ranges:
		H_min_list += [H_min_range[_]]
		H_max_list += [H_max_range[_]]
		H_avg_list += [1 / ((1/H_max_range[_] + 1/H_min_range[_]) / 2)]
		mo_data_list += [deepcopy(mo_data_copy)]
		if found:
			break

	return (
		found, H_avg_list, mo_data_list, H_min_list, H_max_list
	)
