from Backend.DataObjects.MOData import MOData
from Backend.DataObjects.Constants import Constants
from numpy import exp, array, ndarray, abs, inf, sqrt, diag
from scipy.optimize import curve_fit
from scipy.signal import find_peaks
from typing import List, Union, Tuple, Iterable
from copy import deepcopy


def dingleReductionFactor(
		H: Union[float, List[float], ndarray], a: Union[float, List[float], ndarray], b: Union[float, List[float], ndarray]
	) -> Union[float, List[float], ndarray]:
	"""
	Function used by scipy.optimize.curve_fit() to fit the magnetic oscillation amplitudes.
	"""

	if type(H) is list:
		H = array(H)

	return H**(1/2)*exp(-a/H)*b  # we can neglect the cyclotron mass reduction factor because it is unity above ~1T.


def calculateDingleFactor(
		mo_data: MOData, H_min: float, H_max: float, m_c: float, algorithm: str
	) -> Tuple[float, float, Union[ndarray, Iterable, int, float]]:
	"""
	Calculates the Dingle factor by fitting the envelope function of the interpolated data.
	"""

	mo_data_copy = deepcopy(mo_data)
	mo_data_copy.interpolation_magnetic_field_range("default", "default")

	H_range = mo_data_copy.interpolation_magnetic_field_range()
	H_min = H_range[0] if H_min < H_range[0] else H_min
	H_max = H_range[1] if H_max > H_range[1] else H_max

	inverse_magnetic_field = mo_data_copy.interpolation_inverse_magnetic_field
	min_H_index = (abs(inverse_magnetic_field - 1/H_max)).argmin()
	max_H_index = (abs(inverse_magnetic_field - 1/H_min)).argmin()

	if algorithm == "peak_detection":
		peak_indices, _ = find_peaks(mo_data_copy.interpolation_amplitudes[min_H_index:max_H_index])
		envelope = abs(mo_data_copy.interpolation_amplitudes[peak_indices])
		inverse_magnetic_field = mo_data_copy.interpolation_inverse_magnetic_field[peak_indices]

	elif algorithm == "hilbert":
		envelope = mo_data_copy.interpolation_envelope[min_H_index:max_H_index]
		inverse_magnetic_field = mo_data_copy.interpolation_inverse_magnetic_field[min_H_index:max_H_index]
	else:
		raise ValueError("Algorithm type not recognised. Algorithm type should be peak_detection or hilbert.")

	magnetic_field = 1/inverse_magnetic_field

	# Perform curve fitting:
	try:
		fit_parameters, fit_covariance = curve_fit(dingleReductionFactor, magnetic_field, envelope)
	except (ValueError, TypeError, RuntimeError):
		fit_parameters, fit_covariance = [0.0, 0.0], [[inf, inf], [inf, inf]]

	fit_sd = sqrt(diag(fit_covariance))

	dingle_temperature = (
		(fit_parameters[0] * Constants.e * Constants.hbar) /
		(2 * Constants.pi**2 * m_c * Constants.k_b)
	)

	dingle_temperature_sd = (
		(fit_sd[0] * Constants.e * Constants.hbar) /
		(2 * Constants.pi**2 * m_c * Constants.k_b)
	)

	return dingle_temperature, dingle_temperature_sd, fit_parameters
