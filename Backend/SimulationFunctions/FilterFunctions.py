from Backend.DataObjects.MOData import MOData
from scipy.signal import butter, sosfiltfilt, firwin2, firls, remez, lfilter
from scipy.interpolate import UnivariateSpline
from numpy import size
from typing import Union, Tuple, List, Callable


def IIRFiltering(
		mo_data: MOData, order: int, cutoffs: Union[float, Tuple[float, float]], band_type: str = 'bandpass',
		isCanceled: Callable[[None], bool] = None
) -> MOData:
	"""
	Use a Butterworth filter to filter out certain frequencies from a MOData's interpolated amplitudes.
	"""

	if band_type in ('lowpass', 'highpass'):
		if size(cutoffs) != 1:
			raise ValueError("'%s'filter take a single cutoff value", band_type)
	elif band_type in ('bandpass', 'bandstop'):
		if size(cutoffs) != 2:
			raise ValueError("'%s' filter take a sequence of two cutoff values", band_type)
	else:
		raise ValueError("band_type must be 'lowpass', 'highpass', 'bandpass' or 'bandstop'")

	H_min, H_max = mo_data.interpolation_magnetic_field_range()
	fs = mo_data.interpolation_steps / (1/H_min - 1/H_max)
	sos = butter(order, cutoffs, band_type, fs=fs, output='sos')
	if isCanceled is not None and isCanceled():
		return mo_data
	filtered_amplitudes = sosfiltfilt(sos, mo_data.interpolation_amplitudes)
	if isCanceled is not None and isCanceled():
		return mo_data
	new_mo_data = MOData.from_interpolated_data(interpolated_amplitudes=filtered_amplitudes, parent=mo_data)

	new_mo_data.name = band_type + " - " + str(cutoffs) + " [T] - IIR filtered"

	return new_mo_data


def FIRFiltering(
		mo_data: MOData, order: int, cutoffs: Union[float, Tuple[float, float]], trans_width: float,
		band_type: str = 'bandpass', algorithm: str = "firwin2", window: Union[str, Tuple] = "hamming",
		max_order: int = 20000,	auto_order: bool = True, isCanceled: Callable[[None], bool] = None
) -> MOData:
	"""
	Use a FIR filter to filter out certain frequencies from a MOData's interpolated amplitudes.
	"""

	if band_type in ('lowpass', 'highpass'):
		if size(cutoffs) != 1:
			raise ValueError("'%s'filter take a single cutoff value", band_type)
	elif band_type in ('bandpass', 'bandstop'):
		if size(cutoffs) != 2:
			raise ValueError("'%s' filter take a sequence of two cutoff values", band_type)
	else:
		raise ValueError("band_type must be 'lowpass', 'highpass', 'bandpass' or 'bandstop'")

	if auto_order and max_order < order:
		raise ValueError("max_order must be >= order")

	H_min, H_max = mo_data.interpolation_magnetic_field_range()
	fs = mo_data.interpolation_steps / (1/H_min - 1/H_max)
	order_increase = int(mo_data.interpolation_steps * 0.01)
	unwanted_amplitude_at_order = {}
	total_amplitude_at_order = {}

	# create cutoff bands based on user input, this is the format used by scipy's FIR filters:
	if band_type == 'lowpass':
		gain = [1, 1, 0, 0]
		cutoff_bands = [0, cutoffs, cutoffs + trans_width, fs / 2]
	elif band_type == 'highpass':
		gain = [0, 0, 1, 1]
		cutoff_bands = [0, cutoffs - trans_width, cutoffs, fs / 2]
	elif band_type == 'bandpass':
		gain = [0, 0, 1, 1, 0, 0]
		cutoff_bands = [0, cutoffs[0] - trans_width, cutoffs[0], cutoffs[1], cutoffs[1] + trans_width, fs / 2]
	elif band_type == 'bandstop':
		gain = [1, 1, 0, 0, 1, 1]
		cutoff_bands = [0, cutoffs[0] - trans_width, cutoffs[0], cutoffs[1], cutoffs[1] + trans_width, fs / 2]
	else:
		raise ValueError("Band type is not recognised.")

	# loop until max_order is reached. during each loop save the total amplitude of the MOs and
	# the combined amplitude of the MOs that are in the stop band
	while True:
		if algorithm == "firwin2":
			order = order+1 if order % 2 == 0 else order  # make sure we get type I filters (-> linear phase shift only)
			fir_filter = firwin2(order, cutoff_bands, gain, fs=fs, antisymmetric=False, window=window)
		elif algorithm == "firls":
			order = order+1 if order % 2 == 0 else order
			fir_filter = firls(order, cutoff_bands, gain, fs=fs)
		elif algorithm == "remez":
			fir_filter = remez(order, cutoff_bands, gain[::2], fs=fs)
		else:
			raise ValueError("Shucks, algorithm type not recognised.")

		if isCanceled is not None and isCanceled():
			return mo_data

		# apply the filter:
		filtered_amplitudes = lfilter(fir_filter, 1, mo_data.interpolation_amplitudes[::-1])
		new_mo_data = MOData.from_interpolated_data(interpolated_amplitudes=filtered_amplitudes[::-1], parent=mo_data)

		if auto_order is False:
			break

		# calculate the total and unwanted amplitudes at each order:
		_ = 0
		__ = 0
		for oscillation in new_mo_data.oscillation_analysis:
			while _ < len(cutoff_bands)-1:
				if cutoff_bands[_] <= oscillation.frequency < cutoff_bands[_ + 1]:
					if order in total_amplitude_at_order:
						total_amplitude_at_order[order] += oscillation.raw_amplitude
					else:
						total_amplitude_at_order[order] = oscillation.raw_amplitude
					if gain[_] == 0:
						if order in unwanted_amplitude_at_order:
							unwanted_amplitude_at_order[order] += oscillation.raw_amplitude
						else:
							unwanted_amplitude_at_order[order] = oscillation.raw_amplitude

					break  # break because we matched the frequency to a cutoff band
				_ += 1

		if order+order_increase <= max_order+1:
			order += order_increase
		else:
			break
		if isCanceled is not None and isCanceled():
			return mo_data

	# find the order that gave the best unwanted amplitude/total amplitude ratio (best would be 0)
	if auto_order is True:
		best_order = total_amplitude_at_order[list(total_amplitude_at_order)[0]]
		best_ratio = 1

		for order in total_amplitude_at_order:
			if order in unwanted_amplitude_at_order:
				ratio = unwanted_amplitude_at_order[order]/total_amplitude_at_order[order]
			else:
				ratio = 0
			if ratio < best_ratio:
				best_order = order
				best_ratio = ratio

		# recalculate the best order only if required
		if best_order != order:
			new_mo_data = FIRFiltering(
				mo_data, best_order-1, cutoffs, trans_width, band_type, algorithm, window, max_order, auto_order=False
			)
	else:
		best_order = order

	new_mo_data.name = mo_data.name + " - " + band_type + str(cutoffs) + " [T] " + algorithm + " " + str(best_order)

	return new_mo_data


def amplitudeSubtraction(mo_data: MOData, subtracters: Union[MOData, List[MOData]]) -> MOData:
	"""
	Subtract each 'subtracters' interpolated amplitudes from mo_data's interpolated amplitudes.
	Notes
	-----
	Works by aligning the two arrays and reinterpolating the subtracter array to have the correct dimensions.
	"""

	amplitudes = mo_data.interpolation_amplitudes
	if type(subtracters) is MOData:
		subtracters = [subtracters]

	for subtracter in subtracters:
		interpolation_function = UnivariateSpline(
			subtracter.interpolation_inverse_magnetic_field, subtracter.interpolation_amplitudes,
			k=mo_data.interpolation_univariate_spline_degree, ext=1
		)

		interpolated_amplitudes = interpolation_function(mo_data.interpolation_inverse_magnetic_field)

		amplitudes -= interpolated_amplitudes

	return MOData.from_interpolated_data(interpolated_amplitudes=amplitudes, parent=mo_data)
