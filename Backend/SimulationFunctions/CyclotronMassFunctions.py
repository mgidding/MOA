from scipy.optimize import curve_fit
from numpy import sinh, inf, arange, full, mean, float64, array, isclose, ndarray, sqrt, diag
from copy import deepcopy
from Backend.DataObjects.Constants import Constants
from Backend.DataObjects.LKFParameters import LKFParameters
from Backend.DataObjects.MOData import MOData
from Backend.SimulationFunctions.PocketFunctions import extractPockets
from typing import List, Union, Callable, Tuple


def temperatureReductionFactor(
		T: Union[float, List[float], ndarray], a: Union[float, List[float], ndarray]) -> Union[float, List[float], ndarray]:
	"""
	Function used by scipy.optimize.curve_fit() to fit the magnetic oscillation amplitudes as function of temperature.
	"""

	if type(T) is list:
		T = array(T)

	return a*T/sinh(a*T)


def calculateCyclotronMass(
		list_of_lkf_parameters_lists: List[List[LKFParameters]], pocket_type: str, min_fit_number: int = 6
) -> (float, float, List[float], List[float], float):
	"""
	Function to calculate the cyclotron mass of a single pocket for a single range of H.
	Returns m_c, m_c_sd, amplitudes, temperatures and the fit_parameter.

	Notes
	-----
	min_fit_number is the minimum number of points needed before fitting is attempted. Default is 6.

	It is assumed all magnetic oscillation amplitudes were calculated over the same magnetic field range.

	The algorithm is based on the equation for the temperature reduction factor (R_T).
	Each amplitude is divided by the amplitude of the lowest temperature measurement.
	This is done to show how the amplitude decays as function of T.
	This decay can then directly be fitted to R_T, with the free fit parameter being m_c (along with some constants).
	"""

	lkf_parameters_list = extractPockets(list_of_lkf_parameters_lists, pocket_type)
	if len(lkf_parameters_list) == 0:
		return None, None, None, None, None

	# Find list element with lowest temperature:
	min_T_element = lkf_parameters_list[0]
	for lkf_parameters in lkf_parameters_list:
		if min_T_element.temperature > lkf_parameters.temperature:
			min_T_element = lkf_parameters

	# Create temperature and amplitude arrays:
	temperatures: List[float] = []
	amplitudes: List[float] = []
	for lkf_parameters in lkf_parameters_list:
		temperatures += [lkf_parameters.temperature]
		amplitudes += [lkf_parameters.raw_amplitude / min_T_element.raw_amplitude]

	if len(temperatures) < min_fit_number:
		print("Not enough data points to make a fit with.")
		return None, None, None, None, None

	# Perform curve fitting:
	try:
		fit_parameter, fit_covariance = curve_fit(temperatureReductionFactor, temperatures, amplitudes, bounds=(0, inf))
	except ValueError:
		fit_parameter, fit_covariance = [0.0], [0.0]

	fit_sd = sqrt(diag(fit_covariance))

	# Calculate the average magnetic field:
	H_min, H_max = min_T_element.H_min, min_T_element.H_max
	H_avg = 1/((1/H_max + 1/H_min)/2)

	# Calculate the cyclotron mass:
	m_c = fit_parameter[0] * Constants.e * Constants.hbar * H_avg / (2 * Constants.pi**2 * Constants.k_b)
	m_c_sd = fit_sd[0] * Constants.e * Constants.hbar * H_avg / (2 * Constants.pi**2 * Constants.k_b)

	return m_c, m_c_sd, amplitudes, temperatures, fit_parameter[0]


def calculateCyclotronMasses(
		mo_data_list: List[MOData], list_of_lkf_parameters_lists: List[List[LKFParameters]], pocket_type: str,
		H_min: float, H_max: float, fft_min: float, fft_max: float, bin_size: float, H_step: float = 0.04,
		min_fit_number: int = 6, calculation_type: str = 'fixed_interval', isCanceled: Callable[[None], bool] = None
) -> (
		List[float], List[float], List[float], List[List[float]], List[List[float]], List[float], List[float], List[float]
	):

	"""
	Function to calculate the cyclotron mass as function of the average magnetic field.
	Returns m_c_list, m_c_sd_list, H_avg_list, amplitudes_list, temperatures_list, fit_parameters_list, H_min_list
	and H_max_list.

	Notes
	-----
	H_step is not used when calculation type is set to "single". H_step is in 1/H for fixed_interval.

	min_fit_number is the minimum number of points needed before fitting is attempted. Default is 6.

	calculation_type is 'fixed_interval', 'fixed_minimum', 'fixed_maximum' or 'single'.
	"""

	# Make a copy of the original data list
	# This is done because we will change some parameters of the MOData:
	mo_data_list_copy = deepcopy(mo_data_list)

	# define H range and other parameters:
	if calculation_type == "fixed_minimum":
		H_min_range = arange(H_min, H_max-H_step, H_step)
		H_max_range = full((1, len(H_min_range)), H_max)[0]
	elif calculation_type == "fixed_maximum":
		H_max_range = arange(H_min + H_step, H_max, H_step)
		H_min_range = full((1, len(H_max_range)), H_min)[0]
	elif calculation_type == "fixed_interval":
		inverse_H_min_range = arange(1 / H_max + H_step, 1 / H_min, H_step)[::-1]
		inverse_H_max_range = arange(1 / H_max, 1 / H_min - H_step, H_step)[::-1]
		H_min_range = 1/inverse_H_min_range[::-1]
		H_max_range = 1/inverse_H_max_range[::-1]
	elif calculation_type == "single":
		H_min_range = [H_min]
		H_max_range = [H_max]
	else:
		raise ValueError("type", calculation_type, "was not recognised.")

	frequency_width = mo_data_list_copy[0].oscillation_frequency_width
	m_c_list: List[float] = []
	m_c_sd_list: List[float] = []
	H_avg_list: List[float] = []
	amplitudes_list: List[List[float]] = []
	temperatures_list: List[List[float]] = []
	fit_parameters_list: List[float] = []
	H_min_list: List[float] = []
	H_max_list: List[float] = []

	# Iterate over H_min and calculate m_c for each iteration:
	for _ in range(len(H_min_range)):

		# Apply new H range and retrieve the newly created lkf_parameter lists:
		new_list_of_lkf_parameters_lists = []
		for mo_data in mo_data_list_copy:
			if isCanceled is not None and isCanceled():
				return (
					m_c_list, m_c_sd_list, H_avg_list,
					amplitudes_list, temperatures_list, fit_parameters_list,
					H_min_list, H_max_list
				)
			try:
				mo_data.interpolation_magnetic_field_range(H_min_range[_], H_max_range[_])
				mo_data.fft_frequency_limits(fft_min, fft_max)
				mo_data.fft_bin_size = bin_size
			except:
				continue
			if mo_data.interpolation_error:
				continue  # if the H range is too small, skip this data point.
			new_list_of_lkf_parameters_lists += [mo_data.oscillation_analysis]

		# Apply pocket names to newly created lkf parameters:
		__ = 0
		for lkf_parameters_list in new_list_of_lkf_parameters_lists:
			for lkf_parameters in lkf_parameters_list:
				for orig_lkf_parameters in list_of_lkf_parameters_lists[__]:
					if isCanceled is not None and isCanceled():
						return (
							m_c_list, m_c_sd_list, H_avg_list,
							amplitudes_list, temperatures_list, fit_parameters_list,
							H_min_list, H_max_list
						)
					if isclose(lkf_parameters.frequency, orig_lkf_parameters.frequency, atol=frequency_width):
						lkf_parameters.pocket_type = orig_lkf_parameters.pocket_type
			__ += 1

		# Calculate m_c:
		m_c, m_c_sd, amplitudes, temperatures, fit_parameter = (
			calculateCyclotronMass(new_list_of_lkf_parameters_lists, pocket_type, min_fit_number))

		# Store the values:
		if m_c is not None:
			m_c_list += [m_c]
			m_c_sd_list += [m_c_sd]
			amplitudes_list += [amplitudes]
			temperatures_list += [temperatures]
			fit_parameters_list += [fit_parameter]
			H_max_list += [H_max_range[_]]
			H_min_list += [H_min_range[_]]
			H_avg_list += [1 / ((1/H_max_range[_] + 1/H_min_range[_]) / 2)]

	return (
		m_c_list, m_c_sd_list, H_avg_list,
		amplitudes_list, temperatures_list, fit_parameters_list,
		H_min_list, H_max_list
	)


def calculateMeanCyclotronMass(
		m_c_list: List[float], m_c_covariance_list: List[float] = None, normalised: bool = False
) -> (float, Tuple[float, float]):
	"""
	Returns the mean cyclotron mass with 64 bits precision.

	Notes
	----------
	If normalised is set to True the cyclotron mass will be divided by the free electron mass.
	"""

	m_c = float(mean(m_c_list, dtype=float64))
	if normalised:
		m_c = m_c / Constants.m_e

	if m_c_covariance_list is not None:
		m_c_covariance = float(mean(m_c_covariance_list, dtype=float64))
		if normalised:
			m_c_covariance = m_c_covariance / Constants.m_e
		return m_c, m_c_covariance
	else:
		return m_c
