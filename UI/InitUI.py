from qtpy import QtWidgets
from qtpy.QtCore import QEvent, Qt, QObject
from qtpy.QtGui import QKeyEvent
from Backend.DataObjects.DataWrapper import DataWrapper
from UI.ChoiceUI import ChoicePage
from UI.SharedUI import MagneticOscillationPage
from UI.SimulationUI import SimulationPage
from UI.FFTAnalysisUI import FFTAnalysisPage
from UI.InterpolationUI import InterpolationPage
from UI.OscillationUI import OscillationPage
from UI.OnsetFieldUI import OnsetFieldPage
from UI.CyclotronMassUI import CyclotronMassPage
from UI.AngleAnalysisUI import AngleAnalysisPage
from UI.FilterUI import FilterPage
from UI.DingleFactorUI import DingleFactorPage
from UI.ResultsUI import ResultsPage


class Wizard(QtWidgets.QWizard):
	"""
	Wizard initialises a QWizard class instance and adds the QWizardPages used by the program.
	"""

	num_pages = 11
	(
		choice, simulation, interpolation, fft_analysis, mo_identification, onset_calculation, m_c_calculation,
		filter_calculation, dingle_calculation, angle_analysis, results
	) = range(num_pages)

	def __init__(self):
		super().__init__()

		# initialise data:
		data = DataWrapper()
		MagneticOscillationPage.back_button_enabled = self.button(QtWidgets.QWizard.BackButton).setEnabled
		MagneticOscillationPage.next_button_enabled = self.button(QtWidgets.QWizard.NextButton).setEnabled
		ChoicePage.next_button_enabled = self.button(QtWidgets.QWizard.NextButton).setEnabled

		# initialise pages:
		self.setPage(Wizard.choice, ChoicePage(data))
		self.setPage(Wizard.simulation, SimulationPage(data))
		self.setPage(Wizard.interpolation, InterpolationPage(data))
		self.setPage(Wizard.fft_analysis, FFTAnalysisPage(data))
		self.setPage(Wizard.mo_identification, OscillationPage(data))
		self.setPage(Wizard.onset_calculation, OnsetFieldPage(data))
		self.setPage(Wizard.m_c_calculation, CyclotronMassPage(data))
		self.setPage(Wizard.filter_calculation, FilterPage(data))
		self.setPage(Wizard.dingle_calculation, DingleFactorPage(data))
		self.setPage(Wizard.angle_analysis, AngleAnalysisPage(data))
		self.setPage(Wizard.results, ResultsPage(data))
		self.setStartId(Wizard.choice)

		# set buttons for which enter presses should be ignored:
		self.__buttons = [
			self.button(button_type) for button_type in (QtWidgets.QWizard.NextButton, QtWidgets.QWizard.FinishButton)
		]
		for button in self.__buttons:
			button.installEventFilter(self)
		screen = QtWidgets.QDesktopWidget().screenGeometry()
		width = screen.width() if screen.width() < 1200 else 1200
		height = screen.height()-100 if screen.height() < 800 else 800
		self.resize(width, height)

	def eventFilter(self, obj: QObject, event: QEvent) -> bool:
		"""
		Ignore enter presses for all buttons in self.__buttons

		Notes
		-----
		eventFilter is an internal PyQt method that can be reimplemented to change default object behaviour.
		"""

		if obj in self.__buttons and event.type() == QEvent.Show:
			obj.setDefault(False)
		return super().eventFilter(obj, event)

	def keyPressEvent(self, event: QKeyEvent):
		"""
		Don't let escape close the wizard

		Notes
		-----
		keyPressEvent is an internal PyQt method that can be reimplemented to change key press behaviour.
		"""

		if event.key() == Qt.Key_Escape:
			return
