from qtpy import QtWidgets
from qtpy.QtCore import Qt, QCoreApplication
from time import sleep
from platform import system
from UI.PlotUI import PlotFFT
from Backend.SimulationFunctions.PocketFunctions import setPocketTypes, applyPocketTypes, resetPocketTypes
from Backend.DataObjects.DataWrapper import DataWrapper
from UI.PageElements import OscillationForm
import UI.InitUI
from numpy import array, savetxt, column_stack, ndarray, isclose
from UI.SharedUI import MagneticOscillationPage
from typing import List, Optional, Union


class PlotOscillations(PlotFFT):
	"""
	Implements plotting for the FFTAnalysis page
	"""

	def drawMainPlot(
			self, data: DataWrapper, lines: bool = False, multi: bool = False, offset: float = 0, legend_style: str = 'None'):
		"""
		Draw the FFT plot and add MOs.

		Notes
		-----
		If lines is True, plot lines rather than markers.

		if multi is True, plot all mo_data objects in data.

		offset gives the offset between each plot when multi is True.

		legend_style is 'Annotated', 'Boxed' or 'None'.
		"""

		self.addFFTPlot(data, lines, multi, offset, legend_style, x_axis="Frequency [T]", draw=False)

		if multi:
			oscillation_data_list = data.list_of_lkf_parameters_lists
		else:
			oscillation_data_list = [data.lkf_parameters_list]

		# Add stars for oscillations that aren't ignored:
		_ = 0
		legend = False
		for oscillation_data in oscillation_data_list:
			__ = -1
			for oscillation in oscillation_data:
				__ += 1
				if oscillation.active:
					legend = True
					frequency = oscillation.frequency
					amplitude = oscillation.raw_amplitude + _*offset
					name = oscillation.pocket_type
					self.addLine(frequency, amplitude, lines=False, style='r*', priority=10, draw=False)
					if name is not None:
						self.addAnnotation(name, frequency * 1.01, amplitude * 1.01)
			_ += 1

		if legend and legend_style == 'Boxed':
			self.addAxisLegend('Magnetic\nOscillation', color='red', marker='*', lw=0)

		self.plot_thread.run()


class OscillationPage(MagneticOscillationPage):
	"""
	Oscillation page class.

	Notes
	-----
	On the Oscillation page users can label the found magnetic oscillations
	and select which oscillations to analyse further.
	"""

	def __init__(self, data: DataWrapper):
		super().__init__(data, OscillationForm.Ui_Form, PlotOscillations, "Magnetic Oscillation Selection and Labeling")

		# initialising variables:
		self.uncertainty: Optional[float] = None
		self.max_harmonic: Optional[int] = None
		self.frequency_items: Optional[List[QtWidgets.QTableWidgetItem]] = None
		self.pocket_items: Optional[List[QtWidgets.QTableWidgetItem]] = None
		self.alpha_index: Union[int, None] = None
		self.beta_index: Union[int, None] = None
		self.only_manual_error_shown = False

	# Initialisation methods:
	def initializePage(self):
		"""
		Calls all initialisation methods in the class and sets the currently displayed set.

		Notes
		-----
		Called automatically when the page is first displayed.
		"""

		self.data.list_of_lkf_parameters_lists = []
		for mo_data in self.data.mo_data_list:
			try:
				self.data.list_of_lkf_parameters_lists += [mo_data.oscillation_analysis]
			except:
				self.error.showMessage("Error encountered in oscillation detection algorithm")

		self.finished_init = False
		self.alpha_index = None
		self.beta_index = None
		resetPocketTypes(self.data.list_of_lkf_parameters_lists, force=True)
		self.initializeTable()
		super().initializePage()

	def initializeFieldValues(self):
		"""
		Initialises all fields in the class with data from the currently selected MOData object.
		"""

		self.ui.field_uncertainty_labeling.setMinimum(0.0000000001)
		self.ui.field_max_p_labeling.setMinimum(1)
		self.ui.field_change_set.blockSignals(True)

		index = self.data.index
		self.ui.field_change_set.setCurrentIndex(index)
		self.ui.field_change_set.blockSignals(False)
		self.ui.field_uncertainty.setValue(self.data.mo_data.oscillation_frequency_width)
		self.ui.field_uncertainty_labeling.setValue(self.data.mo_data.oscillation_frequency_width)
		self.ui.field_max_p_labeling.setValue(self.data.mo_data.oscillation_max_harmonic)
		self.ui.field_max_p.setValue(self.data.mo_data.oscillation_max_harmonic)
		self.ui.field_offset.setValue(0.0)
		self.ui.field_min_rel_A.setValue(self.data.mo_data.oscillation_min_relative_amplitude)
		self.ui.field_oscillations.setMinimum(0)
		self.ui.field_oscillations.setValue(self.data.mo_data.oscillation_cap)
		self.ui.field_uncertainty.setMinimum(0.0000000001)
		self.ui.field_min_rel_A.setRange(0.0000000001, 1)
		self.ui.field_max_p.setMinimum(1)

	def initializeButtons(self):
		"""
		Connect button methods to their buttons.
		"""

		super().initializeButtons()
		self.ui.alpha_button.clicked.connect(self.buttonSetAlpha)
		self.ui.beta_button.clicked.connect(self.buttonSetBeta)
		self.ui.reset_current_button.clicked.connect(self.buttonResetCurrent)
		self.ui.reset_all_button.clicked.connect(self.buttonResetAll)
		self.ui.apply_all_button.clicked.connect(self.buttonApplyToAll)
		self.ui.enable_all_button.clicked.connect(self.buttonEnableAll)
		self.ui.tableWidget.itemChanged.connect(self.buttonTableItemClicked)
		self.ui.field_harmonic_comp.clicked.connect(self.buttonHarmonicCompToggle)

		self.ui.field_oscillations.valueChanged.connect(self.updateOscillations)
		self.ui.field_min_rel_A.valueChanged.connect(self.updateOscillations)
		self.ui.field_max_p.valueChanged.connect(self.updateOscillations)
		self.ui.field_uncertainty.valueChanged.connect(self.updateOscillations)

	def initializeTable(self):
		"""
		Generate a table containing each magnetic oscillation in the currently selected data set.

		Notes
		-----
		Generates table from the magnetic oscillations that were found on the FFTAnalysis page.
		"""

		self.ui.tableWidget.blockSignals(True)
		self.ui.tableWidget.clearContents()

		if self.data.mode != "angle":
			if self.ui.tableWidget.columnCount() == 1:
				self.ui.tableWidget.setColumnCount(2)
				header = QtWidgets.QTableWidgetItem()
				header.setText("Pocket Type")
				self.ui.tableWidget.setHorizontalHeaderItem(1, header)
		else:
			self.ui.tableWidget.removeColumn(2)
			self.ui.tableWidget.removeColumn(1)

		length = len(self.data.lkf_parameters_list)
		self.ui.tableWidget.setRowCount(length)
		self.ui.tableWidget.setAlternatingRowColors(True)

		# Create a QTableWidgetItem for each frequency and add it to the table and item list:
		self.frequency_items: List[QtWidgets.QTableWidgetItem] = []
		self.pocket_items: List[QtWidgets.QTableWidgetItem] = []

		for _ in range(length):
			# Create a QTableWidgetItem:
			frequency_item = QtWidgets.QTableWidgetItem()
			pocket_item = QtWidgets.QTableWidgetItem()
			phase_item = QtWidgets.QTableWidgetItem()
			# Set the text:
			frequency_item.setText(
				str('{:.3}'.format(self.data.lkf_parameters_list[_].frequency))
			)
			phase_item.setText(
				str('{:.3}'.format(self.data.lkf_parameters_list[_].phase))
			)
			if self.data.lkf_parameters_list[_].pocket_type is not None:
				pocket_item.setText(str(
					self.data.lkf_parameters_list[_].pocket_type))

			# Set how the user can interact with the items:
			frequency_item.setFlags(Qt.ItemIsUserCheckable | Qt.ItemIsEnabled | Qt.ItemIsSelectable)
			phase_item.setFlags(Qt.ItemIsUserCheckable | Qt.ItemIsEnabled | Qt.ItemIsSelectable)
			pocket_item.setFlags(Qt.ItemIsEnabled | Qt.ItemIsSelectable | Qt.ItemIsEditable)

			# Set default state:
			if self.data.lkf_parameters_list[_].active is True:
				frequency_item.setCheckState(Qt.Checked)
			else:
				frequency_item.setCheckState(Qt.Unchecked)

			# Add items to table:
			self.ui.tableWidget.setItem(_, 0, frequency_item)
			self.frequency_items += [frequency_item]
			if self.data.mode != "angle":
				self.ui.tableWidget.setItem(_, 1, pocket_item)
				self.pocket_items += [pocket_item]
				self.ui.tableWidget.setItem(_, 2, phase_item)

		# Resize table headers to fit width:
		self.ui.tableWidget.horizontalHeader().setSectionResizeMode(QtWidgets.QHeaderView.Stretch)
		self.ui.tableWidget.blockSignals(False)

	def initializePageElements(self):
		"""
		Initialises the page elements based on which mode (angle or temperature dependence) the program is in.
		"""

		if self.data.mode == "angle":
			state = False
		else:
			state = True

		# toggle the following objects based on the mode:
		ui_objects = (
				[self.ui.alpha_button] + [self.ui.beta_button] + [self.ui.label_max_p_labeling] +
				[self.ui.field_max_p_labeling] + [self.ui.apply_all_button]
		)
		for ui_object in ui_objects:
			ui_object.setVisible(state)

		# change names:
		if state:  # temperature mode
			self.ui.MO_labeling.setTitle("Magnetic Oscillation Labeling")
			self.setTitle("Magnetic Oscillation Selection and Labeling")
		else:
			self.ui.MO_labeling.setTitle("Toggling Options")
			self.setTitle("Magnetic Oscillation Selection")

# Validator methods:
	def validatePage(self) -> bool:
		"""
		Validates the state of the page and returns true if the state is acceptable.

		Notes
		-----
		Called automatically when the next button is clicked.
		"""

		# If this is the last page, return True
		if self.nextId() == -1:
			return True

		if not super().validatePage():
			return False

		# Only continue if at least one active oscillation is named and activated:
		_ = 0
		valid = False
		frequencies = []
		for lkf_parameters_list in self.data.list_of_lkf_parameters_lists:
			__ = 0
			for lkf_parameters in lkf_parameters_list:
				if lkf_parameters.active:
					valid = True
					if lkf_parameters.pocket_type is None:
						pocket_type = str(round(lkf_parameters.frequency, 2))
						in_list = False
						for frequency in frequencies:
							if isclose(frequency, lkf_parameters.frequency, atol=self.ui.field_uncertainty_labeling.value()):
								pocket_type = str(round(frequency, 2))
								in_list = True
								break
						if in_list is False:
							frequencies += [lkf_parameters.frequency]
						lkf_parameters.pocket_type = pocket_type
				__ += 1
			_ += 1

		# No oscillations are named, handle this state:
		if valid:
			return True
		else:
			# No oscillations are activated.
			self.error.showMessage(
				"You have not activated any magnetic oscillations. "
				"Please activate (and name) at least one magnetic oscillation."
			)
			return False

	def nextId(self) -> int:
		"""
		Decide which Wizard page to load next based on page state.

		Notes
		-----
		nextId() is an internal PyQt method that can be reimplemented to change which QWizard page is loaded next.
		"""

		if self.data.mode == "angle":
			return UI.InitUI.Wizard.angle_analysis
		else:
			if MagneticOscillationPage.onset_field_detection:
				return UI.InitUI.Wizard.onset_calculation
			elif MagneticOscillationPage.cyclotron_mass_detection:
				return UI.InitUI.Wizard.m_c_calculation
			elif MagneticOscillationPage.dingle_temp_calculation:
				return UI.InitUI.Wizard.filter_calculation
			else:
				return UI.InitUI.Wizard.results

# Data methods:

	def updateData(self):
		"""
		Reads user input from the UI.
		"""

		for mo_data in self.data.mo_data_list:
			mo_data.oscillation_frequency_width = self.ui.field_uncertainty.value()
			mo_data.oscillation_min_relative_amplitude = self.ui.field_min_rel_A.value()
			mo_data.oscillation_harmonic_compensation = self.ui.field_harmonic_comp.isChecked()
			self.uncertainty = self.ui.field_uncertainty_labeling.value()
			self.max_harmonic = self.ui.field_max_p_labeling.value()
			mo_data.oscillation_max_harmonic = self.ui.field_max_p.value()
			mo_data.oscillation_cap = self.ui.field_oscillations.value()

	def updatePocketNames(self):
		"""
		Update the pocket names of each oscillation based on user input.

		Notes
		-----
		Uses Backend.PocketFunctions.setPocketTypes() with the alpha and beta indices of the currently selected set.
		"""

		self.updateData()
		setPocketTypes(
			self.data.lkf_parameters_list, self.alpha_index, self.beta_index, self.max_harmonic, self.uncertainty
		)

	def saveData(self, filename: str):
		"""
		Saves data to user-selected text file.
		"""

		frequencies: Union[List[float], ndarray] = []
		amplitudes: Union[List[float], ndarray] = []
		names: Union[List[str], ndarray] = []

		_ = 0
		for oscillation in self.data.lkf_parameters_list:
			if oscillation.active:
				frequencies += [oscillation.frequency]
				amplitudes += [oscillation.raw_amplitude]
				names += [str(oscillation.pocket_type)]
			_ += 1

		frequencies = array(frequencies)
		amplitudes = array(amplitudes)
		names = array(names)

		outdata = column_stack((names, frequencies, amplitudes))

		savetxt(
			filename, outdata, delimiter="	", fmt="%s",
			header="Pocket Name Frequency [T]   Amplitude")

	def saveAllData(self, foldername: str):
		"""
		Saves all data sets to user-selected folder.
		"""

		index = self.data.index
		for _ in range(len(self.data.mo_data_list)):
			self.data.index = _
			filename = str(self.data.mo_data.name + " pockets.txt")
			self.saveData(foldername + "/" + filename)
		self.data.index = index

	def setAllLKFData(self) -> bool:  #
		"""
		Returns True if process was not canceled, else False.
		"""

		index = self.data.index
		progress = QtWidgets.QProgressDialog(
			"Finding oscillations...", "Cancel", 0, len(self.data.mo_data_list), self
		)
		progress.forceShow()
		for _ in range(len(self.data.mo_data_list)):
			progress.setValue(_+1)
			QCoreApplication.processEvents()
			sleep(0.01)
			if progress.wasCanceled():
				self.data.index = index
				return False
			self.data.index = _
			self.setLKFData()
		if system() == 'Windows':
			QCoreApplication.processEvents()  # Windows needs this else the next button won't work...
		self.data.index = index
		return True

	def setLKFData(self):
		"""
		Retrieve the LKFParameters list from the currently selected MOData object and write it to DataWrapper.
		"""

		# noinspection PyBroadException
		try:
			self.data.lkf_parameters_list = self.data.mo_data.oscillation_analysis
			for lkf_parameters in self.data.lkf_parameters_list:
				lkf_parameters.active = False
		except:
			self.error.showMessage("Error encountered in oscillation detection algorithm")

# UI methods:
	def updateTable(self):
		"""
		Updates the contents of the table showing the oscillations and labels.
		"""

		for _ in range(len(self.data.lkf_parameters_list)):
			if self.data.mode != "angle":
				if self.data.lkf_parameters_list[_].pocket_type is not None:
					self.pocket_items[_].setText(self.data.lkf_parameters_list[_].pocket_type)
				else:
					self.pocket_items[_].setText("")
			if self.data.lkf_parameters_list[_].active is True:
				self.frequency_items[_].setCheckState(Qt.Checked)
			else:
				self.frequency_items[_].setCheckState(Qt.Unchecked)

	def changeSet(self):
		"""
		Update the UI whenever the currently selected MOData object gets changed.
		"""

		self.ui.tableWidget.itemChanged.disconnect()
		self.ui.tableWidget.blockSignals(True)
		index = self.ui.field_change_set.currentIndex()
		self.data.index = index

		self.initializeTable()
		self.updateTable()
		if not self.ui.plot_all.isChecked():
			self.renderUI()
		self.ui.tableWidget.blockSignals(False)
		self.ui.tableWidget.itemChanged.connect(self.buttonTableItemClicked)

	def updateOscillations(self):
		"""
		Update the UI whenever the MO detection algorithm options are changed.
		"""

		if not self.finished_init:
			return
		self.updateData()
		self.setAllLKFData()
		self.ui.tableWidget.itemChanged.disconnect()
		self.ui.tableWidget.blockSignals(True)
		self.initializeTable()
		self.updateTable()
		self.autoUpdate()
		self.ui.tableWidget.blockSignals(False)
		self.ui.tableWidget.itemChanged.connect(self.buttonTableItemClicked)

# Button methods:
	def buttonTableItemClicked(self, item: QtWidgets.QTableWidgetItem):
		"""
		Process whether the selected table item was enabled or disabled.

		Notes
		-----
		Gets called automatically when a table item is clicked by the user.
		"""

		row = item.row()
		if item.column() == 1:
			if item.text() != "" and item.text() != self.data.lkf_parameters_list[row].pocket_type:
				self.data.lkf_parameters_list[row].pocket_type = item.text()
				self.data.lkf_parameters_list[row].manually_typed = True
				if row == self.alpha_index:
					self.alpha_index = None
				elif row == self.beta_index:
					self.beta_index = None
				self.updatePocketNames()
			self.updateTable()
			self.autoUpdate()

		# Check if the oscillation was selected or deselected:
		else:
			if item.checkState() == Qt.Checked and self.data.lkf_parameters_list[row].active is False:
				self.data.lkf_parameters_list[row].active = True
			elif item.checkState() == Qt.Unchecked and self.data.lkf_parameters_list[row].active is True:
				self.data.lkf_parameters_list[row].active = False
		self.autoUpdate()

	def buttonSetAlpha(self):
		"""
		Updates the alpha index of the currently selected set.
		"""

		self.ui.tableWidget.itemChanged.disconnect()
		alpha_index = self.ui.tableWidget.currentRow()
		if self.alpha_index != alpha_index:
			self.data.lkf_parameters_list[alpha_index].pocket_type = None
			self.data.lkf_parameters_list[alpha_index].manually_typed = False
			self.alpha_index = alpha_index
			if alpha_index == self.beta_index:
				self.beta_index = None
		else:
			self.alpha_index = None

		self.updatePocketNames()
		self.updateTable()
		self.ui.tableWidget.itemChanged.connect(self.buttonTableItemClicked)
		self.autoUpdate()

	def buttonSetBeta(self):
		"""
		Updates the beta index of the currently selected set.
		"""

		self.ui.tableWidget.itemChanged.disconnect()
		beta_index = self.ui.tableWidget.currentRow()
		if self.beta_index != beta_index:
			self.data.lkf_parameters_list[beta_index].pocket_type = None
			self.data.lkf_parameters_list[beta_index].manually_typed = False
			self.beta_index = beta_index
			if beta_index == self.alpha_index:
				self.alpha_index = None
		else:
			self.beta_index = None

		self.updatePocketNames()
		self.updateTable()
		self.ui.tableWidget.itemChanged.connect(self.buttonTableItemClicked)
		self.autoUpdate()

	def buttonResetCurrent(self):
		"""
		Resets current labeling and ignore lists.
		"""

		self.ui.tableWidget.itemChanged.disconnect()
		resetPocketTypes([self.data.lkf_parameters_list], force=True)
		self.updateTable()
		self.ui.tableWidget.itemChanged.connect(self.buttonTableItemClicked)
		self.autoUpdate()

	def buttonResetAll(self):
		"""
		Resets all labeling and ignore lists.
		"""

		self.ui.tableWidget.itemChanged.disconnect()
		resetPocketTypes(self.data.list_of_lkf_parameters_lists, force=True)
		self.updateTable()
		self.ui.tableWidget.itemChanged.connect(self.buttonTableItemClicked)
		self.autoUpdate()

	def buttonApplyToAll(self):
		"""
		Apply the current pocket labeling scheme to all data sets
		"""

		self.updateData()
		index = self.ui.field_change_set.currentIndex()
		resetPocketTypes(self.data.list_of_lkf_parameters_lists, [index], force=True)
		applyPocketTypes(self.data.list_of_lkf_parameters_lists, index, self.uncertainty, self.max_harmonic)
		self.autoUpdate()

	def buttonEnableAll(self):
		"""
		Enable all MOs.
		"""

		self.ui.tableWidget.blockSignals(True)
		for lkf_parameters_list in self.data.list_of_lkf_parameters_lists:
			for lkf_parameters in lkf_parameters_list:
				lkf_parameters.active = True
		self.updateTable()
		self.ui.tableWidget.blockSignals(False)
		self.autoUpdate()

	def buttonHarmonicCompToggle(self):
		"""
		Toggles the harmonic compensation UI elements.

		Notes
		-----
		Called automatically when harmonic compensation is toggled by the user.
		"""

		mo_harmonic_fields = [self.ui.field_uncertainty, self.ui.field_max_p]
		mo_harmonic_labels = [self.ui.label_uncertainty, self.ui.label_max_p]
		if self.ui.field_harmonic_comp.isChecked():
			for ui_object in mo_harmonic_fields + mo_harmonic_labels:
				ui_object.setEnabled(True)
		else:
			for ui_object in mo_harmonic_fields + mo_harmonic_labels:
				ui_object.setEnabled(False)

		self.updateOscillations()
