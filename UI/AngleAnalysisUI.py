from UI.PageElements import AngleAnalysisForm
from UI.PlotUI import PlotCanvas
from numpy import cos, savetxt, array, column_stack
from UI.SharedUI import MagneticOscillationPage
from Backend.DataObjects.DataWrapper import DataWrapper


class PlotAngleAnalysis(PlotCanvas):
	"""
	Implements plotting for the angle analysis page.
	"""

	def drawAnglePlot(self, data: DataWrapper, multiply: bool = False, mode: str = "theta_vs_F"):
		"""
		Plot the angle vs the frequency.

		Notes
		-----
		If multiply is True, multiply the frequency by cos(angle)

		mode is either "theta_vs_F" (default) or "F_vs_theta"
		"""

		if mode == "theta_vs_F":
			x_label = "Frequency [T]"
			y_label = "Angle θ"
		else:
			x_label = "Angle θ"
			y_label = "Frequency [T]"

		self.addPlot(x_label, y_label)

		if len(data.mo_data_list) == 0:
			return

		angles = []
		frequencies = []
		_ = 0
		# Retrieve the angle and frequency for each not-ignored oscillation:
		for magnetic_oscillation_list in data.list_of_lkf_parameters_lists:
			for magnetic_oscillation in magnetic_oscillation_list:
				if not magnetic_oscillation.active:
					continue
				if len(data.mo_data_list) == 0:
					continue
				angle = data.mo_data_list[_].angle
				angles += [angle]
				frequency = magnetic_oscillation.frequency
				# multiply the angle by cos(theta) if requested:
				if multiply:
					frequency *= cos(angle)
				frequencies += [frequency]
			_ += 1

		if mode == "theta_vs_F":
			x = frequencies
			y = angles
		else:
			x = angles
			y = frequencies

		self.addLine(x, y, False)
		self.plot_thread.setParameters(auto_x=False)
		self.plot_thread.run()


class AngleAnalysisPage(MagneticOscillationPage):
	"""
	Angle Analysis page class.

	Notes
	-----
	On the angle analysis page users can plot the angle vs the frequency.
	"""

	def __init__(self, data: DataWrapper):
		super().__init__(data, AngleAnalysisForm.Ui_Form, PlotAngleAnalysis, "Angle Dependence Analysis")

	# Initialisation methods:
	def initializePage(self):
		"""
		Calls all initialisation methods in the class and sets the currently displayed set.

		Notes
		-----
		Called automatically when the page is first displayed.
		"""

		self.finished_init = False
		self.initializeButtons()
		self.resizeScrollArea()
		self.ui.auto_update.blockSignals(True)
		self.ui.auto_update.setChecked(MagneticOscillationPage.auto_update)
		self.ui.auto_update.blockSignals(False)
		self.finished_init = True
		self.renderUI()

	def initializeButtons(self):
		"""
		Connect button methods to their buttons.
		"""

		self.container_ui.plot_button.clicked.connect(self.renderUI)
		self.ui.auto_update.clicked.connect(self.autoUpdate)
		self.ui.F_vs_theta.clicked.connect(self.autoUpdate)
		self.ui.theta_vs_F.clicked.connect(self.autoUpdate)
		self.ui.multiply.clicked.connect(self.autoUpdate)
		self.ui.save_data.clicked.connect(self.buttonSaveData)
		self.container_ui.abort_plot_button.clicked.connect(self.plot_widget.plot_thread.cancel)

	def initializeUI(self):
		pass

	def nextId(self) -> int:
		return -1  # final page

# UI methods:
	def plot(self, alternative=False):
		"""
		Updates the on-screen plot.

		Notes
		-----
		Uses UI.PlotUI.PlotInterpolation().
		"""

		# prevent multiple plots running at the same time:
		if self.is_plotting:
			return
		self.is_plotting = True

		if alternative is True or not self.ui.F_vs_theta.isChecked():
			mode = "F_vs_theta"
		else:
			mode = "theta_vs_F"

		self.plot_widget.drawAnglePlot(self.data, multiply=self.ui.multiply.isChecked(), mode=mode)

		self.is_plotting = False

# Data methods:
	def saveData(self, filename: str):
		"""
		Saves data to user-selected text file.
		"""

		angles = []
		frequencies = []
		_ = 0
		# Retrieve the angle and frequency for each not-ignored oscillation:
		for lkf_parameters_list in self.data.list_of_lkf_parameters_lists:
			for lkf_parameters in lkf_parameters_list:
				if not lkf_parameters.active:
					continue
				angle = self.data.mo_data_list[_].angle
				angles += [angle]
				frequency = lkf_parameters.frequency
				# multiply the angle by cos(theta) if requested:
				if self.ui.multiply.isChecked():
					frequency *= cos(angle)
				frequencies += [frequency]
			_ += 1

		angles = array(angles)
		frequencies = array(frequencies)

		if self.ui.F_vs_theta.isChecked():
			x = angles
			y = frequencies
			header = "Angle, Frequency [T]"
		else:
			x = frequencies
			y = angles
			header = "Frequency [T] Angle"

		if self.ui.multiply.isChecked():
			comment = "Frequency is multiplied by cos(theta)"
		else:
			comment = ""

		output = column_stack((x, y))
		savetxt(
			filename, output, delimiter="	", fmt="%s", header=header, comments=comment)
