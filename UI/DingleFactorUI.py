from qtpy.QtCore import QThread
from Backend.DataObjects.Constants import Constants
from Backend.DataObjects.MOData import MOData
from UI.PageElements import DingleFactorForm
from Backend.SimulationFunctions.DingleFunctions import calculateDingleFactor, dingleReductionFactor
from UI.PlotUI import PlotMagneticField
from UI.SharedUI import MagneticOscillationPage
from math import floor, ceil
from typing import List, Dict, Union, Iterable, Optional, Callable, Any
from numpy import ndarray
from Backend.DataObjects.DataWrapper import DataWrapper
import UI.InitUI


class PlotDingleFactor(PlotMagneticField):
	"""
	Implements plotting for the Dingle factor page.
	"""

	def drawDinglePlot(
			self, data: DataWrapper, fit_parameters: Union[ndarray, Iterable, int, float], min_H: float, max_H: float,
			index: int = 0, lines: bool = False, legend_style: str = 'None'
	):
		"""
		Draw the Dingle temperature plot.

		Notes
		-----
		If lines is True, plot lines rather than markers.

		legend_style is 'Annotated', 'Boxed' or 'None'.
		"""

		self.addMagneticFieldPlot("Inverse Field [1/T]", "Amplitude", offset=0, legend_style=legend_style)

		min_H_index = (abs(data.mo_data.filtered_data_list[index].interpolation_inverse_magnetic_field - 1 / max_H)).argmin()
		max_H_index = (abs(data.mo_data.filtered_data_list[index].interpolation_inverse_magnetic_field - 1 / min_H)).argmin()
		inv_H_field = data.mo_data.filtered_data_list[index].interpolation_inverse_magnetic_field[min_H_index:max_H_index]
		amplitudes = data.mo_data.filtered_data_list[index].interpolation_amplitudes[min_H_index:max_H_index]
		annotation = str(data.mo_data.filtered_data_list[index].temperature) + '[K]'
		self.addLine(inv_H_field, amplitudes, lines, label=annotation, draw=False)
		self.addLine(
			inv_H_field, dingleReductionFactor(1/inv_H_field, fit_parameters[0], fit_parameters[1]), lines,
			label="Envelope Fit", draw=False
		)
		self.plot_thread.run()


class DingleFactorCalculation(QThread):
	"""
	QThread child class that handles running the cyclotron mass calculation on its own thread.
	"""

	def __init__(self):
		QThread.__init__(self)
		self.mo_data: Optional[MOData] = None
		self.H_min: Optional[float] = None
		self.H_max: Optional[float] = None
		self.m_c: Optional[float] = None
		self.dingle_temperature: Optional[float] = None
		self.dingle_temperature_sd: Optional[float] = None
		self.fit_parameters: Optional[Union[ndarray, Iterable, int, float]] = None
		self.canceled: bool = False
		self.error: bool = False
		self.algorithm: Optional[str] = None

	def run(self):
		"""
		Method that is called when DingleFactorCalculation.start() is executed.

		Notes
		-----
		Uses Backend.SimulationFunctions.DingleFunctions.calculateDingleFactor.
		"""

		self.error = False
		self.canceled = False
		try:
			(self.dingle_temperature, self.dingle_temperature_sd, self.fit_parameters) = (
				calculateDingleFactor(self.mo_data, self.H_min, self.H_max, self.m_c, self.algorithm)
			)
		except:
			self.error = True

	def getResults(self) -> (float,  float, Union[ndarray, Iterable, int, float]):
		return self.dingle_temperature, self.dingle_temperature_sd, self.fit_parameters

	def isCanceled(self) -> bool:
		"""
		Returns whether the calculation is canceled.
		"""

		return self.canceled

	def errorEncountered(self) -> bool:
		"""
		Returns true if the calculation thread encountered an error.
		"""

		return self.error

	def cancel(self):
		"""
		Cancels the calculation when called.
		"""

		self.canceled = True

	def setParameters(self, mo_data: MOData, H_min: float, H_max: float, m_c: float, algorithm: str):
		"""
		Method that sets the parameters used by the dingle factor calculation.
		"""

		self.mo_data = mo_data
		self.H_min = H_min
		self.H_max = H_max
		self.m_c = m_c
		self.algorithm = algorithm


class DingleFactorPage(MagneticOscillationPage):
	"""
	DingleFactor page class.

	Notes
	-----
	On the Dingle factor page users can change the settings used by the Dingle temperature estimation algorithm.
	"""

	algorithms = ['peak_detection', 'hilbert']

	def __init__(self, data: DataWrapper):
		super().__init__(data, DingleFactorForm.Ui_Form, PlotDingleFactor, "Dingle Temperature Calculation")

		# initialize variables:
		self.H_min: Optional[float] = None
		self.H_max: Optional[float] = None
		self.frequencies: Optional[Dict[int]] = None
		self.dingle_temperature: Optional[float] = None
		self.dingle_temperature_sd: Optional[float] = None
		self.fit_parameters: Optional[Union[ndarray, Iterable, int, float]] = None
		self.pockets: Dict[int] = {}
		self.run: bool = True
		self.data_set: Optional[str] = None
		self.pocket_type: Optional[str] = None
		self.m_c: Optional[float] = None
		self.callback: Optional[Callable[[None], ...]] = None
		self.index: Optional[int] = None
		self.algorithm: Optional[str] = None

		# Set up threading:
		self.calculation_thread: QThread = DingleFactorCalculation()
		self.calculation_thread.finished.connect(self.finishDingleFactorCalculation)

	# Initialisation methods:
	def initializeButtons(self):
		"""
		Connect button methods to their buttons.
		"""

		# Plot buttons:
		self.container_ui.plot_button.clicked.connect(self.renderUI)
		self.ui.plot_lines.clicked.connect(self.buttonPlotLines)
		self.ui.field_legend.currentIndexChanged.connect(self.buttonLegend)
		self.ui.auto_update.clicked.connect(self.autoUpdate)
		self.ui.field_change_set.currentIndexChanged.connect(self.changeSet)
		self.container_ui.abort_plot_button.clicked.connect(self.plot_widget.plot_thread.cancel)
		self.ui.button_save.clicked.connect(self.buttonSaveValue)
		self.ui.field_change_pocket.currentIndexChanged.connect(self.changePocket)
		self.ui.field_algorithm.currentIndexChanged.connect(self.autoUpdate)

	def initializeChangeSet(self):
		"""
		Adds each identified pocket to the drop down menu so they can be selected.
		"""

		# Create a list of all pocket types that should be added to change_set:
		names: List[str] = []
		_ = 0
		index = None
		for lkf_parameters_list in self.data.list_of_lkf_parameters_lists:
			add = False
			for lkf_parameters in lkf_parameters_list:
				if lkf_parameters.active:
					add = True
			if add and len(self.data.mo_data_list[_].filtered_data_list) > 0:
				names += [self.data.mo_data_list[_].name]
				index = _ if index is None else index
			_ += 1

		self.data.index = index

		# Add the data sets to the UI and (re)connect the plot function to change_set.
		self.ui.field_change_set.blockSignals(True)
		self.ui.field_change_set.clear()
		self.ui.field_change_set.addItems(names)
		self.ui.field_change_set.blockSignals(False)
		self.initializePockets()

	def initializePockets(self):
		"""
		Adds each filtered data set to the drop down menu so they can be selected.
		"""

		# Create a list of all pocket types that should be added to change_set:
		self.pockets: Dict[int] = {}
		pocket_types: List[str] = []
		_ = 0
		for filtered_data in self.data.mo_data.filtered_data_list:
			if filtered_data.lkf_parameters.active is False:
				_ += 1
				continue
			pocket_type = filtered_data.lkf_parameters.pocket_type
			if pocket_type not in self.pockets and pocket_type is not None:
				self.pockets[pocket_type] = _
				pocket_types += [pocket_type]
			_ += 1

		# Add the names to the UI:
		self.ui.field_change_pocket.blockSignals(True)
		self.ui.field_change_pocket.clear()
		self.ui.field_change_pocket.addItems(pocket_types)
		self.ui.field_change_pocket.blockSignals(False)

	def initializePage(self):
		"""
		Calls all initialisation methods in the class and sets the currently displayed set.

		Notes
		-----
		Called automatically when the page is first displayed.
		"""

		self.run = True
		self.container_ui.plot_button.setText("Calculate")

		# Show "Calculating" during initial plot:
		self.plot_widget.addPlot("Inverse Field [1/T]", "Amplitude")
		# Draw a single white dot as we need to draw something
		self.plot_widget.addLine(0, 0, False, style='w*')
		self.plot_widget.addAnnotation("Calculating!", 0, 0)
		self.plot_widget.showPlots()

		super().initializePage()

	def initializeFieldValues(self):
		"""
		Initialises all fields in the class with data from the currently selected MOData object.
		"""

		index = self.pockets[self.ui.field_change_pocket.currentText()]
		min_H, max_H = self.data.mo_data.filtered_data_list[index].interpolation_magnetic_field_range()

		# Set ranges:
		self.ui.field_min_H.setRange(floor(min_H), ceil(max_H))
		self.ui.field_max_H.setRange(floor(min_H), ceil(max_H))

		# overwrite min_H with the onset field H if it was calculated previously:
		for oscillation in self.data.lkf_parameters_list:
			if oscillation.pocket_type == self.ui.field_change_set.currentText() and oscillation.onset_field is not None:
				self.ui.field_min_H.setValue(oscillation.onset_field)
				break
		else:
			self.ui.field_min_H.setValue(floor(min_H))
		self.ui.field_max_H.setValue(ceil(max_H))

		self.ui.field_m_c.setValue(self.data.mo_data.filtered_data_list[index].lkf_parameters.m_c/Constants.m_e)

# Validator methods:
	def validateFields(self) -> bool:
		"""
		Validate each field in the page and return true if all are acceptable.
		"""

		# Check that the min magnetic field is lower than the max and that the interval is not too big:
		if self.ui.field_min_H.value() >= self.ui.field_max_H.value():
			self.ui.field_min_H.setStyleSheet('QDoubleSpinBox { background-color: #da4453}')
			self.ui.field_max_H.setStyleSheet('QDoubleSpinBox { background-color: #da4453}')
			return False
		else:
			self.ui.field_min_H.setStyleSheet('')
			self.ui.field_max_H.setStyleSheet('')
		return True

	def nextId(self) -> int:
		"""
		Decide which Wizard page to load next based on page state.

		Notes
		-----
		nextId() is an internal PyQt method that can be reimplemented to change which QWizard page is loaded next.
		"""

		return UI.InitUI.Wizard.results

# Data methods:
	def startDingleFactorCalculation(self, custom_callback: Callable[[None], Any] = None):
		"""
		Sets up and starts the Dingle factor calculation.

		Notes
		-----
		custom_callback is the function to call when calculation is finished.
		"""

		data_set = self.ui.field_change_set.currentText()
		if self.data_set != data_set:
			self.data_set = data_set
			self.run = True

		pocket_type = self.ui.field_change_pocket.currentText()
		if self.pocket_type != pocket_type:
			self.pocket_type = pocket_type
			self.run = True

		H_min = self.ui.field_min_H.value()
		if self.H_min != H_min:
			self.H_min = H_min
			self.run = True

		H_max = self.ui.field_max_H.value()
		if self.H_max != H_max:
			self.H_max = H_max
			self.run = True

		m_c = self.ui.field_m_c.value() * Constants.m_e
		if self.m_c != m_c:
			self.m_c = m_c
			self.run = True

		index = self.pockets[self.ui.field_change_pocket.currentText()]
		if self.index != index:
			self.index = index
			self.run = True

		algorithm = DingleFactorPage.algorithms[self.ui.field_algorithm.currentIndex()]
		if self.algorithm != algorithm:
			self.algorithm = algorithm
			self.run = True

		if custom_callback is None:
			self.callback = self.finishRenderUI
		else:
			self.callback = custom_callback

		if not self.run:
			self.callback()
		else:
			self.calculation_thread.setParameters(self.data.mo_data.filtered_data_list[index], H_min, H_max, m_c, algorithm)
			self.container_ui.abort_button.setVisible(True)
			self.container_ui.plot_button.setVisible(False)
			self.enableUI(False)
			self.calculation_thread.start()

	def terminateDingleFactorCalculation(self):
		"""
		Terminates the Dingle factor calculation.
		"""

		self.calculation_thread.cancel()
		self.calculation_thread.quit()
		self.calculation_thread.wait(1000)
		self.container_ui.abort_button.setVisible(False)
		self.container_ui.plot_button.setVisible(True)
		self.enableUI(True)

	def finishDingleFactorCalculation(self):
		"""
		Processes the result of the Dingle factor calculation and updates the UI.

		Notes
		-----
		Is called automatically by the Dingle factor calculation thread when it finishes its calculations.
		"""

		self.container_ui.abort_button.setVisible(False)
		self.container_ui.plot_button.setVisible(True)
		self.dingle_temperature, self.dingle_temperature_sd, self.fit_parameters = self.calculation_thread.getResults()
		self.enableUI(True)
		self.callback()
		self.run = False

# UI methods:
	def renderUI(self):
		"""
		Calculate the Dingle factor and update the UI afterwards.

		Notes
		-----
		The actual rendering of the UI is performed by finishRenderUI(),
		which is called automatically when the Dingle factor calculation is finished.
		"""

		if not self.finished_init:
			return

		self.startDingleFactorCalculation()

	def finishRenderUI(self):
		"""
		Update the UI after the Dingle factor has been calculated.

		Notes
		-----
		Called automatically by the Dingle factor calculation thread.
		"""

		if self.calculation_thread.errorEncountered():
			self.error.showMessage("An error was encountered during the calculation.")
			self.run = False
			return

		self.ui.field_T_D.setText(str('{:.4}'.format(self.dingle_temperature)))
		self.ui.field_T_D_sd.setText(str('{:.3}'.format(self.dingle_temperature_sd)))

		if self.validateFields():
			self.plot()

	def plot(self, alternative: bool = False):
		"""
		Updates the on-screen plot.
		"""

		# prevent multiple plots running at the same time:
		if self.is_plotting:
			return
		self.is_plotting = True

		self.enableUI(False)
		self.container_ui.plot_button.setVisible(False)
		self.container_ui.abort_plot_button.setVisible(True)

		marker_style = self.ui.plot_lines.isChecked()
		legend = self.ui.field_legend.currentText()

		index = self.pockets[self.ui.field_change_pocket.currentText()]
		H_min, H_max = self.data.mo_data.filtered_data_list[index].interpolation_magnetic_field_range()
		H_min = self.ui.field_min_H.value() if H_min < self.ui.field_min_H.value() else H_min
		H_max = self.ui.field_max_H.value() if H_max > self.ui.field_max_H.value() else H_max

		self.plot_widget.drawDinglePlot(
			self.data, self.fit_parameters, H_min, H_max, index, marker_style, legend
		)

		self.enableUI(True)
		self.container_ui.plot_button.setVisible(True)
		self.container_ui.abort_plot_button.setVisible(False)

		self.is_plotting = False

	def changeSet(self):
		"""
		Called when changing set.
		"""

		self.data.index = self.ui.field_change_set.currentIndex()
		self.initializePockets()
		self.changePocket()

	def changePocket(self):
		index = self.pockets[self.ui.field_change_pocket.currentText()]
		self.ui.field_m_c.setValue(self.data.mo_data.filtered_data_list[index].lkf_parameters.m_c/Constants.m_e)

		# overwrite min_H with the onset field H if it was calculated previously:
		for oscillation in self.data.lkf_parameters_list:
			if oscillation.pocket_type == self.ui.field_change_set.currentText() and oscillation.onset_field is not None:
				self.ui.field_min_H.setValue(oscillation.onset_field)
				break
		else:
			self.ui.field_min_H.setValue(self.data.mo_data.filtered_data_list[index].interpolation_magnetic_field_range()[0])

		self.autoUpdate()

	def toggleDefaultStates(self):
		"""
		Toggles the offset state based on the plot all state.
		"""

		# Set default state:
		self.ui.auto_update.blockSignals(True)
		self.ui.auto_update.setChecked(MagneticOscillationPage.auto_update)
		self.ui.auto_update.blockSignals(False)

		self.ui.plot_lines.blockSignals(True)
		self.ui.plot_lines.setChecked(MagneticOscillationPage.plot_lines)
		self.ui.plot_lines.blockSignals(False)

		# If the user has enabled the legend, overwrite the default legend state:
		if MagneticOscillationPage.legend_style != 'None':
			self.ui.field_legend.blockSignals(True)
			self.ui.field_legend.setCurrentIndex(MagneticOscillationPage.legend_styles[MagneticOscillationPage.legend_style])
			self.ui.field_legend.blockSignals(False)

# Button methods:
	def buttonSaveValue(self):
		"""
		Saves the currently displayed dingle temperature value to the backend.
		"""

		pocket_type = self.ui.field_change_pocket.currentText()

		for lkf_parameters in self.data.lkf_parameters_list:
			if lkf_parameters.pocket_type == pocket_type:
				lkf_parameters.dingle_temperature = self.dingle_temperature
				break
