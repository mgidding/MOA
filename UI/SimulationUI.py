from qtpy import QtWidgets
from Backend.SimulationFunctions.LKFSimulation import LKFSimulation
from Backend.DataObjects.LKFParameters import LKFParameters
from Backend.DataObjects.Constants import Constants
from Backend.DataObjects.DataWrapper import DataWrapper
from Backend.DataObjects.MOData import MOData
from UI.PageElements import SimulationTabElements
from UI.PageElements import SimulationForm
from UI.PlotUI import PlotMagneticField
from UI.SharedUI import MagneticOscillationPage
from typing import List, Optional


class PlotSimulation(PlotMagneticField):
	"""
	Implements plotting for the Simulation page.
	"""

	def drawMainPlot(
			self, data: DataWrapper, lines: bool = False, multi: bool = False, offset: float = 0, legend_style: str = 'None'):
		"""
		Plot as function of the magnetic field.

		Notes
		-----
		If lines is True, plot lines rather than markers.

		if multi is True, plot all mo_data objects in data.

		offset gives the offset between each plot when multi is True.

		legend_style is 'Annotated', 'Boxed' or 'None'.
		"""

		self.addMagneticFieldPlot("Field [T]", "Amplitude (arb. u.)", offset, legend_style)

		_ = 0
		for mo_data in self.getMODataList(data, multi):
			amplitudes = mo_data.raw_amplitudes + _*offset
			annotation = str(mo_data.temperature) + '[K]'
			self.addLine(mo_data.raw_magnetic_field, amplitudes, lines, label=annotation)
			_ += 1

		self.plot_thread.run()

	def drawAlternativePlot(
			self, data: DataWrapper, lines: bool = False, multi: bool = False, offset: float = 0, legend_style: str = 'None'):
		"""
		Plot as function of the inverse magnetic field.

		Notes
		-----
		If lines is True, plot lines rather than markers.

		if multi is True, plot all mo_data objects in data.

		offset gives the offset between each plot when multi is True.

		legend_style is 'Annotated', 'Boxed' or 'None'.
		"""

		self.addMagneticFieldPlot("Inverse Field [1/T]", "Amplitude", offset, legend_style)

		_ = 0
		for mo_data in self.getMODataList(data, multi):
			amplitudes = mo_data.raw_amplitudes + _*offset
			annotation = str(mo_data.temperature) + '[K]'
			self.addLine(1/mo_data.raw_magnetic_field[::-1], amplitudes[::-1], lines, label=annotation)
			_ += 1

		self.plot_thread.run()


class SimulationData:
	"""
	SimulationData manages a set of LKFParameter objects that exist at single temperature.

	Notes
	-----
	This is needed because the UI tracks some information in the tabs while other information is shared between the tabs.
	This class stores the shared information.
	"""

	min_H = 5.0
	max_H = 30.0
	steps = 1000
	noise_scale = 0.0
	T = [0.0]

	def __init__(self):
		self.tabs: List[SimulationTab] = []


class SimulationTab(QtWidgets.QWidget):
	"""
	SimulationTab class. This is the frontend for a single LKFParameters object.

	Notes
	----------
	LKFParameters gives the LKFParameters object to store data in. If not supplied, a new LKFParameters object is created.

	Notes
	-----
	Can also track a *copy* of an existing LKFParameters by using fromParameters().
	"""

	def __init__(self, autoUpdate: MagneticOscillationPage.autoUpdate, parameters: LKFParameters = LKFParameters()):
		super().__init__()

		# initialising variables:
		self.autoUpdate = autoUpdate
		self.ui = SimulationTabElements.Ui_SimulationTab()
		self.ui.setupUi(self)
		self.lkf_parameters = parameters
		self.spinboxes = [
			self.ui.field_T_D, self.ui.field_F, self.ui.field_max_p,
			self.ui.field_m_c, self.ui.field_g, self.ui.field_A_p
		]

		self.initializeSpinBoxes()
		self.initializeFieldValues()
		self.initializeButtons()

	@classmethod
	def fromParameters(cls, source_lkf_parameters: LKFParameters, autoUpdate: MagneticOscillationPage.autoUpdate):
		"""
		Initialise a SimulationTab object by creating a copy of an existing LKFParameters object.
		"""

		lkf_parameters = LKFParameters(
			frequency=source_lkf_parameters.frequency,
			extremal_area_type=source_lkf_parameters.extremal_area_type,
			max_harmonic=source_lkf_parameters.max_harmonic,
			amplitude_harmonic_dep=source_lkf_parameters.amplitude_harmonic_dependence,
			g_factor=source_lkf_parameters.g_factor,
			m_c=source_lkf_parameters.m_c,
			temperature=source_lkf_parameters.temperature,
			dingle_temperature=source_lkf_parameters.dingle_temperature,
			raw_amplitude=source_lkf_parameters.raw_amplitude,
			pocket_type=source_lkf_parameters.pocket_type
		)
		return cls(autoUpdate, lkf_parameters)

	# Initialisation methods:
	def initializeFieldValues(self):
		"""
		Initialise fields with default values of LKFParameters object
		"""

		if self.lkf_parameters.extremal_area_type == "3D Maximal":
			self.ui.field_a_max.setCurrentIndex(0)
		elif self.lkf_parameters.extremal_area_type == "3D Minimal":
			self.ui.field_a_max.setCurrentIndex(1)
		else:
			self.ui.field_a_max.setCurrentIndex(2)

		# Set ranges:
		for field in self.spinboxes:
			field.setRange(0, 1e8)
		self.ui.field_F.setMinimum(10 ** -self.ui.field_F.decimals())
		self.ui.field_max_p.setMinimum(1)

		# Set default values:
		self.ui.field_F.setValue(self.lkf_parameters.frequency)
		self.ui.field_A_p.setValue(self.lkf_parameters.amplitude_harmonic_dependence)
		self.ui.field_g.setValue(self.lkf_parameters.g_factor)
		self.ui.field_m_c.setValue(self.lkf_parameters.m_c / Constants.m_e)
		self.ui.field_T_D.setValue(self.lkf_parameters.dingle_temperature)
		self.ui.field_max_p.setValue(self.lkf_parameters.max_harmonic)

	def initializeButtons(self):
		"""
		Connect button methods to their buttons.
		"""

		self.ui.field_a_max.currentIndexChanged.connect(self.autoUpdate)
		self.ui.enabled.clicked.connect(self.toggleEnabled)

	def initializeSpinBoxes(self):
		"""
		Initialise all spin box steps sizes.
		"""

		for spinbox in self.spinboxes:
			spinbox.valueChanged.connect(self.autoUpdate)

# Data methods:
	def updateLKFParameters(self):
		"""
		Updates the data of the LKFParameters object.
		"""

		self.lkf_parameters.frequency = self.ui.field_F.value()
		self.lkf_parameters.amplitude_harmonic_dependence = self.ui.field_A_p.value()
		self.lkf_parameters.g_factor = self.ui.field_g.value()
		self.lkf_parameters.m_c = self.ui.field_m_c.value() * Constants.m_e
		self.lkf_parameters.dingle_temperature = self.ui.field_T_D.value()
		self.lkf_parameters.max_harmonic = self.ui.field_max_p.value()
		self.lkf_parameters.extremal_area_type = self.ui.field_a_max.itemText(self.ui.field_a_max.currentIndex())

# UI methods:
	def toggleEnabled(self):
		"""
		Toggles whether the fields in the tab are enabled.
		"""

		state = self.ui.enabled.isChecked()
		for field in self.spinboxes + [self.ui.field_a_max]:
			field.setEnabled(state)
		self.autoUpdate()


class SimulationPage(MagneticOscillationPage):
	"""
	Simulation page class.

	Notes
	-----
	On the Simulation page users can create their own magnetic oscillation data.
	They can add multiple oscillations to each data set (which exists at a single temperature).
	Multiple data sets, existing at different temperatures, can be created.
	"""

	def __init__(self, data: DataWrapper):
		super().__init__(data, SimulationForm.Ui_Form, PlotSimulation, "Magnetic Oscillation Simulation")

		# initialise variables:
		self.active_simulation: Optional[SimulationTab] = None
		self.simulations: List[SimulationTab] = []

	# Initialisation methods:
	def initializePage(self):
		"""
		Calls all initialisation methods in the class and sets the currently displayed set.

		Notes
		-----
		Called automatically when the page is first displayed.
		"""

		self.finished_init = False
		self.simulations: List[SimulationTab] = []
		self.ui.tabWidget.clear()
		self.initializeSpinBoxes()
		self.initializeSimulations()
		self.initializeChangeSet()
		self.initializeFieldValues()
		self.initializeTabs()
		self.addMOData()
		self.finished_init = True
		self.renderUI()

	def initializeFieldValues(self):
		"""
		Initialises all fields in the class with data from SimulationData.
		"""

		# set ranges:
		for field in self.spinboxes:
			field.setRange(0, 1e8)
		self.ui.field_offset.setMinimum(-1e8)
		self.ui.field_steps.setRange(1, int(1e6))
		self.ui.field_change_set.blockSignals(True)
		self.ui.field_noise.setMinimum(0)

		# set default values:
		self.ui.field_change_set.setCurrentIndex(self.data.index)
		self.ui.field_change_set.blockSignals(False)
		self.ui.field_min_H.setValue(SimulationData.min_H)
		self.ui.field_max_H.setValue(SimulationData.max_H)
		self.ui.field_steps.setValue(SimulationData.steps)
		self.ui.field_noise.setValue(SimulationData.noise_scale)
		self.ui.field_T.setValue(SimulationData.T[self.ui.field_change_set.currentIndex()])
		self.ui.field_offset.setValue(0)

	def initializeButtons(self):
		"""
		Connect button methods to their buttons.
		"""

		super().initializeButtons()
		self.ui.toggle_alternative_plot.stateChanged.connect(self.renderUI)
		self.ui.add_tab_button.clicked.connect(self.buttonAddTab)
		self.ui.remove_tab_button.clicked.connect(self.buttonRemoveTab)
		self.ui.add_data_button.clicked.connect(self.buttonAddSimulation)
		self.ui.remove_data_button.clicked.connect(self.buttonRemoveSimulation)

	def initializeSimulations(self):
		"""
		Initialise a new SimulationData object if none exist yet.
		"""

		if len(self.simulations) == 0:
			self.simulations = [SimulationData()]
			self.active_simulation = self.simulations[0]

	def initializeTabs(self):
		"""
		Initialise a new tab if none exist yet.
		"""
		if len(self.active_simulation.tabs) == 0:
			self.active_simulation.tabs = [SimulationTab(self.autoUpdate)]
			self.ui.tabWidget.addTab(self.active_simulation.tabs[0], "Oscillation 1")
			self.active_simulation.tabs[0].ui.enabled.setChecked(True)
			self.ui.tabWidget.resize(self.ui.tabWidget.sizeHint())

			self.resizeScrollArea()

	def initializeChangeSet(self):
		"""
		Adds each data set to the drop down menu so they can be selected.
		"""

		# Create a list of all pocket types that should be added to change_set:
		names: List[str] = []
		for _ in range(1, len(self.simulations) + 1):
			names += ["Simulation " + str(_)]

		# Add the names to the UI:
		self.ui.field_change_set.blockSignals(True)
		self.ui.field_change_set.clear()
		self.ui.field_change_set.addItems(names)
		self.ui.field_change_set.blockSignals(False)

# Validator methods:
	def validatePage(self) -> bool:
		"""
		Validates the state of the page and returns true if the state is acceptable.

		Notes
		-----
		Called automatically when the next button is clicked.
		"""

		if not super().validatePage:
			return False

		# At this point the page is validated.
		# Now we set the final names of each simulation and return True:
		_ = 1
		for mo_data in self.data.mo_data_list:
			mo_data.name = "Simulation " + str(_)
			_ += 1
		self.data.sortMOData()
		return True

	def validateFields(self) -> bool:
		"""
		Validate each field in the page and return true if all are acceptable.
		"""

		# check that min_H != max_H:
		if self.ui.field_min_H.value() >= self.ui.field_max_H.value():
			self.ui.field_min_H.setStyleSheet('QDoubleSpinBox { background-color: #da4453}')
			self.ui.field_max_H.setStyleSheet('QDoubleSpinBox { background-color: #da4453}')
			return False
		else:
			self.ui.field_min_H.setStyleSheet('')
			self.ui.field_max_H.setStyleSheet('')
		return True

# Data methods:
	def updateAllData(self):
		"""
		Updates all data with the on-screen values.
		"""

		self.updateAllMOData()
		self.updateSimulationData()

	def updateAllMOData(self):
		"""
		Updates all MO data with the on-screen values.
		"""

		mo_data_list = []
		for _ in range(len(self.simulations)):
			mo_data_list += [self.generateMOData(_)]
		self.data.mo_data_list = mo_data_list

		active_index = self.ui.field_change_set.currentIndex()
		self.active_simulation = self.simulations[active_index]

	def addMOData(self):
		"""
		Adds a new MOData object using the on-screen values.
		"""

		self.data.mo_data_list += [self.generateMOData()]

	def updateData(self):
		"""
		Updates currently displayed data with the on-screen values.
		"""

		self.updateMOData()
		self.updateSimulationData()

	def updateMOData(self):
		"""
		Sets the active MOData object using the on-screen values.
		"""

		self.data.mo_data = self.generateMOData()

	def generateMOData(self, custom_index: int = None) -> MOData:
		"""
		Generate a MOData object from user input.

		Notes
		-----
		custom_index overwrites index of simulation to create MOData for.

		Uses Backend.Simulation.simulate() to generate the MOData object from LKFParameters.
		"""

		self.updateSimulationData()

		lkf_parameters_list = []

		if custom_index is None:
			simulation = self.active_simulation
			T = SimulationData.T[self.ui.field_change_set.currentIndex()]
		else:
			simulation = self.simulations[custom_index]
			T = SimulationData.T[custom_index]

		for tab in simulation.tabs:
			if tab.ui.enabled.isChecked():
				tab.updateLKFParameters()
				lkf_parameters = tab.lkf_parameters
				lkf_parameters.temperature = T
				lkf_parameters_list += [lkf_parameters]

		return LKFSimulation(
			lkf_parameters_list, SimulationData.min_H, SimulationData.max_H, SimulationData.steps,
			name="Simulation " + str(self.data.index + 1), temperature=float(T), noise_scale=SimulationData.noise_scale)

	def updateSimulationData(self):
		"""
		Update the SimulationData values that are shared between all simulations.
		"""

		SimulationData.min_H = self.ui.field_min_H.value()
		SimulationData.max_H = self.ui.field_max_H.value()
		SimulationData.steps = self.ui.field_steps.value()
		SimulationData.noise_scale = self.ui.field_noise.value()
		SimulationData.T[self.ui.field_change_set.currentIndex()] = self.ui.field_T.value()

	def updateSharedParameters(self):
		"""
		Updates the on-screen shared parameters with values from MOData.
		"""

		self.ui.field_T.setValue(self.data.mo_data.temperature)

	def saveData(self, filename: str):
		"""
		Saves data to user-selected text file.
		"""

		self.data.mo_data.save_raw_data(filename)

	def saveAllData(self, foldername: str):
		"""
		Saves all data sets to user-selected folder.
		"""

		for mo_data in self.data.mo_data_list:
			filename = str(mo_data.name) + " - " + str(mo_data.temperature) + "K.txt"
			mo_data.save_raw_data(foldername + "/" + filename)

# UI methods:
	def renderUI(self):
		"""
		Update the UI whenever the currently selected MOData object gets changed.
		"""

		if not self.finished_init:
			return

		if not self.validateFields():
			return

		if self.ui.plot_all.isChecked():
			self.updateAllMOData()
		else:
			self.updateMOData()

		if self.ui.toggle_alternative_plot.isChecked():
			alternative = True
		else:
			alternative = False
		self.plot(alternative)

	def renderTabUI(self):
		"""
		Update the tab UI when the active simulation is changed.
		"""

		# Clear old tabs.
		self.ui.tabWidget.clear()

		# Set up new tabs.
		index = self.ui.field_change_set.currentIndex()
		self.data.index = index
		self.active_simulation = self.simulations[index]

		# Add each individual tab:
		for _ in range(len(self.active_simulation.tabs)):
			self.ui.tabWidget.addTab(self.active_simulation.tabs[_], ("Oscillation " + str(_+1)))
			self.active_simulation.tabs[_].ui.enabled.setChecked(True)
			self.active_simulation.tabs[_].initializeFieldValues()

	def changeSet(self):
		"""
		Called when changing the currently active simulation.
		"""

		if self.data.index == self.ui.field_change_set.currentIndex():
			return

		# Save old data:
		self.updateData()

		# Update UI to display new tabs:
		self.renderTabUI()
		self.updateSharedParameters()

		self.renderUI()

	def enableUI(self, state: bool):
		"""
		Toggles the UI availability.
		"""

		super().enableUI(state)
		self.ui.tabWidget.setEnabled(state)

# Button methods:
	def buttonAddTab(self):
		"""
		Add a new SimulationTab.

		Notes
		-----
		If possible, the currently active SimulationTab's parameters will be copied.
		"""

		self.finished_init = False
		if len(self.active_simulation.tabs) == 0:
			self.active_simulation.tabs += [SimulationTab(self.autoUpdate)]
		else:
			self.active_simulation.tabs += [
				SimulationTab.fromParameters(self.active_simulation.tabs[-1].lkf_parameters, self.autoUpdate)]

		self.ui.tabWidget.addTab(
			self.active_simulation.tabs[-1],
			("Oscillation " + str(len(self.active_simulation.tabs))))
		self.active_simulation.tabs[-1].ui.enabled.setChecked(True)
		self.finished_init = True
		self.autoUpdate()

	def buttonRemoveTab(self):
		"""
		Remove the currently active SimulationTab
		"""

		if len(self.active_simulation.tabs) == 1:
			return

		self.active_simulation.tabs.pop(self.ui.tabWidget.currentIndex())
		self.ui.tabWidget.removeTab(self.ui.tabWidget.currentIndex())

		for _ in range(len(self.active_simulation.tabs)):
			self.ui.tabWidget.setTabText(_, "Oscillation " + str(_+1))
		self.autoUpdate()

	def buttonAddSimulation(self):
		"""
		Add a new simulation.

		Notes
		-----
		If possible, the currently active simulation's parameters will be copied.
		"""

		self.finished_init = False

		# Copy all SimulationData parameters:
		self.simulations += [SimulationData()]
		# Add a temperature value
		SimulationData.T += [0.0]

		old_simulation = self.active_simulation
		new_simulation = self.simulations[-1]

		# Copy the currently active simulation's parameters:
		for _ in range(len(old_simulation.tabs)):
			# Copy each tab:
			new_simulation.tabs += ([SimulationTab.fromParameters(old_simulation.tabs[_].lkf_parameters, self.autoUpdate)])

		# Add a MOData object for the new simulation
		self.addMOData()

		# Set the new simulation as active:
		self.data.index = len(self.simulations) - 1
		self.active_simulation = new_simulation

		# Update the UI:
		self.initializeChangeSet()
		self.ui.field_change_set.setCurrentIndex(len(self.simulations)-1)
		self.renderTabUI()

		self. finished_init = True

		self.renderUI()

	def buttonRemoveSimulation(self):
		"""
		Removes the currently active simulation.
		"""

		if len(self.simulations) > 1:
			old_index = int(self.ui.field_change_set.currentIndex())
			if old_index == 0:
				new_index = 0
			else:
				new_index = old_index - 1
			SimulationData.T.pop(old_index)
			self.simulations.remove(self.active_simulation)
			self.active_simulation = self.simulations[new_index]
			self.data.mo_data_list.pop(old_index)
			self.data.index = new_index
			self.initializeChangeSet()
			self.ui.field_change_set.setCurrentIndex(new_index)
			self.initializeFieldValues()
			self.renderUI()
