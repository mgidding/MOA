from qtpy import QtWidgets
from qtpy.QtCore import QCoreApplication
from UI.PageElements import FFTAnalysisForm
from UI.PlotUI import PlotFFT
from UI.SharedUI import MagneticOscillationPage
from time import sleep
from platform import system
from math import log10, floor, ceil
from Backend.DataObjects.DataWrapper import DataWrapper
from typing import Union


class PlotFFTAnalysis(PlotFFT):
	def drawMainPlot(
			self, data: DataWrapper, lines: bool = False, multi: bool = False, offset: float = 0, legend_style: str = 'None'):
		"""
		Draw the FFT plot.

		Notes
		-----
		If lines is True, plot lines rather than markers.

		if multi is True, plot all mo_data objects in data.

		offset gives the offset between each plot when multi is True.

		legend_style is 'Annotated', 'Boxed' or 'None'.
		"""

		try:
			self.addFFTPlot(data, lines, multi, offset, legend_style, x_axis="Frequency [T]")
		except:
			self.plot_thread.run()

		self.plot_thread.run()


class FFTAnalysisPage(MagneticOscillationPage):
	"""
	FFTAnalysis page class.

	Notes
	-----
	On the FFTAnalysis page users can change the settings used by the Fourier transform
	and magnetic oscillation finding algorithm.
	"""

	def __init__(self, data: DataWrapper):
		super().__init__(data, FFTAnalysisForm.Ui_Form, PlotFFTAnalysis, "Fourier Transform")

	# Initialisation methods:
	def initializePage(self):
		"""
		Calls all initialisation methods in the class and sets the currently displayed set.

		Notes
		-----
		Called automatically when the page is first displayed.
		"""

		self.ui.plot_all.blockSignals(True)
		self.ui.plot_all.setChecked(False)
		self.ui.plot_all.blockSignals(False)
		super().initializePage()

	def initializeFieldValues(self):
		"""
		Initialises all fields in the class with data from the currently selected MOData object.
		"""

		# Set ranges:
		try:
			self.data.mo_data.fft_bin_size: Union[str, float] = "default"
		except:
			self.error.showMessage("FFT error encountered")
		# noinspection PyTypeChecker
		self.ui.field_bin_size.setMinimum(0.01 * self.data.mo_data.fft_bin_size + 0.0001)
		self.ui.field_bin_size.setMaximum(self.data.mo_data.fft_bin_size)
		self.ui.field_cutoff.setRange(0, 0.999)

		min_freq = self.data.mo_data.fft_full_frequency_range[0]
		max_freq = self.data.mo_data.fft_full_frequency_range[-1]
		self.ui.field_min_freq.setRange(min_freq, max_freq)
		self.ui.field_max_freq.setRange(min_freq, max_freq)

		self.ui.field_bin_size.setDecimals(10)

		self.ui.field_change_set.blockSignals(True)

		# Set default values:
		index = self.data.index
		self.ui.field_change_set.setCurrentIndex(index)
		self.ui.field_change_set.blockSignals(False)
		self.ui.field_bin_size.setValue(ceil(self.data.mo_data.fft_bin_size / 4))
		self.ui.field_cutoff.setValue(self.data.mo_data.fft_filter)
		self.ui.field_min_freq.setValue(min_freq)
		self.ui.field_max_freq.setValue(10 ** floor(log10(max_freq)))
		self.ui.field_window_parameter_1.setValue(1.0)
		self.ui.field_window_parameter_2.setValue(1.0)
		self.ui.field_offset.setValue(0.0)

	def initializeButtons(self):
		"""
		Connect button methods to their buttons.
		"""

		super().initializeButtons()
		self.ui.field_window.currentIndexChanged.connect(self.buttonChangeWindowType)

# Validator methods:
	def validatePage(self) -> bool:
		"""
		Validates the state of the page and returns true if the state is acceptable.

		Notes
		-----
		Called automatically when the next button is clicked.
		"""

		if not super().validatePage():
			return False

		self.updateAllData()
		if not self.calculateFFTs():
			return False
		return True

	def validateFields(self) -> bool:
		"""
		Validate each field in the page and return true if all are acceptable.
		"""

		# check that fft min frequency is lower than the max frequency:
		if self.ui.field_min_freq.value() >= self.ui.field_max_freq.value() - self.data.mo_data.fft_bin_size:

			self.ui.field_min_freq.setStyleSheet('QDoubleSpinBox { background-color: #da4453}')
			self.ui.field_max_freq.setStyleSheet('QDoubleSpinBox { background-color: #da4453}')
			return False
		else:
			self.ui.field_min_freq.setStyleSheet('')
			self.ui.field_max_freq.setStyleSheet('')

		return True

# Data methods:
	def updateAllData(self):
		"""
		Updates the data of each MOData object in the MODataList

		Notes
		-----
		Should be called whenever all data sets should be updated with the current on-screen values.
		"""

		index = self.data.index
		for _ in range(len(self.data.mo_data_list)):
			self.data.index = _
			if not self.updateData():
				self.error.showMessage("FFT error encountered")
				break
		self.data.index = index

	def updateData(self) -> bool:
		"""
		Updates the data of the MOData object that's currently selected with the on-screen values.

		Notes
		-----
		Returns False if an error was encountered.
		"""

		try:
			self.data.mo_data.fft_bin_size = self.ui.field_bin_size.value()

			self.data.mo_data.fft_filter = self.ui.field_cutoff.value()

			self.data.mo_data.fft_frequency_limits(self.ui.field_min_freq.value(), self.ui.field_max_freq.value())

			self.data.mo_data.oscillation_frequency_limits("default", "default")

			self.data.mo_data.fft_window(
				self.ui.field_window.itemText(self.ui.field_window.currentIndex()),
				self.ui.field_window_parameter_1.value(), self.ui.field_window_parameter_2.value()
			)
			_ = self.data.mo_data.fft_amplitude_range  # this triggers FFT calculation

		except:
			return False
		return True

	def calculateFFTs(self) -> bool:
		"""
		Returns True if process was not canceled, else False.
		"""

		index = self.data.index
		progress = QtWidgets.QProgressDialog(
			"Performing FFTs...", "Cancel", 0, len(self.data.mo_data_list), self
		)
		progress.forceShow()
		for _ in range(len(self.data.mo_data_list)):
			progress.setValue(_+1)
			QCoreApplication.processEvents()
			sleep(0.01)
			if progress.wasCanceled():
				self.data.index = index
				return False
			self.data.index = _
			__ = self.data.mo_data.fft_amplitude_range  # this triggers FFT calculation
		if system() == 'Windows':
			QCoreApplication.processEvents()  # Windows needs this else the next button won't work...
		self.data.index = index
		return True

	def saveData(self, filename: str):
		"""
		Saves data to user-selected text file.
		"""

		self.data.mo_data.save_fft_data(filename)

	def saveAllData(self, foldername: str):
		"""
		Saves all data sets to user-selected folder.
		"""

		for mo_data in self.data.mo_data_list:
			filename = str(mo_data.name) + " FFT data.txt"
			mo_data.save_fft_data(foldername + "/" + filename)

# UI Methods:
	def renderUI(self):
		"""
		Update the UI whenever the currently selected MOData object gets changed.
		"""

		if not self.finished_init:
			return

		if self.ui.plot_all.isChecked():
			self.enableUI(False)

		QCoreApplication.processEvents()

		if not self.validateFields():
			self.enableUI(True)
			return

		# set old data:
		if self.ui.plot_all.isChecked():
			self.updateAllData()
			if not self.calculateFFTs():
				self.enableUI(True)
				return
		else:
			self.updateData()

		index = self.ui.field_change_set.currentIndex()
		self.data.index = index

		# set new data:
		if not self.ui.plot_all.isChecked():
			self.updateData()

		self.plot()
		self.enableUI(True)

	def updateWindowUI(self):
		"""
		Updates the UI according to which window type is currently selected.
		"""

		window_type = self.ui.field_window.itemText(self.ui.field_window.currentIndex())
		if window_type in [
			'kaiser', 'gaussian', 'general_gaussian', 'chebwin', 'tukey']:
			self.ui.field_window_parameter_1.setEnabled(True)
			self.ui.label_window_parameter_1.setEnabled(True)
			if window_type == 'general_gaussian':
				self.ui.field_window_parameter_2.setEnabled(True)
				self.ui.label_window_parameter_2.setEnabled(True)
			else:
				self.ui.field_window_parameter_2.setEnabled(False)
				self.ui.label_window_parameter_2.setEnabled(False)
				self.ui.field_window_parameter_2.setValue(1)
		else:
			self.ui.field_window_parameter_1.setEnabled(False)
			self.ui.label_window_parameter_1.setEnabled(False)
			self.ui.field_window_parameter_2.setEnabled(False)
			self.ui.label_window_parameter_2.setEnabled(False)
			self.ui.field_window_parameter_1.setValue(1)
			self.ui.field_window_parameter_2.setValue(1)
			return

		if window_type == "kaiser":
			self.ui.field_window_parameter_1.setRange(-100, 100)
			self.ui.field_window_parameter_2.setRange(-100, 100)
		elif window_type == "gaussian" or window_type == "general_gaussian":
			self.ui.field_window_parameter_1.setMinimum(0.1)
			self.ui.field_window_parameter_2.setMinimum(0.1)
		elif window_type == "chebwin":
			self.ui.field_window_parameter_1.setRange(45, 1000)
			self.ui.field_window_parameter_2.setRange(45, 1000)

		# Set default value(s):
		if window_type == "kaiser":
			self.ui.field_window_parameter_1.setValue(10)
		elif window_type == "gaussian":
			self.ui.field_window_parameter_1.setValue(100000)
		elif window_type == "general_gaussian":
			self.ui.field_window_parameter_1.setValue(1)
			self.ui.field_window_parameter_2.setValue(100000)
		elif window_type == "chebwin":
			self.ui.field_window_parameter_1.setValue(100)

	def changeSet(self):
		"""
		Called when changing set.
		"""

		self.renderUI()

# Button methods:
	def buttonChangeWindowType(self):
		"""
		Updates UI when the window type is changed.

		Notes
		-----
		Called automatically when the window type is changed.
		"""

		self.updateWindowUI()
		self.autoUpdate()

