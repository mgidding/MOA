from UI.PageElements import InterpolationForm
from math import ceil, floor
from UI.PlotUI import PlotMagneticField
from UI.SharedUI import MagneticOscillationPage
from Backend.DataObjects.DataWrapper import DataWrapper
from UI.CustomWidgets import OddIntValidator


class PlotInterpolation(PlotMagneticField):
	"""
	Implements plotting for the Interpolation page
	"""

	def drawMainPlot(
			self, data: DataWrapper, lines: bool = False, multi: bool = False, offset: float = 0, legend_style: str = 'None'):
		"""
		Plot as function of the magnetic field.

		Notes
		-----
		If lines is True, plot lines rather than markers.

		if multi is True, plot all mo_data objects in data.

		offset gives the offset between each plot when multi is True.

		legend_style is 'Annotated', 'Boxed' or 'None'.
		"""

		self.addMagneticFieldPlot("Field [T]", "Amplitude", offset, legend_style)

		_ = 0
		for mo_data in self.getMODataList(data, multi):
			amplitudes = mo_data.raw_amplitudes + _*offset
			if data.mode == "temperature":
				annotation = str(mo_data.temperature) + '[K]'
			else:
				annotation = str(mo_data.angle)
			self.addLine(mo_data.raw_magnetic_field, amplitudes, lines, label=annotation)
			_ += 1

		self.plot_thread.run()

	def drawAlternativePlot(
			self, data: DataWrapper, lines: bool = False, multi: bool = False, offset: float = 0, legend_style: str = 'None'):
		"""
		Plot as function of the inverse magnetic field.

		Notes
		-----
		If lines is True, plot lines rather than markers.

		if multi is True, plot all mo_data objects in data.

		offset gives the offset between each plot when multi is True.

		legend_style is 'Annotated', 'Boxed' or 'None'.
		"""

		self.addMagneticFieldPlot("Inverse Field [1/T]", "Amplitude", offset, legend_style)

		_ = 0
		for mo_data in self.getMODataList(data, multi):
			try:
				amplitudes = mo_data.interpolation_amplitudes + _ * offset
			except:
				self.plot_thread.run()
				return
			if data.mode == "temperature":
				annotation = str(mo_data.temperature) + '[K]'
			else:
				annotation = str(mo_data.angle)
			self.addLine(mo_data.interpolation_inverse_magnetic_field, amplitudes, lines, label=annotation)
			_ += 1

		self.plot_thread.run()


class InterpolationPage(MagneticOscillationPage):
	"""
	Interpolation page class.

	Notes
	-----
	On the interpolation page users can change the settings used by the interpolation algorithm.
	"""

	def __init__(self, data: DataWrapper):
		super().__init__(data, InterpolationForm.Ui_Form, PlotInterpolation, "Data Interpolation")

	# Initialisation methods:
	def initializeFieldValues(self):
		"""
		Initialises all fields in the class with data from the currently selected MOData object.
		"""

		# Set ranges:
		self.ui.field_min_H.setMinimum(floor(self.data.mo_data.raw_magnetic_field[0]))
		self.ui.field_min_H.setMaximum(self.data.mo_data.raw_magnetic_field[-2])
		self.ui.field_max_H.setMinimum(self.data.mo_data.raw_magnetic_field[3])
		self.ui.field_max_H.setMaximum(ceil(self.data.mo_data.raw_magnetic_field[-1]))
		min_interpol_steps = len(self.data.mo_data.raw_magnetic_field) + 1
		self.ui.field_steps.setRange(min_interpol_steps, 1000000)
		self.ui.field_derivative.setRange(0, 5)
		self.ui.field_change_set.blockSignals(True)
		self.ui.field_spline_smoothing.setMinimum(0.0)
		self.ui.field_medfit_kernel_size.setMinimum(1)
		self.ui.field_savgol_poly_order.setMinimum(1)
		self.ui.field_savgol_window_length.setMinimum(3)
		self.ui.field_savgol_deriv.setMinimum(0)

		# Set special validators for fields that only take odd integers:
		self.ui.field_medfit_kernel_size.validator = OddIntValidator()
		self.ui.field_savgol_poly_order.validator = OddIntValidator()
		self.ui.field_savgol_window_length.validator = OddIntValidator()

		# Set default values:
		index = self.data.index
		self.ui.field_change_set.setCurrentIndex(index)
		self.ui.field_change_set.blockSignals(False)
		self.ui.field_min_H.setValue(floor(self.data.mo_data.raw_magnetic_field[0]))
		self.ui.field_max_H.setValue(ceil(self.data.mo_data.raw_magnetic_field[-1]))
		self.ui.field_steps.setValue(len(self.data.mo_data.interpolation_inverse_magnetic_field))
		self.ui.field_derivative.setValue(self.data.mo_data.interpolation_derivative)
		self.ui.field_offset.setValue(0.0)
		self.ui.field_spline_smoothing.setValue(0.1)
		self.ui.field_medfit_kernel_size.setValue(3)
		self.ui.field_savgol_poly_order.setValue(3)
		self.ui.field_savgol_window_length.setValue(5)
		self.ui.field_savgol_deriv.setValue(0)

	def initializeButtons(self):
		"""
		Connect button methods to their buttons.
		"""

		super().initializeButtons()
		self.ui.toggle_alternative_plot.stateChanged.connect(self.renderUI)
		self.ui.field_fit_type.currentIndexChanged.connect(self.autoUpdate)
		self.ui.field_smoothing_type.currentIndexChanged.connect(self.smoothingElementsVisibility)
		self.ui.field_savgol_mode.currentIndexChanged.connect(self.autoUpdate)

	def initializePageElements(self):
		self.smoothingElementsVisibility()

# Validator methods
	def validatePage(self) -> bool:
		"""
		Validates the state of the page and returns true if the state is acceptable.

		Notes
		-----
		Called automatically when the next button is clicked.
		"""

		if not super().validatePage():
			return False

		self.updateAllData()

		for mo_data in self.data.mo_data_list:
			if mo_data.interpolation_error:
				self.error.showMessage(
					"Problem interpolating array of data set '" + mo_data.name +
					"'. Magnetic field range is too small. Refusing to continue. "
					"Make sure the magnetic field column is selected correctly on the previous page.")
				return False

		return True

	def validateFields(self) -> bool:
		"""
		Validate each field in the page and return true if all are acceptable.
		"""

		valid = True
		window_length_valid = True

		# check that min_H != max_H:
		if self.ui.field_min_H.value() >= self.ui.field_max_H.value():
			self.ui.field_min_H.setStyleSheet('QDoubleSpinBox { background-color: #da4453}')
			self.ui.field_max_H.setStyleSheet('QDoubleSpinBox { background-color: #da4453}')
			valid = False
		else:
			self.ui.field_min_H.setStyleSheet('')
			self.ui.field_max_H.setStyleSheet('')

		if self.ui.field_savgol_poly_order.value() >= self.ui.field_savgol_window_length.value():
			self.ui.field_savgol_poly_order.setStyleSheet('QDoubleSpinBox { background-color: #da4453}')
			self.ui.field_savgol_window_length.setStyleSheet('QDoubleSpinBox { background-color: #da4453}')
			valid = False
			window_length_valid = False
		else:
			self.ui.field_savgol_poly_order.setStyleSheet('')
			self.ui.field_savgol_window_length.setStyleSheet('')

		if self.ui.field_savgol_deriv.value() >= self.ui.field_savgol_window_length.value():
			self.ui.field_savgol_deriv.setStyleSheet('QDoubleSpinBox { background-color: #da4453}')
			self.ui.field_savgol_window_length.setStyleSheet('QDoubleSpinBox { background-color: #da4453}')
			valid = False
		else:
			self.ui.field_savgol_deriv.setStyleSheet('')
			if window_length_valid:
				self.ui.field_savgol_window_length.setStyleSheet('')

		if self.ui.field_medfit_kernel_size.value() >= self.ui.field_steps.value():
			self.ui.field_medfit_kernel_size.setStyleSheet('QDoubleSpinBox { background-color: #da4453}')
			self.ui.field_steps.setStyleSheet('QDoubleSpinBox { background-color: #da4453}')
			valid = False
		else:
			self.ui.field_medfit_kernel_size.setStyleSheet('')
			self.ui.field_steps.setStyleSheet('')

		return valid

# Data methods:
	def updateAllData(self):
		"""
		Updates the data of each MOData object in the MODataList

		Notes
		-----
		Should be called whenever all data sets should be updated with the current on-screen values.
		"""

		index = self.data.index
		for _ in range(len(self.data.mo_data_list)):
			self.data.index = _
			error = self.updateData()
			if error:
				self.error.showMessage("Interpolation error encountered")
				break
		self.data.index = index

	def updateData(self) -> bool:
		"""
		Updates the data of the MOData object that's currently selected with the on-screen values.
		"""

		try:
			self.data.mo_data.interpolation_magnetic_field_range(
				self.ui.field_min_H.value(), self.ui.field_max_H.value(), self.ui.field_steps.value())
			self.data.mo_data.interpolation_derivative = self.ui.field_derivative.value()
			self.data.mo_data.interpolation_univariate_spline_degree = (
				self.ui.field_fit_type.itemText(self.ui.field_fit_type.currentIndex()))
			self.data.mo_data.interpolation_smoothing(
				smoothing_type=self.ui.field_smoothing_type.currentText(),
				spline_smoothing=self.ui.field_spline_smoothing.value(),
				medfilt_kernel_size=self.ui.field_medfit_kernel_size.value(),
				savgol_window_length=self.ui.field_savgol_window_length.value(),
				savgol_poly_order=self.ui.field_savgol_poly_order.value(),
				savgol_deriv=self.ui.field_savgol_deriv.value(),
				savgol_mode=self.ui.field_savgol_mode.currentText()
			)
		except:
			return True
		return False

# UI methods:
	def renderUI(self):
		"""
		Update the UI whenever the currently selected MOData object gets changed.
		"""

		if not self.finished_init:
			return

		if not self.validateFields():
			return

		self.updateAllData()
		index = self.ui.field_change_set.currentIndex()
		self.data.index = index

		if self.ui.toggle_alternative_plot.isChecked():
			alternative = True
		else:
			alternative = False
		self.plot(alternative)

	def changeSet(self):
		"""
		Called when changing set.
		"""

		self.renderUI()

	def smoothingElementsVisibility(self):
		"""
		Changes the visibility of the smoothing options depending on which smoothing type is selected.
		"""

		savgol_elements = [
			self.ui.field_savgol_mode, self.ui.label_savgol_mode,
			self.ui.field_savgol_poly_order, self.ui.label_savgol_poly_order,
			self.ui.field_savgol_window_length, self.ui.label_savgol_window_length,
			self.ui.field_savgol_deriv, self.ui.label_savgol_deriv
		]

		spline_elements = [self.ui.field_spline_smoothing, self.ui.label_spline_smoothing]

		medfilt_elements = [self.ui.field_medfit_kernel_size, self.ui.label_medfilt_kernel_size]

		smoothing_type = self.ui.field_smoothing_type.currentText()

		if smoothing_type == "none":
			hidden = savgol_elements + spline_elements + medfilt_elements
			visible = []
		elif smoothing_type == "spline":
			hidden = savgol_elements + medfilt_elements
			visible = spline_elements
		elif smoothing_type == "medfilt":
			hidden = savgol_elements + spline_elements
			visible = medfilt_elements
		else:
			hidden = spline_elements + medfilt_elements
			visible = savgol_elements

		for element in hidden:
			element.setVisible(False)
		for element in visible:
			element.setVisible(True)

# Button methods:
	def saveData(self, filename: str):
		"""
		Saves data to user-selected text file.
		"""

		self.data.mo_data.save_interpolated_data(filename)

	def saveAllData(self, foldername: str):
		"""
		Saves all data sets to user-selected folder.
		"""

		for mo_data in self.data.mo_data_list:
			filename = str(mo_data.name) + " interpolated data.txt"
			mo_data.save_interpolated_data(foldername + "/" + filename)
