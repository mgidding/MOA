from qtpy.QtCore import QThread
from Backend.DataObjects.Constants import Constants
from UI.PageElements import CyclotronMassForm
from Backend.SimulationFunctions.CyclotronMassFunctions import calculateCyclotronMasses, calculateMeanCyclotronMass, \
	temperatureReductionFactor
from numpy import savetxt, array, column_stack
from UI.PlotUI import PlotCanvas
from UI.SharedUI import MagneticOscillationPage
from math import floor, ceil, log10
from typing import List, Callable, Any, Optional
from Backend.DataObjects.DataWrapper import DataWrapper
from Backend.DataObjects.MOData import MOData
from Backend.DataObjects.LKFParameters import LKFParameters
import UI.InitUI


class PlotCyclotronMass(PlotCanvas):
	"""
	Implements plotting for the CyclotronMass page.
	"""

	def drawCyclotronMassPlot(self, m_c_list: List[float], H_avg_list: List[float], lines: bool = False):
		"""
		Draws a plot of the cyclotron mass vs <H>.

		Notes
		----------
		If lines is True, plot lines rather than markers.
		"""

		m_c_list = [m_c / Constants.m_e for m_c in m_c_list]
		self.addPlot(
			"Average magnetic field [T]", "Cyclotron mass/electron mass", y_min=0, legend_items_per_column=18)
		self.addLine(H_avg_list, m_c_list, lines, label="Fit Values")

		self.plot_thread.setParameters(auto_x=lines)
		self.plot_thread.run()

	def drawLKFitPlot(
			self, amplitudes: List[List[float]], temperatures: List[List[float]], fit_parameter: List[float],
			H_list: List[float], H_min_list: List[float], H_max_list: List[float], multi: bool,
			legend_style: str = 'None', offset: float = 0.0):
		"""
		Draws plots of the LK fits.

		Notes
		-----
		legend_style is 'Annotated', 'Boxed' or 'None'.
		"""

		y_axis = "Relative amplitude"
		if offset != 0:
			y_axis += " (arb. u.)"

		self.addPlot("Temperature [K]", y_axis, legend_style=legend_style)

		if multi:
			elements = len(amplitudes)
		else:
			if len(H_list) > 0:
				elements = 1
			else:
				elements = 0

		for _ in range(elements):
			annotation = (
					'{:.3}'.format(str(H_list[_])) + " [T]\n(" +
					'{:.3}'.format(str(H_min_list[_])) + ", " +
					'{:.3}'.format(str(H_max_list[_])) + ")")
			offset_amplitudes = [amplitude + _*offset for amplitude in amplitudes[_]]
			self.addLine(temperatures[_], offset_amplitudes, lines=False)

			offset_fit = [amplitude + _*offset for amplitude in temperatureReductionFactor(temperatures[_], fit_parameter[_])]
			self.addLine(temperatures[_], offset_fit, lines=True, label=annotation)

		self.plot_thread.run()


class CyclotronMassCalculation(QThread):
	"""
	QThread child class that handles running the cyclotron mass calculation on its own thread.
	"""

	def __init__(self):
		QThread.__init__(self)
		self.mo_data_list: Optional[List[MOData]] = []
		self.list_of_lkf_parameters_lists: Optional[List[List[LKFParameters]]] = [[]]
		self.pocket_type: Optional[str] = None
		self.H_min: Optional[float] = None
		self.H_max: Optional[float] = None
		self.fft_min: Optional[float] = None
		self.fft_max: Optional[float] = None
		self.bin_size: Optional[float] = None
		self.inverse_H_step: Optional[float] = None
		self.min_fit_number: Optional[int] = None
		self.calculation_type: Optional[str] = None
		self.m_c_list: Optional[List[float]] = []
		self.m_c_sd_list: Optional[List[float]] = []
		self.H_list: Optional[List[float]] = []
		self.amplitudes_list: Optional[List[List[float]]] = []
		self.temperatures_list: Optional[List[List[float]]] = [[]]
		self.fit_parameters_list: Optional[List[float]] = []
		self.H_min_list: Optional[List[float]] = []
		self.H_max_list: Optional[List[float]] = []
		self.canceled: bool = False
		self.error: bool = False

	def run(self):
		"""
		Method that is called when CyclotronMassCalculation.start() is executed.

		Notes
		-----
		Uses Backend.SimulationFunctions.CyclotronMassCalculation.calculateCyclotronMasses
		"""

		self.error = False
		self.canceled = False
		try:
			(
				self.m_c_list, self.m_c_sd_list, self.H_list,
				self.amplitudes_list, self.temperatures_list, self.fit_parameters_list,
				self.H_min_list, self.H_max_list
			) = (
				calculateCyclotronMasses(
					self.mo_data_list, self.list_of_lkf_parameters_lists, self.pocket_type, self.H_min, self.H_max,
					self.fft_min, self.fft_max, self.bin_size, self.inverse_H_step, self.min_fit_number,
					self.calculation_type, self.isCanceled
				)
			)
		except:
			self.error = True

	def getResults(self) -> (
			List[float], List[float], List[float], List[List[float]],
			List[List[float]], List[float], List[float], List[float]
		):
		return (
			self.m_c_list, self.m_c_sd_list, self.H_list,
			self.amplitudes_list, self.temperatures_list, self.fit_parameters_list,
			self.H_min_list, self.H_max_list
		)

	def errorEncountered(self) -> bool:
		"""
		Returns true if the calculation thread encountered an error.
		"""

		return self.error

	def isCanceled(self) -> bool:
		"""
		Returns whether the calculation is canceled.
		"""

		return self.canceled

	def cancel(self):
		"""
		Cancels the calculation when called.
		"""

		self.canceled = True

	def setParameters(
			self, mo_data_list: List[MOData], list_of_lkf_parameters_lists: List[List[LKFParameters]], pocket_type: str,
			H_min: float, H_max: float, fft_min: float,	fft_max: float, bin_size: float, inverse_H_step: float,
			min_fit_number: int, calculation_type: str
	):
		"""
		Method that sets the parameters used by the cyclotron mass calculation.

		Notes
		----------
		inverse_H_step is not used when calculation type is set to "single".

		min_fit_number is the minimum number of points needed before fitting is attempted. Default is 6.

		calculation_type is either 'fixed_interval', 'fixed_minimum', 'fixed_maximum' or 'single'
		"""

		self.mo_data_list = mo_data_list
		self.list_of_lkf_parameters_lists = list_of_lkf_parameters_lists
		self.pocket_type = pocket_type
		self.H_min = H_min
		self.H_max = H_max
		self.fft_min = fft_min
		self.fft_max = fft_max
		self.bin_size = bin_size
		self.inverse_H_step = inverse_H_step
		self.min_fit_number = min_fit_number
		self.calculation_type = calculation_type


class CyclotronMassPage(MagneticOscillationPage):
	"""
	CyclotronMass page class.

	Notes
	-----
	On the cyclotron mass page users can change the settings used by the cyclotron mass estimation algorithm.
	"""

	# Dict to look up estimation types for backend.
	estimation_types = ['fixed_minimum', 'fixed_maximum', 'fixed_interval', 'single']

	def __init__(self, data: DataWrapper):
		super().__init__(data, CyclotronMassForm.Ui_Form, PlotCyclotronMass, "Cyclotron Mass Calculation")

		# initialize variables:
		self.m_c_list: Optional[List[float]] = []
		self.m_c_sd_list: Optional[List[float]] = []
		self.H_list: Optional[List[float]] = []
		self.amplitudes_list: Optional[List[float]] = []
		self.temperatures_list: Optional[List[List[float]]] = [[]]
		self.fit_parameters_list: Optional[List[float]] = []
		self.H_min_list: Optional[List[float]] = []
		self.H_max_list: Optional[List[float]] = []
		self.pocket_type: Optional[str] = None
		self.estimation_type: Optional[str] = None
		self.H_min: Optional[float] = None
		self.H_max: Optional[float] = None
		self.interval: Optional[float] = None
		self.min_num: Optional[int] = None
		self.fft_min: Optional[float] = None
		self.fft_max: Optional[float] = None
		self.bin_size: Optional[float] = None
		self.m_c: float = 0.0
		self.m_c_sd: float = 0.0
		self.pocket_types: List[str] = []
		self.run: bool = True
		self.callback: Optional[Callable[[None], ...]] = None
		self.change_set_index: int = 0
		self.save_data_index: int = 0
		self.save_data_filename: Optional[str] = None
		self.foldername: Optional[str] = None

		# Set up threading:
		self.calculation_thread: QThread = CyclotronMassCalculation()
		self.calculation_thread.finished.connect(self.finishCyclotronMassCalculation)

	# Initialisation methods:
	def initializeChangeSet(self):
		"""
		Adds each identified pocket to the drop down menu so they can be selected.
		"""

		# Disconnect the plot function, else plot will be called when we update the change_set item list.
		try:
			self.ui.field_change_set.currentIndexChanged.disconnect()
		except TypeError:
			pass

		# Create a list of all pocket types that should be added to change_set:
		self.pocket_types: List[str] = []
		_ = 0
		for LKFParametersList in self.data.list_of_lkf_parameters_lists:
			__ = 0
			for lkf_parameters in LKFParametersList:
				if lkf_parameters.active is False:
					continue
				pocket_type = lkf_parameters.pocket_type
				if pocket_type not in self.pocket_types and pocket_type is not None:
					self.pocket_types += [pocket_type]
				__ += 1
			_ += 1

		# Add the pockets to the UI and (re)connect the plot function to change_set.
		self.ui.field_change_set.blockSignals(True)
		self.ui.field_change_set.clear()
		self.ui.field_change_set.addItems(self.pocket_types)
		self.ui.field_change_set.currentIndexChanged.connect(self.changeSet)
		self.ui.field_change_set.blockSignals(False)

	def initializePage(self):
		"""
		Calls all initialisation methods in the class and sets the currently displayed set.

		Notes
		-----
		Called automatically when the page is first displayed.
		"""

		self.run = True
		self.container_ui.plot_button.setText("Calculate")

		# Show "Calculating" during initial plot:
		self.plot_widget.addPlot(
			"Average magnetic field [T]", "Cyclotron mass/electron mass")
		# Draw a single white dot as we need to draw something
		self.plot_widget.addLine(0, 0, False, style='w*')
		self.plot_widget.addAnnotation("Calculating!", 0, 0)
		self.plot_widget.showPlots()

		super().initializePage()

	def initializeFieldValues(self):
		"""
		Initialises all fields in the class with data from the currently selected MOData object.
		"""

		# Set ranges:
		self.ui.field_min_H.setRange(
			1/self.data.mo_data.interpolation_inverse_magnetic_field[-2],
			1/self.data.mo_data.interpolation_inverse_magnetic_field[0]
		)
		self.ui.field_max_H.setRange(
			1/self.data.mo_data.interpolation_inverse_magnetic_field[-1],
			ceil(1/self.data.mo_data.interpolation_inverse_magnetic_field[1])
		)
		self.ui.field_interval.setRange(
			0.001, self.data.mo_data.raw_magnetic_field[-1] - self.data.mo_data.raw_magnetic_field[0]
		)

		self.ui.field_min_num_points.setMinimum(1)

		min_freq = self.data.mo_data.fft_full_frequency_range[0]
		max_freq = self.data.mo_data.fft_full_frequency_range[-1]
		self.ui.field_min_freq.setRange(min_freq, max_freq)
		self.ui.field_max_freq.setRange(min_freq, max_freq)
		self.ui.field_bin_size.setMinimum(0.5)
		self.ui.field_bin_size.setDecimals(10)

		# Set default values:
		for oscillation in self.data.lkf_parameters_list:
			if oscillation.pocket_type == self.ui.field_change_set.currentText() and oscillation.onset_field is not None:
				self.ui.field_min_H.setValue(oscillation.onset_field)
				break
		else:
			min_H = 1 / self.data.mo_data.interpolation_inverse_magnetic_field[-1]
			self.ui.field_min_H.setValue(floor(min_H))

		self.ui.field_max_H.setValue(ceil(1 / self.data.mo_data.interpolation_inverse_magnetic_field[0]))
		self.ui.field_interval.setValue(5)
		points = len(self.data.mo_data_list)
		if points > 6:
			self.ui.field_min_num_points.setValue(6)
		else:
			self.ui.field_min_num_points.setValue(points)
		self.ui.field_offset.setValue(0)

		fft_min, fft_max = self.data.mo_data.fft_frequency_limits()
		self.ui.field_min_freq.setValue(floor(fft_min))
		self.ui.field_max_freq.setValue(10**ceil(log10(fft_max)))
		self.ui.field_bin_size.setValue(ceil(self.data.mo_data.fft_bin_size))

	def initializeButtons(self):
		"""
		Connect button methods to their buttons.
		"""

		super().initializeButtons()
		self.ui.field_estimation_type.currentIndexChanged.connect(self.buttonEstimationType)
		self.ui.toggle_alternative_plot.clicked.connect(self.buttonPlotLKFit)
		self.container_ui.abort_button.clicked.connect(self.terminateCyclotronMassCalculation)
		self.ui.button_save.clicked.connect(self.buttonSaveValue)

# Validator methods:
	def validatePage(self) -> bool:
		if not super().validateFields():
			return False
		self.updateBackend()
		return True

	def validateFields(self) -> bool:
		"""
		Validate each field in the page and return true if all are acceptable.
		"""

		# Check that the min magnetic field is lower than the max and that the interval is not too big:
		if self.ui.field_min_H.value() >= self.ui.field_max_H.value():
			self.ui.field_min_H.setStyleSheet('QDoubleSpinBox { background-color: #da4453}')
			self.ui.field_max_H.setStyleSheet('QDoubleSpinBox { background-color: #da4453}')
			return False
		else:
			self.ui.field_min_H.setStyleSheet('')
			self.ui.field_max_H.setStyleSheet('')

		# check that fft min frequency is lower than the max frequency:
		if self.ui.field_min_freq.value() >= self.ui.field_max_freq.value() - self.data.mo_data.fft_bin_size:

			self.ui.field_min_freq.setStyleSheet('QDoubleSpinBox { background-color: #da4453}')
			self.ui.field_max_freq.setStyleSheet('QDoubleSpinBox { background-color: #da4453}')
			return False
		else:
			self.ui.field_min_freq.setStyleSheet('')
			self.ui.field_max_freq.setStyleSheet('')
		return True

	def nextId(self) -> int:
		"""
		Decide which Wizard page to load next based on page state.

		Notes
		-----
		nextId() is an internal PyQt method that can be reimplemented to change which QWizard page is loaded next.
		"""

		if MagneticOscillationPage.dingle_temp_calculation:
			return UI.InitUI.Wizard.filter_calculation
		else:
			return UI.InitUI.Wizard.results

# Data methods:
	def startCyclotronMassCalculation(self, custom_callback: Callable[[None], Any] = None):
		"""
		Sets up and starts the cyclotron mass calculation.

		Notes
		-----
		custom_callback is the function to call when calculation is finished.
		"""

		pocket_type = self.ui.field_change_set.currentText()
		if self.pocket_type != pocket_type:
			self.pocket_type = pocket_type
			self.run = True

		estimation_type = CyclotronMassPage.estimation_types[self.ui.field_estimation_type.currentIndex()]
		if self.estimation_type != estimation_type:
			self.estimation_type = estimation_type
			self.run = True

		H_min = self.ui.field_min_H.value()
		if self.H_min != H_min:
			self.H_min = H_min
			self.run = True

		H_max = self.ui.field_max_H.value()
		if self.H_max != H_max:
			self.H_max = H_max
			self.run = True

		interval = self.ui.field_interval.value()
		if self.interval != interval:
			self.interval = interval
			self.run = True

		min_num = self.ui.field_min_num_points.value()
		if self.min_num != min_num:
			self.min_num = min_num
			self.run = True

		fft_min = self.ui.field_min_freq.value()
		if self.fft_min != fft_min:
			self.fft_min = fft_min
			self.run = True

		fft_max = self.ui.field_max_freq.value()
		if self.fft_max != fft_max:
			self.fft_max = fft_max
			self.run = True

		bin_size = self.ui.field_bin_size.value()
		if self.bin_size != bin_size:
			self.bin_size = bin_size
			self.run = True

		if custom_callback is None:
			self.callback = self.finishRenderUI
		else:
			self.callback = custom_callback

		if not self.run:
			self.callback()
		else:
			self.calculation_thread.setParameters(
				self.data.mo_data_list, self.data.list_of_lkf_parameters_lists, pocket_type, H_min, H_max, fft_min,
				fft_max, bin_size, interval, min_num, estimation_type
			)
			self.container_ui.abort_button.setVisible(True)
			self.container_ui.plot_button.setVisible(False)
			self.enableUI(False)
			self.calculation_thread.start()

	def terminateCyclotronMassCalculation(self):
		"""
		Terminates the cyclotron mass calculation.
		"""

		self.calculation_thread.cancel()
		self.calculation_thread.quit()
		self.calculation_thread.wait(1000)
		self.container_ui.abort_button.setVisible(False)
		self.container_ui.plot_button.setVisible(True)
		self.enableUI(True)

	def finishCyclotronMassCalculation(self):
		"""
		Processes the result of the cyclotron mass calculation and updates the UI.

		Notes
		-----
		Is called automatically by the cyclotron mass calculation thread when it finishes its calculations.
		"""

		self.container_ui.abort_button.setVisible(False)
		self.container_ui.plot_button.setVisible(True)
		self.enableUI(True)

		if self.calculation_thread.errorEncountered():
			self.error.showMessage("An error was encountered during the calculation.")
			self.run = False
			return

		(
			self.m_c_list, self.m_c_sd_list, self.H_list,
			self.amplitudes_list, self.temperatures_list, self.fit_parameters_list,
			self.H_min_list, self.H_max_list
		) = self.calculation_thread.getResults()
		self.callback()
		self.run = False

	def updateBackend(self):
		"""
		Updates the backend with the calculated cyclotron mass.
		"""

		try:
			valid_m_c = float(self.ui.field_mean_m_c.text()) > 0
		except ValueError:
			valid_m_c = False

		if self.pocket_type != "None" and valid_m_c:
			for oscillation in self.data.lkf_parameters_list:
				if oscillation.pocket_type == self.pocket_type:
					oscillation.m_c = float(self.ui.field_mean_m_c.text())*Constants.m_e
					break

# UI methods:
	def plot(self, alternative: bool = False):
		"""
		Updates the on-screen plot.
		"""

		# prevent multiple plots running at the same time:
		if self.is_plotting:
			return
		self.is_plotting = True

		self.enableUI(False)
		self.container_ui.plot_button.setVisible(False)
		self.container_ui.abort_plot_button.setVisible(True)

		multi = self.ui.plot_all.isChecked()
		marker_style = self.ui.plot_lines.isChecked()
		offset = self.ui.field_offset.value()
		legend = self.ui.field_legend.currentText()

		if self.ui.toggle_alternative_plot.isChecked():
			self.plot_widget.drawLKFitPlot(
				self.amplitudes_list, self.temperatures_list, self.fit_parameters_list,
				self.H_list, self.H_min_list, self.H_max_list, multi, legend, offset)
		else:
			self.plot_widget.drawCyclotronMassPlot(self.m_c_list, self.H_list, marker_style)

		self.enableUI(True)
		self.container_ui.plot_button.setVisible(True)
		self.container_ui.abort_plot_button.setVisible(False)

		self.is_plotting = False

	def renderUI(self):
		"""
		Calculate the cyclotron mass and update the UI afterwards.

		Notes
		-----
		The actual rendering of the UI is performed by finishRenderUI(),
		which is called automatically when the cyclotron mass calculation is finished.
		"""

		if not self.finished_init:
			return

		self.startCyclotronMassCalculation()

	def finishRenderUI(self):
		"""
		Update the UI after the cyclotron mass has been calculated.

		Notes
		-----
		Called automatically by the cyclotron mass calculation thread.
		"""

		self.m_c, self.m_c_sd = calculateMeanCyclotronMass(self.m_c_list, self.m_c_sd_list, normalised=True)
		self.ui.field_mean_m_c.setText(str('{:.4}'.format(self.m_c)))
		self.ui.field_mean_m_c_sd.setText(str('{:.3}'.format(self.m_c_sd)))
		self.plot()

	def enableUI(self, state):
		"""
		Toggles the UI elements so that the UI becomes inactive (except for the abort button) when calculating.
		"""

		super().enableUI(state)
		if state and not self.container_ui.ui_scroll_area.isEnabled():
			self.togglePlotLKFit()
			self.togglePlotAllFits()

	def togglePlotLKFit(self):
		""""
		Toggles UI elements when the LK Fit button is toggled.
		"""

		if self.ui.toggle_alternative_plot.isChecked():
			self.ui.field_legend.setEnabled(True)
			self.ui.label_legend.setEnabled(True)
			self.ui.plot_all.setEnabled(True)
			self.ui.plot_lines.setDisabled(True)
		else:
			self.ui.field_legend.setDisabled(True)
			self.ui.label_legend.setDisabled(True)
			self.ui.plot_all.setDisabled(True)
			self.ui.plot_all.setChecked(False)
			self.ui.plot_lines.setEnabled(True)

	def togglePlotAllFits(self):
		"""
		Toggles show LK Fit button if plot all is toggled.
		"""

		if self.ui.plot_all.isChecked():
			self.ui.toggle_alternative_plot.setChecked(True)
			self.ui.field_legend.setEnabled(True)
			self.ui.label_legend.setEnabled(True)
			self.ui.field_offset.setEnabled(True)
			self.ui.label_offset.setEnabled(True)
		else:
			self.ui.field_offset.setDisabled(True)
			self.ui.label_offset.setDisabled(True)

	def changeSet(self):
		"""
		Called when changing set.
		"""

		self.updateBackend()

		for oscillation in self.data.lkf_parameters_list:
			if oscillation.pocket_type == self.ui.field_change_set.currentText() and oscillation.onset_field is not None:
				self.ui.field_min_H.setValue(oscillation.onset_field)
				break
		else:
			min_H = 1 / self.data.mo_data.interpolation_inverse_magnetic_field[-1]
			self.ui.field_min_H.setValue(floor(min_H))

		self.autoUpdate()

# Button methods:
	def buttonPlotLKFit(self):
		""""
		Toggles UI elements when the LK Fit button is toggled and then plots.
		"""

		self.togglePlotLKFit()
		self.renderUI()

	def buttonEstimationType(self):
		"""
		Handles toggling of UI elements when estimation type is changed.
		"""

		if self.ui.field_estimation_type.currentText() == "Single":
			self.ui.field_interval.setEnabled(False)
			self.ui.label_interval.setEnabled(False)
		else:
			self.ui.field_interval.setEnabled(True)
			self.ui.label_interval.setEnabled(True)
		if (
			self.ui.field_estimation_type.currentText() == "Fixed 1/H Interval"
		):
			self.ui.label_interval.setText("1/(Magnetic Field) Interval")
			self.ui.field_interval.setRange(
				0.001, self.data.mo_data.interpolation_inverse_magnetic_field[-1] -
				self.data.mo_data.interpolation_inverse_magnetic_field[0]
			)
		else:
			self.ui.label_interval.setText("Magnetic Field Interval")
			self.ui.field_interval.setRange(
				0.001, self.data.mo_data.raw_magnetic_field[-1] - self.data.mo_data.raw_magnetic_field[0]
			)
			self.ui.field_interval.setValue(5)

		self.autoUpdate()

	def buttonSaveValue(self):
		"""
		Saves the currently displayed cyclotron mass value to the backend.
		"""

		pocket_type = self.ui.field_change_set.currentText()

		for lkf_parameters_list in self.data.list_of_lkf_parameters_lists:
			for lkf_parameters in lkf_parameters_list:
				if lkf_parameters.pocket_type == pocket_type:
					lkf_parameters.m_c = self.m_c*Constants.m_e

	def saveData(self, filename: str):
		"""
		Saves data to user-selected text file.
		"""

		m_c, m_c_sd = calculateMeanCyclotronMass(self.m_c_list, self.m_c_sd_list, normalised=False)
		names = array((["Mean"] + [str(H) for H in self.H_list]))
		masses = array(([m_c] + self.m_c_list))
		standard_deviation = array(([m_c_sd] + self.m_c_sd_list))
		output = column_stack((names, masses, standard_deviation))

		savetxt(
			filename, output, delimiter="	", fmt="%s",
			header="Average Magnetic Field [T]  Cyclotron Mass [kg] Cyclotron Mass standard deviation [kg]",
			comments="First data point is the mean and does not have an average magnetic field associated with it.\n")

	def saveAllData(self, foldername: str):
		"""
		Saves all data sets to user-selected folder.
		"""

		self.change_set_index = self.ui.field_change_set.currentIndex()
		self.save_data_index = 0
		self.ui.field_change_set.setCurrentIndex(0)
		self.foldername = foldername
		self.save_data_filename = self.foldername + "/" + str(self.pocket_types[0]) + " cyclotron masses.txt"
		self.startCyclotronMassCalculation(custom_callback=self.saveAllDataHelper)

	def saveAllDataHelper(self):
		"""
		Loops over all data sets and calls saveData() on all of them.
		"""

		self.saveData(self.save_data_filename)
		if self.save_data_index < len(self.pocket_types)-1:
			self.save_data_index += 1
			self.save_data_filename = self.foldername + "/" + str(
				self.pocket_types[self.save_data_index]) + " cyclotron masses.txt"
			self.ui.field_change_set.setCurrentIndex(self.save_data_index)
			self.startCyclotronMassCalculation(custom_callback=self.saveAllDataHelper)
		else:
			self.ui.field_change_set.setCurrentIndex(self.change_set_index)
