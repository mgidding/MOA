from qtpy import QtWidgets
from qtpy.QtCore import Qt, QCoreApplication
from Backend.DataObjects.MOData import FileError
from Backend.DataObjects.DataWrapper import DataWrapper
from Backend.DataObjects.FileData import FileData
import UI.InitUI
from UI.PageElements import ChoicePageElements
from UI.SharedUI import MagneticOscillationPage
from numpy import genfromtxt, isnan, ndarray
from platform import system
from typing import List, Optional
from math import ceil


class ChoicePage(QtWidgets.QWizardPage):
	"""
	Choice page class.

	Notes
	-----
	On the choice page users decide whether to simulate magnetic oscillation data or use imported data.
	Data importing is also handled by this class.
	"""

	next_button_enabled: QtWidgets.QAbstractButton.isEnabled = None

	def __init__(self, data: DataWrapper):
		super().__init__()

		# initialising variables:
		self.ui = ChoicePageElements.Ui_ChoiceElements()
		self.ui.setupUi(self)
		self.files: List[UI.ChoiceUI.FileData] = []
		self.data: DataWrapper = data
		self.table_items: int = 0
		self.name_file: Optional[ndarray] = None
		self.error = QtWidgets.QErrorMessage(self)

		self.ui.tableWidget.setAlternatingRowColors(True)

		self.initializeButtons()
		self.initializeFieldValues()
		self.buttonToggle()

	# Initialisation methods:
	def initializePage(self):
		"""
		Initialises all page elements.
		"""

		self.setTitle("Data Selection")
		self.ui.field_scaling.setValue(1)
		# Resize table headers to fit:
		self.ui.tableWidget.horizontalHeader().setSectionResizeMode(QtWidgets.QHeaderView.Stretch)

		# Resize the scroll area to fit the contents:
		self.ui.scrollAreaWidgetContents.resize(self.ui.scrollAreaWidgetContents.sizeHint())
		# Set the scroll area's maximum width equal to that of the scroll area plus spacing:
		self.ui.scrollArea.setMaximumWidth(ceil((
			self.ui.scrollAreaWidgetContents.width() + 2 * self.ui.scrollArea.frameWidth() +
			self.ui.scrollArea.horizontalScrollBar().width())*1.2)
		)
		self.ui.scrollArea.setMaximumHeight(
			self.ui.scrollAreaWidgetContents.height() + 2 * self.ui.scrollArea.frameWidth() +
			self.ui.scrollArea.verticalScrollBar().width()
		)

	def initializeFieldValues(self):
		"""
		Initialises all fields in the class with data from the currently selected MOData object.
		"""

		self.ui.field_scaling.setRange(0.001, 1.0)

	def initializeButtons(self):
		"""
		Connect button methods to their buttons.
		"""

		self.ui.button_add_file.clicked.connect(self.buttonAddFiles)
		self.ui.button_remove_file.clicked.connect(self.buttonRemoveFiles)
		self.ui.tableWidget.itemChanged.connect(self.buttonTableItemChanged)
		self.ui.button_import.toggled.connect(self.buttonToggle)
		self.ui.button_temp_dep.toggled.connect(self.buttonToggleMode)
		self.ui.button_apply_to_all.clicked.connect(self.buttonApplyToAll)
		self.ui.button_name_file.clicked.connect(self.buttonAddNameFile)
		self.ui.button_apply_name_file.clicked.connect(self.buttonApplyNameFile)

	# Validator methods:
	def validatePage(self) -> bool:
		"""
		Validates the state of the page and returns true if the state is acceptable.

		Notes
		-----
		Called automatically when the next button is clicked.
		"""

		# make sure we don't use any old data
		self.data.mo_data_list = []
		self.data.list_of_lkf_parameters_lists = []
		self.data.index = 0

		if self.ui.button_import.isChecked():
			# Check all files:
			if not self.validateFiles():
				return False
			else:
				if len(self.files) == 0:
					return False
				ChoicePage.next_button_enabled(False)
				# Create MOData:
				progress = QtWidgets.QProgressDialog("Reading files...", "Cancel", 0, len(self.files), self)
				progress.setMinimumDuration(100)
				_ = 0
				for file in self.files:
					_ += 1
					progress.setValue(_)
					QCoreApplication.processEvents()  # Force the UI to update.
					try:
						self.data.mo_data_list += [file.MOData()]
					except:
						progress.cancel()
						self.error.showMessage(
							"Problem loading arrays from file '" + file.name + "'. "
							"Refusing to continue. "
							"Make sure the magnetic field column is selected correctly.")
						ChoicePage.next_button_enabled(True)
						return False
					if progress.wasCanceled():
						ChoicePage.next_button_enabled(True)
						return False
				# Set the scaling factor on all data sets:
				for data in self.data.mo_data_list:
					data.raw_magnetic_field_scaling_factor = self.ui.field_scaling.value()
				if system() == 'Windows':
					QCoreApplication.processEvents()  # Windows needs this else the next button won't work...
		else:
			self.data.mode = "temperature"  # simulation is always in temperature mode
		self.data.sortMOData()
		MagneticOscillationPage.onset_field_detection = self.ui.onset_detection.isChecked()
		MagneticOscillationPage.cyclotron_mass_detection = self.ui.cyclotron_mass_detection.isChecked()
		MagneticOscillationPage.dingle_temp_calculation = self.ui.dingle_temp_detection.isChecked()
		ChoicePage.next_button_enabled(True)

		# Set default index:
		self.data.index = 0
		return True

	def validateFiles(self) -> bool:
		"""
		Validate all ChoiceData files
		"""

		if len(self.files) == 0:
			return False

		for file in self.files:
			_, __ = file.columns
			if _ == __:
				return False
		return True

	def nextId(self) -> int:
		"""
		Decide which Wizard page to load next based on page state.

		Notes
		-----
		nextId() is an internal PyQt method that can be reimplemented to change which QWizard page is loaded next.
		"""

		if self.ui.button_simulate.isChecked():
			return UI.InitUI.Wizard.simulation
		else:
			return UI.InitUI.Wizard.interpolation

# Data methods:
	def generateTable(self):
		"""
		Generate a table containing all user-loaded data.

		Notes
		-----
		Generates table from MOData objects that were previously added by buttonAddFiles().
		"""

		self.ui.tableWidget.clearContents()

		rows = len(self.files)
		self.ui.tableWidget.setRowCount(rows)

		for _ in range(rows):
			# Set name column:
			name = QtWidgets.QTableWidgetItem()
			name.setText(self.files[_].name)
			self.ui.tableWidget.setItem(_, 0, name)

			# Set parameter (temperature or angle) column:
			parameter = QtWidgets.QTableWidgetItem()
			if self.data.mode == "temperature":
				if self.files[_].temperature is not None:
					parameter.setData(Qt.EditRole, float(self.files[_].temperature))
				else:
					parameter.setData(Qt.EditRole, float(0))
			else:
				if self.files[_].angle is not None:
					parameter.setData(Qt.EditRole, float(self.files[_].angle))
				else:
					parameter.setData(Qt.EditRole, float(0))
			self.ui.tableWidget.setItem(_, 1, parameter)

			# Set H-Field and Amplitude items
			H_field = QtWidgets.QComboBox()
			amplitude = QtWidgets.QComboBox()
			boxes = [H_field, amplitude]

			for box in boxes:
				box.addItems(self.files[_].headers)
				box.setStyleSheet("""
				QComboBox {
				margin-right: 5px;
				margin-left: 5px;
				}""")
				# noinspection PyUnresolvedReferences
				box.currentIndexChanged.connect(self.buttonTableComboBoxChanged)
				box.setSizeAdjustPolicy(QtWidgets.QComboBox.AdjustToMinimumContentsLengthWithIcon)

			# set current indexes:
			H_column, amplitude_column = self.files[_].columns
			H_field.setCurrentIndex(H_column)
			amplitude.setCurrentIndex(amplitude_column)

			self.ui.tableWidget.setCellWidget(_, 2, H_field)
			self.ui.tableWidget.setCellWidget(_, 3, amplitude)

		# Resize headers to fit:
		self.ui.tableWidget.horizontalHeader().setSectionResizeMode(QtWidgets.QHeaderView.Stretch)

# Button methods:
	def buttonAddFiles(self):
		"""
		Open a QFileDialog and try reading user selected files.

		Notes
		-----
		Works for lists of files too.
		Resulting MOData will have its name set to the file name.
		Will try to extract temperature from the filename using a regexp.
		"""

		files = QtWidgets.QFileDialog.getOpenFileNames()[0]
		if not files:
			return
		error = False

		if self.ui.button_temp_dep.isChecked():
			mode = "temperature"
		else:
			mode = "angle"

		for file in files:
			try:
				file = FileData(file, mode=mode)
			except FileError:
				error = True
				continue
			self.files += [file]
		if error:
			self.error.showMessage("One or more files could not be loaded.")
		self.generateTable()

	def buttonRemoveFiles(self):
		"""
		Remove user-selected table row(s).
		"""

		# find the row index of each selected item.
		selected_items = self.ui.tableWidget.selectedItems()

		# add the rows to a list and sort in reverse order
		rows = []
		for item in selected_items:
			row = item.row()
			if row not in rows:
				rows += [row]
		rows.sort(reverse=True)

		# pop the rows
		for row in rows:
			self.files.pop(row)

		self.generateTable()

	def buttonTableItemChanged(self, item: QtWidgets.QTableWidgetItem):
		"""
		Update MOData name and/or temperature identifiers when user changed them in the table.
		"""

		row = item.row()
		column = item.column()

		if column == 0:
			self.files[row].name = str(item.text())
		elif column == 1:
			if self.data.mode == "temperature":
				self.files[row].temperature = float(item.text())
			else:
				self.files[row].angle = float(item.text())

	def buttonTableComboBoxChanged(self):
		"""
		Update the column selection in the backend when they are changed in the frontend.
		"""

		item = self.ui.tableWidget.currentIndex()
		if item is None:
			return
		row = item.row()
		column = item.column()

		combobox = self.ui.tableWidget.cellWidget(row, column)

		if column == 2:
			self.files[row].magnetic_field_column = combobox.currentIndex()

		elif column == 3:
			self.files[row].amplitude_column = combobox.currentIndex()

	def buttonToggle(self):
		"""
		Toggle the visibility of the table widget and add/remove data buttons.
		"""

		# Toggle UI visibility
		state = self.ui.button_import.isChecked()
		self.ui.tableWidget.setEnabled(state)
		self.ui.scrollArea.setEnabled(state)
		self.ui.frame_analysis_mode.setEnabled(state)

		if state:
			# If user wants to import data, make sure all previous simulation data is removed:
			self.data.mo_data_list = []

	def buttonToggleMode(self):
		"""
		Toggle between temperature and angle analysis modes.
		"""

		if self.ui.button_angle_dep.isChecked():
			self.ui.tableWidget.horizontalHeaderItem(1).setText("Angle")
			self.ui.frame_detection_options.setDisabled(True)
			self.data.mode = "angle"
			mode = "angle"
		else:
			self.ui.tableWidget.horizontalHeaderItem(1).setText("Temperature [K]")
			self.ui.frame_detection_options.setEnabled(True)
			self.data.mode = "temperature"
			mode = "temperature"

		# Remove all loaded data when changing mode:
		for file in self.files:
			if mode == "angle":
				file.angle = file.temperature
				file.temperature = 0.0
			else:
				file.temperature = file.angle
				file.angle = 0.0

		self.generateTable()

	def buttonApplyToAll(self):
		"""
		Applies the column selection of the selected row to all rows.
		"""

		selected_items = self.ui.tableWidget.currentIndex()

		row = selected_items.row()
		if row == -1:
			return

		H_column, amplitude_column = self.files[row].columns

		for file in self.files:
			file.magnetic_field_column = H_column
			file.amplitude_column = amplitude_column

		self.generateTable()

	def buttonAddNameFile(self):
		"""
		Adds a name file.

		Notes
		-----
		Assumes the order of the file is the same as the order the data files are loaded in.
		"""

		header_contents: Optional[list] = None
		file: Optional[ndarray] = None
		files = QtWidgets.QFileDialog.getOpenFileNames()[0]
		if not files:
			return
		error = False

		file_name = files[0]  # ignore everything but the first file

		comment_rows = 0
		while comment_rows <= 5:
			try:
				# noinspection PyTypeChecker
				file = genfromtxt(file_name, skip_header=comment_rows, max_rows=10, names=True)
				break
			except ValueError:
				comment_rows += 1

		if comment_rows > 5:
			error = True

		if not error and (len(file) == 0 or len(file[0]) == 1):
			error = True

		if error:
			self.error.showMessage("File could not be loaded.")
			return

		# If the first header item is not a number, we guess the file has a header:
		try:
			float(file.dtype.names[0])
			header = False
		except ValueError:
			header = True

		if header:
			# Extract the header contents to a separate file as headers mess up array dtype.
			header_contents = list(file.dtype.names)

			# Add column numbering:
			for _ in range(len(header_contents)):
				header_contents[_] = str(_+1) + " - " + header_contents[_]

		# Reload the file so we have the proper dtype
		try:
			file = genfromtxt(file_name, skip_header=comment_rows)
		except ValueError:
			self.error.showMessage("File could not be loaded.")
			return

		if not header:
			# Simply put numbers as headers:
			header_contents = []
			for _ in range(len(file[0])):
				header_contents += [str(_+1) + " - " + str(file[0][_]) + ", " + str(file[1][_]) + ", ..."]

		file = file[~isnan(file).any(axis=1)]  # remove NaN rows

		self.name_file = file

		self.ui.field_name_file_column.setEnabled(True)
		self.ui.button_apply_name_file.setEnabled(True)
		self.ui.field_name_file_column.addItems(header_contents)

	def buttonApplyNameFile(self):
		"""
		Applies the values from a parameter file to the loaded oscillations.

		Notes
		-----
		Assumes the order of the file is the same as the order the data files are loaded in.
		"""

		if self.ui.button_temp_dep.isChecked():
			mode = "temperature"
		else:
			mode = "angle"

		column = self.ui.field_name_file_column.currentIndex()
		names = self.name_file[:, column]
		_ = 0
		for file in self.files:
			if mode == "temperature":
				file.temperature = names[_]
			else:
				file.angle = names[_]
			_ += 1

		self.generateTable()
