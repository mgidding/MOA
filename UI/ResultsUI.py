from qtpy import QtWidgets
from qtpy.QtCore import Qt
from Backend.DataObjects.DataWrapper import DataWrapper
from Backend.DataObjects.Constants import Constants
from UI.PageElements import ResultsPageElements
from numpy import savetxt, array, row_stack, empty


class ResultsPage(QtWidgets.QWizardPage):
	"""
	Results page class.

	Notes
	-----
	On the results page users are presented with an overview of all calculated physical values.
	"""

	def __init__(self, data: DataWrapper):
		super().__init__()

		# initialising variables:
		self.ui = ResultsPageElements.Ui_ResultsElements()
		self.ui.setupUi(self)
		self.data: DataWrapper = data

		self.ui.tableWidget.setAlternatingRowColors(True)

		self.initializeButtons()

	# Initialisation methods:
	def initializePage(self):
		"""
		Initialises page elements.
		"""

		self.setTitle("Results")
		self.initializeTable()

		# Resize table headers to fit:
		self.ui.tableWidget.horizontalHeader().setSectionResizeMode(QtWidgets.QHeaderView.Stretch)

		# Resize the scroll area to fit the contents:
		self.ui.scrollAreaWidgetContents.resize(self.ui.scrollAreaWidgetContents.sizeHint())
		# Set the scroll area's maximum width equal to that of the scroll area plus spacing:
		self.ui.scrollArea.setMaximumWidth(
			self.ui.scrollAreaWidgetContents.width() + 2 * self.ui.scrollArea.frameWidth() +
			self.ui.scrollArea.horizontalScrollBar().width()
		)
		self.ui.scrollArea.setMaximumHeight(
			self.ui.scrollAreaWidgetContents.height() + 2 * self.ui.scrollArea.frameWidth() +
			self.ui.scrollArea.verticalScrollBar().width()
		)

	def initializeButtons(self):
		"""
		Connect button methods to their buttons.
		"""

		self.ui.button_save.clicked.connect(self.buttonSaveData)

	def initializeTable(self):
		"""
		Generate a table containing all results.
		"""

		self.ui.tableWidget.clearContents()

		rows = 0
		for lkf_list in self.data.list_of_lkf_parameters_lists:
			rows += len(lkf_list)

		self.ui.tableWidget.setRowCount(rows)

		_ = 0
		for lkf_list in self.data.list_of_lkf_parameters_lists:
			for lkf_parameters in lkf_list:
				# Set data set column:
				data_set = QtWidgets.QTableWidgetItem()
				data_set.setText(lkf_parameters.mo_data.name)
				data_set.setFlags(Qt.ItemIsEnabled)
				self.ui.tableWidget.setItem(_, 0, data_set)

				# Set frequency column:
				frequency = QtWidgets.QTableWidgetItem()
				frequency.setText(str(round(lkf_parameters.frequency, 2)))
				frequency.setFlags(Qt.ItemIsEnabled)
				self.ui.tableWidget.setItem(_, 1, frequency)

				# Set pocket column:
				pocket = QtWidgets.QTableWidgetItem()
				pocket.setText(lkf_parameters.pocket_type)
				pocket.setFlags(Qt.ItemIsEnabled)
				self.ui.tableWidget.setItem(_, 2, pocket)

				# Set m_c column:
				m_c = QtWidgets.QTableWidgetItem()
				m_c_value = lkf_parameters.m_c
				m_c_value = None if m_c_value == Constants.m_e else str(round(m_c_value/Constants.m_e, 2))
				m_c.setText(m_c_value)
				m_c.setFlags(Qt.ItemIsEnabled)
				self.ui.tableWidget.setItem(_, 3, m_c)

				# Set T_D column:
				T_D = QtWidgets.QTableWidgetItem()
				T_D_value = lkf_parameters.dingle_temperature
				T_D_value = None if T_D_value == 1 else str(round(T_D_value, 2))
				T_D.setText(T_D_value)
				T_D.setFlags(Qt.ItemIsEnabled)
				self.ui.tableWidget.setItem(_, 4, T_D)

				# Set onset field column:
				onset = QtWidgets.QTableWidgetItem()
				onset_value = lkf_parameters.onset_field
				onset_value = None if onset_value is None else str(round(onset_value, 2))
				onset.setText(onset_value)
				onset.setFlags(Qt.ItemIsEnabled)
				self.ui.tableWidget.setItem(_, 5, onset)

				_ += 1

		# Resize headers to fit:
		self.ui.tableWidget.horizontalHeader().setSectionResizeMode(QtWidgets.QHeaderView.Stretch)

	# Button methods:
	def buttonSaveData(self):
		"""
		Saves data to user-selected text file.
		"""

		filename = QtWidgets.QFileDialog.getSaveFileName(
			caption="Save data", filter="Text files (*.txt)")[0]

		if filename:
			self.saveData(filename)

	def saveData(self, filename):
		"""
		Saves data to user-selected text file.
		"""

		output = empty((0, 6))
		for lkf_list in self.data.list_of_lkf_parameters_lists:
			for lkf_parameters in lkf_list:
				result = array((
					lkf_parameters.mo_data.name,
					round(lkf_parameters.frequency, 2) if lkf_parameters.frequency is not None else None,
					lkf_parameters.pocket_type,
					round(lkf_parameters.m_c/Constants.m_e, 2) if
					lkf_parameters.m_c is not None and lkf_parameters.m_c != Constants.m_e else None,
					round(lkf_parameters.dingle_temperature, 2) if
					lkf_parameters.dingle_temperature is not None and lkf_parameters.dingle_temperature != 1 else None,
					round(lkf_parameters.onset_field, 2) if lkf_parameters.onset_field is not None else None
				))
				output = row_stack((output, result))

		# noinspection PyTypeChecker
		savetxt(
			filename, output, delimiter="	", fmt="%s",
			header="Data Set	Frequency [T]	Pocket	Cyclotron Mass [m_e]	Dingle Temperature [K]	Onset Field [T]"
		)
