from qtpy.QtCore import QThread
from qtpy import QtWidgets
from UI.PageElements import OnsetFieldForm
from Backend.SimulationFunctions.OnsetFieldFunctions import calculateOnsetField
from numpy import savetxt, column_stack
from UI.PlotUI import PlotCanvas
from UI.SharedUI import MagneticOscillationPage
import UI.InitUI
from math import floor, log10, ceil
from typing import List, Callable, Dict, Any, Optional
from Backend.DataObjects.DataWrapper import DataWrapper
from Backend.DataObjects.MOData import MOData
from Backend.DataObjects.LKFParameters import LKFParameters


class PlotOnsetField(PlotCanvas):
	"""
	Implements plotting for the Onset Field page.
	"""

	def drawOnsetFieldPlot(
		self, mo_data_list: List[MOData], fft_min: float, fft_max: float, H_min_list: List[float],
		H_max_list: List[float], H_avg_list: List[float], pocket: str, lines: bool = False, multi: bool = False,
		offset: float = 0, legend_style: str = 'None'
	):
		"""
		Plot the FFT analyses provided by mo_data_list.

		Notes
		-----
		If lines is True, plot lines rather than markers.

		if multi is True, plot all mo_data objects in data.

		offset gives the offset between each plot when multi is True.

		legend_style is 'Annotated', 'Boxed' or 'None'.
		"""

		if len(mo_data_list) == 0:
			return

		if offset != 0:
			legend_order = 'inverse'
		else:
			legend_order = 'normal'

		self.addPlot(
			"Frequency [T]", "Amplitude (arb. u.)", legend_style=legend_style,
			legend_order=legend_order, x_min=fft_min, x_max=fft_max, y_min=0, legend_items_per_column=18)

		if multi:
			iteration_list = mo_data_list
		else:
			iteration_list = [mo_data_list[-1]]
			H_avg_list = [H_avg_list[-1]]
			H_min_list = [H_min_list[-1]]
			H_max_list = [H_max_list[-1]]

		_ = 0
		for mo_data in iteration_list:
			amplitudes = mo_data.fft_amplitude_range + _*offset
			annotation = (
					'{:.3}'.format(str(H_avg_list[_])) + " [T]\n(" +
					'{:.3}'.format(str(H_min_list[_])) + ", " +
					'{:.3}'.format(str(H_max_list[_])) + ")")
			self.addLine(mo_data.fft_frequency_range, amplitudes, lines, label=annotation)

			# Add oscillation indicator:
			for lkf_parameters in mo_data.oscillation_analysis:
				if lkf_parameters.pocket_type == pocket:
					name = lkf_parameters.pocket_type
					amplitude = lkf_parameters.raw_amplitude + _*offset
					frequency = lkf_parameters.frequency

					self.addLine(frequency, amplitude, lines=False, style='*r', priority=10)
					if name is not None:
						self.addAnnotation(name, frequency * 1.01, amplitude * 1.01)
					break
			_ += 1

		self.plot_thread.run()


class OnsetFieldCalculation(QThread):
	"""
	QThread child class that handles running the onset field calculation on its own thread.
	"""

	def __init__(self):
		QThread.__init__(self)
		self.mo_data: Optional[MOData] = None
		self.lkf_parameters_list: Optional[List[LKFParameters]] = []
		self.pocket_type: Optional[str] = None
		self.H_min: Optional[float] = None
		self.H_max: Optional[float] = None
		self.inverse_H_step: Optional[float] = None
		self.min_rel_A: Optional[float] = None
		self.bin_size: Optional[float] = None
		self.calculation_type: Optional[str] = None
		self.mo_detected = False
		self.H_avg_list: Optional[List[float]] = []
		self.mo_data_list: Optional[List[MOData]] = []
		self.H_min_list: Optional[List[float]] = []
		self.H_max_list: Optional[List[float]] = []
		self.canceled: bool = False
		self.error: bool = False

	def run(self):
		"""
		Method that is called when OnsetCalculation.start() is executed.

		Notes
		-----
		Uses Backend.SimulationFunctions.OnsetFieldFunctions.calculateOnsetField()
		"""

		try:
			self.canceled = False
			self.error = False
			self.mo_detected, self.H_avg_list, self.mo_data_list, self.H_min_list, self.H_max_list = (
				calculateOnsetField(
					self.mo_data, self.lkf_parameters_list, self.pocket_type, self.H_min, self.H_max,
					self.inverse_H_step, self.min_rel_A, self.bin_size, self.calculation_type,
					self.isCanceled
				)
			)
		except:
			self.error = True

	def getResults(self) -> (bool, List[float], List[MOData], List[float], List[float]):
		"""
		Returns the calculation result.
		"""

		return self.mo_detected, self.H_avg_list, self.mo_data_list, self.H_min_list, self.H_max_list

	def errorEncountered(self) -> bool:
		"""
		Returns true if the calculation thread encountered an error.
		"""

		return self.error

	def isCanceled(self) -> bool:
		"""
		Returns whether the calculation is canceled.
		"""

		return self.canceled

	def cancel(self):
		"""
		Cancels the calculation when called.
		"""

		self.canceled = True

	def setParameters(
			self, mo_data: MOData, lkf_parameters_list: List[LKFParameters], pocket_type: str, H_min: float, H_max: float,
			inverse_H_step: float, min_rel_A: float, bin_size: float, calculation_type: str
	):
		"""
		Method that sets the parameters used by the onset field calculation.

		Notes
		----------
		inverse_H_step is not used when calculation type is set to "single".

		calculation_type is either'fixed_interval' or 'fixed_minimum'.
		"""

		self.mo_data = mo_data
		self.lkf_parameters_list = lkf_parameters_list
		self.pocket_type = pocket_type
		self.H_min = H_min
		self.H_max = H_max
		self.inverse_H_step = inverse_H_step
		self.min_rel_A = min_rel_A
		self.bin_size = bin_size
		self.calculation_type = calculation_type


class OnsetFieldPage(MagneticOscillationPage):
	"""
	Onset Field page class.

	Notes
	-----
	On the onset field page users can change the settings used by the onset field detection algorithm.
	"""

	# Dict to look up estimation types for backend.
	estimation_types = ['fixed_minimum', 'fixed_interval']

	def __init__(self, data: DataWrapper):
		super().__init__(data, OnsetFieldForm.Ui_Form, PlotOnsetField, "Onset Field Calculation")
		self.setFinalPage(True)

		# initialize variables:
		self.H_avg_list: Optional[List[float]] = []
		self.mo_data_list: Optional[List[MOData]] = []
		self.H_min_list: Optional[List[float]] = []
		self.H_max_list: Optional[List[float]] = []
		self.estimation_type: Optional[str] = None
		self.H_min: Optional[float] = None
		self.H_max: Optional[float] = None
		self.interval: Optional[float] = None
		self.min_rel_A: Optional[float] = None
		self.bin_size: Optional[float] = None
		self.fft_min: Optional[float] = None
		self.fft_max: Optional[float] = None
		self.callback: Optional[Callable[[None], ...]] = None

		self.save_data_filename: Optional[str] = None
		self.foldername: Optional[str] = None
		self.set: Optional[int] = None

		# assign variables:
		self.mo_detected: bool = False
		self.pocket_type: str = "None"
		self.m_c: float = 0.0
		self.m_c_cov: float = 0.0
		self.pocket_types: List[str] = []
		self.run: bool = True
		self.change_pocket_index: int = 0
		self.save_data_index: int = 0
		self.frequencies: Dict[int] = {}

		# Set up threading:
		self.calculation_thread = OnsetFieldCalculation()
		self.calculation_thread.finished.connect(self.finishOnsetFieldCalculation)

	# Initialisation methods:
	def initializeSelectPocket(self):
		"""
		Adds each identified pocket to the drop down menu so they can be selected.
		"""

		# Disconnect the plot function, else plot will be called when we update the change_set item list.
		try:
			self.ui.field_change_pocket.currentIndexChanged.disconnect()
		except TypeError:
			pass

		# Create a list of all pocket types that should be added to change_set:
		self.pocket_types = ["None"]
		self.frequencies = {}
		_ = 0
		for lkf_parameters_list in self.data.list_of_lkf_parameters_lists:
			__ = 0
			for lkf_parameters in lkf_parameters_list:
				if lkf_parameters.active is False:
					continue
				pocket_type = lkf_parameters.pocket_type
				if pocket_type not in self.pocket_types and pocket_type is not None:
					self.pocket_types += [pocket_type]
					self.frequencies[pocket_type] = lkf_parameters.frequency
				__ += 1
			_ += 1

		# Add the pockets to the UI and (re)connect the plot function to change_set.
		self.ui.field_change_pocket.blockSignals(True)
		self.ui.field_change_pocket.clear()
		self.ui.field_change_pocket.addItems(self.pocket_types)
		self.ui.field_change_pocket.currentIndexChanged.connect(self.autoUpdate)
		self.ui.field_change_pocket.blockSignals(False)

	def initializePage(self):
		"""
		Calls all initialisation methods in the class and sets the currently displayed set.

		Notes
		-----
		Called automatically when the page is first displayed.
		"""

		self.finished_init = False
		self.run = True
		self.container_ui.plot_button.setText("Calculate")
		self.initializeSelectPocket()
		super().initializePage()
		if MagneticOscillationPage.cyclotron_mass_detection is True:
			self.setFinalPage(False)

	def initializeFieldValues(self):
		"""
		Initialises all fields in the class with data from the currently selected MOData object.
		"""

		# Set ranges:
		self.ui.field_min_H.setRange(
			floor(self.data.mo_data.raw_full_magnetic_field[0]),
			self.data.mo_data.raw_full_magnetic_field[-2]
		)

		self.ui.field_max_H.setRange(
			self.data.mo_data.raw_full_magnetic_field[1],
			ceil(self.data.mo_data.raw_full_magnetic_field[-1])
		)
		self.ui.field_interval.setRange(0.001, self.data.mo_data.raw_full_magnetic_field[-1])
		min_freq = self.data.mo_data.fft_full_frequency_range[0]
		max_freq = self.data.mo_data.fft_full_frequency_range[-1]
		self.ui.field_min_freq.setRange(min_freq, max_freq)
		self.ui.field_max_freq.setRange(min_freq, max_freq)
		self.ui.field_min_rel_A.setRange(0.0000000001, 1)
		self.ui.field_bin_size.setMinimum(0.5)
		self.ui.field_bin_size.setDecimals(10)
		self.ui.field_change_set.blockSignals(True)

		# Set default values:
		index = self.data.index
		self.ui.field_change_set.setCurrentIndex(index)
		self.ui.field_change_set.blockSignals(False)
		self.ui.field_estimation_type.setCurrentIndex(1)
		self.ui.field_min_H.setValue(0)
		self.ui.field_max_H.setValue(ceil(1 / self.data.mo_data.interpolation_inverse_magnetic_field[0]))
		self.ui.field_interval.setValue(1.0)
		self.ui.field_offset.setValue(0)
		fft_min, fft_max = self.data.mo_data.fft_frequency_limits()
		self.ui.field_min_freq.setValue(floor(fft_min))
		self.ui.field_max_freq.setValue(10 ** ceil(log10(fft_max)))
		self.ui.field_min_rel_A.setValue(0.05)
		self.ui.field_bin_size.setValue(ceil(self.data.mo_data.fft_bin_size))
		if len(self.pocket_types) > 1:
			self.ui.field_change_pocket.blockSignals(True)
			self.ui.field_change_pocket.setCurrentIndex(1)
			self.ui.field_change_pocket.blockSignals(False)
		self.validateFields()  # make sure none of the fields are displaying red without reason.

	def initializeButtons(self):
		"""
		Connect button methods to their buttons.
		"""

		super().initializeButtons()
		self.ui.field_estimation_type.currentIndexChanged.connect(self.buttonEstimationType)
		self.ui.field_change_pocket.currentIndexChanged.connect(self.changePocket)
		self.container_ui.abort_button.clicked.connect(self.terminateOnsetFieldCalculation)
		self.ui.save_fft_data_button.clicked.connect(self.saveFFTData)
		self.ui.save_all_fft_data_button.clicked.connect(self.saveAllFFTData)

# Validator methods
	def validateFields(self) -> bool:
		"""
		Validate each field in the page and return true if all are acceptable.
		"""

		# check that fft min frequency is lower than the max frequency:
		if self.ui.field_min_freq.value() >= self.ui.field_max_freq.value():
			self.ui.field_min_freq.setStyleSheet('QDoubleSpinBox { background-color: #da4453}')
			self.ui.field_max_freq.setStyleSheet('QDoubleSpinBox { background-color: #da4453}')
			return False
		else:
			self.ui.field_min_freq.setStyleSheet('')
			self.ui.field_max_freq.setStyleSheet('')

		# Check that the min magnetic field is lower than the max and that the interval is not too big:
		if self.ui.field_min_H.value() >= self.ui.field_max_H.value():
			self.ui.field_min_H.setStyleSheet('QDoubleSpinBox { background-color: #da4453}')
			self.ui.field_max_H.setStyleSheet('QDoubleSpinBox { background-color: #da4453}')
			return False
		else:
			self.ui.field_min_H.setStyleSheet('')
			self.ui.field_max_H.setStyleSheet('')
		if self.ui.field_interval.value() > self.ui.field_max_H.value() - self.ui.field_min_H.value():
			self.ui.field_interval.setStyleSheet('QDoubleSpinBox { background-color: #da4453}')
			return False
		else:
			self.ui.field_interval.setStyleSheet('')

		return True

	def validatePage(self) -> bool:
		if not super().validateFields():
			return False
		self.updateBackend()
		return True

	def nextId(self) -> int:
		"""
		Decide which Wizard page to load next based on page state.

		Notes
		-----
		nextId() is an internal PyQt method that can be reimplemented to change which QWizard page is loaded next.
		"""

		if MagneticOscillationPage.cyclotron_mass_detection:
			return UI.InitUI.Wizard.m_c_calculation
		elif MagneticOscillationPage.dingle_temp_calculation:
			return UI.InitUI.Wizard.filter_calculation
		else:
			return UI.InitUI.Wizard.results

# Data methods:
	def startOnsetFieldCalculation(self, custom_callback: Callable[[None], Any] = None):
		"""
		Sets up and starts the onset field calculation.
		"""

		data_set = self.ui.field_change_set.currentIndex()
		if self.set != data_set:
			self.set = data_set
			self.run = True

		pocket_type = self.ui.field_change_pocket.currentText()
		if self.pocket_type != pocket_type:
			self.pocket_type = pocket_type
			self.run = True

		estimation_type = OnsetFieldPage.estimation_types[self.ui.field_estimation_type.currentIndex()]
		if self.estimation_type != estimation_type:
			self.estimation_type = estimation_type
			self.run = True

		H_min = self.ui.field_min_H.value()
		if self.H_min != H_min:
			self.H_min = H_min
			self.run = True

		H_max = self.ui.field_max_H.value()
		if self.H_max != H_max:
			self.H_max = H_max
			self.run = True

		interval = self.ui.field_interval.value()
		if self.interval != interval:
			self.interval = interval
			self.run = True

		min_rel_A = self.ui.field_min_rel_A.value()
		if self.min_rel_A != min_rel_A:
			self.min_rel_A = min_rel_A
			self.run = True

		bin_size = self.ui.field_bin_size.value()
		if self.bin_size != bin_size:
			self.bin_size = bin_size
			self.run = True

		fft_min = self.ui.field_min_freq.value()
		if self.fft_min != fft_min:
			self.fft_min = fft_min
			self.run = True

		fft_max = self.ui.field_max_freq.value()
		if self.fft_max != fft_max:
			self.fft_max = fft_max
			self.run = True

		if custom_callback is None:
			self.callback = self.finishRenderUI
		else:
			self.callback = custom_callback

		if not self.run:
			self.callback()
		else:
			self.calculation_thread.setParameters(
				self.data.mo_data, self.data.lkf_parameters_list, pocket_type, H_min, H_max, interval, min_rel_A,
				bin_size, estimation_type
			)
			self.container_ui.abort_button.setVisible(True)
			self.container_ui.plot_button.setVisible(False)
			self.enableUI(False)
			self.calculation_thread.start()

	def terminateOnsetFieldCalculation(self):
		"""
		Terminates the onset field calculation.
		"""

		self.calculation_thread.cancel()
		self.calculation_thread.quit()
		self.calculation_thread.wait(1000)
		self.container_ui.abort_button.setVisible(False)
		self.container_ui.plot_button.setVisible(True)
		self.enableUI(True)

	def finishOnsetFieldCalculation(self):
		"""
		Processes the result of the onset field calculation and updates the UI.

		Notes
		-----
		Is called automatically by the onset field calculation thread when it finishes its calculations.
		"""

		self.container_ui.abort_button.setVisible(False)
		self.container_ui.plot_button.setVisible(True)
		self.enableUI(True)

		if self.calculation_thread.errorEncountered():
			self.error.showMessage("An error was encountered during the calculation.")
			self.run = False
			return

		(
			self.mo_detected, self.H_avg_list, self.mo_data_list,
			self.H_min_list, self.H_max_list
		) = self.calculation_thread.getResults()
		self.callback()
		self.run = False

	def updateBackend(self):
		"""
		Updates the backend with the calculated onset field.
		"""

		if self.pocket_type != "None":
			for oscillation in self.data.lkf_parameters_list:
				if oscillation.pocket_type == self.pocket_type:
					try:
						oscillation.onset_field = float(self.ui.field_onset_H_avg.text())
					except ValueError:
						oscillation.onset_field = 1/self.data.mo_data.interpolation_inverse_magnetic_field[-1]
					break

		self.pocket_type = self.ui.field_change_pocket.currentText()

	# UI methods:
	def plot(self, alternative: bool = False):
		"""
		Updates the on-screen plot.
		"""

		# prevent multiple plots running at the same time:
		if self.is_plotting:
			return
		self.is_plotting = True

		multi = self.ui.plot_all.isChecked()

		if multi:
			self.enableUI(False)
			self.container_ui.plot_button.setVisible(False)
			self.container_ui.abort_plot_button.setVisible(True)

		marker_style = self.ui.plot_lines.isChecked()
		offset = self.ui.field_offset.value()
		legend = self.ui.field_legend.currentText()
		fft_min = self.ui.field_min_freq.value()
		fft_max = self.ui.field_max_freq.value()
		pocket = self.ui.field_change_pocket.currentText()

		self.plot_widget.drawOnsetFieldPlot(
			self.mo_data_list, fft_min, fft_max, self.H_min_list, self.H_max_list, self.H_avg_list,
			pocket, marker_style, multi, offset, legend
		)

		if multi:
			self.enableUI(True)
			self.container_ui.plot_button.setVisible(True)
			self.container_ui.abort_plot_button.setVisible(False)

		self.is_plotting = False

	def renderUI(self):
		"""
		Calculate the onset field and update the UI afterwards.

		Notes
		-----
		The actual rendering of the UI is performed by finishRenderUI(),
		which is called automatically when the onset field calculation is finished.
		"""

		if not self.finished_init:
			return

		self.startOnsetFieldCalculation()

	def finishRenderUI(self):
		"""
		Update the UI after the onset field has been calculated.

		Notes
		-----
		Called automatically by the onset field calculation thread.
		"""

		if self.ui.field_change_pocket.currentIndex() != 0:
			self.ui.field_frequency.setText(str('{:.3g}'.format(self.frequencies[self.ui.field_change_pocket.currentText()])))
		else:
			self.ui.field_frequency.setText("Not available")
		if self.mo_detected:
			self.ui.field_onset_H_avg.setText(str('{:.3}'.format(self.H_avg_list[-1])))
		else:
			self.ui.field_onset_H_avg.setText("Not detected")
		self.plot()

	def changeSet(self):
		"""
		Called when changing data set.
		"""

		self.updateBackend()
		index = self.ui.field_change_set.currentIndex()
		self.data.index = index
		self.autoUpdate()

	def changePocket(self):
		"""
		Called when changing pocket.
		"""

		self.updateBackend()
		self.autoUpdate()

# Button methods:
	def buttonEstimationType(self):
		"""
		Handles toggling of UI elements when estimation type is changed.
		"""

		if self.ui.field_estimation_type.currentText() == "Single":
			self.ui.field_interval.setEnabled(False)
			self.ui.label_interval.setEnabled(False)
		else:
			self.ui.field_interval.setEnabled(True)
			self.ui.label_interval.setEnabled(True)
		self.autoUpdate()

	def saveData(self, filename: str):
		"""
		Saves data to user-selected text file.
		"""

		name = self.ui.field_change_pocket.currentText()
		frequency = str(self.frequencies[name])
		output = column_stack(([name], [frequency], [str(self.H_max_list[-1])], [str(self.H_avg_list[-1])]))

		savetxt(
			filename, output, delimiter="	", fmt="%s",
			header="Name    Frequency    Onset Field (avg.) [T]    Onset Field (max.) [T]"
		)

	def saveAllData(self, foldername: str):
		"""
		Saves all data sets to user-selected folder.
		"""

		self.change_pocket_index = self.ui.field_change_pocket.currentIndex()
		self.save_data_index = 0
		self.ui.field_change_pocket.setCurrentIndex(0)
		self.foldername = foldername
		self.save_data_filename = (
				self.foldername + "/" + str(self.data.mo_data.name) +
				" " + str(self.pocket_types[0]) + " onset fields.txt"
		)
		self.startOnsetFieldCalculation(custom_callback=self.saveAllDataHelper)

	def saveFFTData(self):
		"""
		Saves FFT data to user-selected text file.
		"""

		if not self.validateFields():
			return

		filename = QtWidgets.QFileDialog.getSaveFileName(
			caption="Save data", filter="Text files (*.txt)")[0]

		if not filename:
			return

		self.mo_data_list[-1].fft_frequency_limits(self.ui.field_min_freq.value(), self.ui.field_max_freq.value())
		self.mo_data_list[-1].save_fft_data(filename)

	def saveAllFFTData(self):
		"""
		Saves all FFT data sets to user-selected folder.
		"""

		if not self.validateFields():
			return

		self.updateAllData()

		foldername = str(QtWidgets.QFileDialog.getExistingDirectory(self, "Select Directory"))

		if not foldername:
			return

		_ = 0
		for mo_data in self.mo_data_list:
			mo_data.fft_frequency_limits(self.ui.field_min_freq.value(), self.ui.field_max_freq.value())
			filename = str('{:.3}'.format(self.H_avg_list[_])) + "[T] avg FFT data.txt"
			mo_data.save_fft_data(foldername + "/" + filename)
			_ += 1

	def saveAllDataHelper(self):
		"""
		Loops over all data sets and calls saveData() on all of them.
		"""

		self.saveData(self.save_data_filename)
		if self.save_data_index < len(self.pocket_types)-1:
			self.save_data_index += 1
			self.save_data_filename = (
					self.foldername + "/" + str(self.data.mo_data.name) +
					" " + str(self.pocket_types[self.save_data_index]) + " onset fields.txt"
			)
			self.ui.field_change_pocket.setCurrentIndex(self.save_data_index)
			self.startOnsetFieldCalculation(custom_callback=self.saveAllDataHelper)
		else:
			self.ui.field_change_pocket.setCurrentIndex(self.change_pocket_index)
