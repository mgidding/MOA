from qtpy import QtWidgets
from qtpy.QtCore import QThread
import matplotlib
import matplotlib.figure
from matplotlib.lines import Line2D
from matplotlib.backends.backend_qt5agg import FigureCanvasQTAgg as Canvas
from math import ceil
from numpy import ndarray
from qtpy.QtCore import QCoreApplication
from operator import lt, gt
from typing import Callable, List, Union
from Backend.DataObjects.DataWrapper import DataWrapper
from Backend.DataObjects.MOData import MOData


class PlotThread(QThread):
	"""
	Class that puts plot rendering on its own thread using a QThread.
	"""

	def __init__(self, showPlots: Callable[[bool, bool, Callable[[None], bool]], None]):
		super().__init__()
		self.showPlots = showPlots
		self.auto_x = True
		self.clear = True
		self.canceled = False

	def run(self):
		"""
		Start the plot is a separate thread.
		"""

		self.canceled = False
		self.showPlots(self.clear, self.auto_x, self.isCanceled)

	def isCanceled(self) -> bool:
		"""
		Returns whether the calculation is canceled.
		"""

		return self.canceled

	def cancel(self):
		"""
		Cancels the calculation when called.
		"""

		self.canceled = True

	def setParameters(self, clear: bool = True, auto_x: bool = True):
		"""
		Set showPlots() options.

		Notes
		-----
		If clear is True, clear plot data after rendering.

		If auto_x is True, automatically set the x axis. Assumes ordered x arrays.
		"""

		self.auto_x = auto_x
		self.clear = clear


class PlotAxisData:
	"""
	Data class that tracks all variables for one axis.
	"""

	def __init__(
		self, x_label: str, y_label: str, x_min: float, x_max: float, y_min: float, y_max: float,
		legend_style: str, legend_order: str, legend_items_per_column: int
	):
		self.x_label: str = x_label
		self.y_label: str = y_label
		self.x_min: float = x_min
		self.x_max: float = x_max
		self.y_min: float = y_min
		self.y_max: float = y_max
		self.legend_style: str = legend_style
		self.legend_order: str = legend_order
		self.legend_items_per_column: int = legend_items_per_column
		self.plot_line_data: List[PlotLineData] = []
		self.annotations: List[PlotAnnotationData] = []
		self.axis_legends: List[Line2D] = []


class PlotLineData:
	"""
	Data class that tracks all data for one line.
	"""

	def __init__(
		self, x: Union[List[float], ndarray, float], y: Union[List[float], ndarray, float], fmt: str, label: str,
		priority: int, draw: bool
	):
		self.x: Union[List[float], ndarray, float] = x
		self.y: Union[List[float], ndarray, float] = y
		self.fmt: str = fmt
		self.label: str = label
		self.priority: int = priority
		self.draw: bool = draw


class PlotAnnotationData:
	"""
	Data class that tracks all data for one annotation
	"""

	def __init__(self, text: str, x: float, y: float):
		self.text: str = text
		self.x: float = x
		self.y: float = y


class PlotCanvas(Canvas):
	"""
	Parent class for all plot classes used by the UI.
	"""

	def __init__(self, parent: QtWidgets.QWidget = None, dpi: int = 100):
		# initialise variables:
		self.fig = matplotlib.figure.Figure(dpi=dpi, tight_layout=True)
		self.axes: List[PlotAxisData] = []
		self.plot_thread = PlotThread(self.showPlots)
		self.empty_legends = 0

		# initialise Canvas class:
		Canvas.__init__(self, self.fig)
		self.setParent(parent)
		Canvas.setSizePolicy(self, QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Expanding)
		Canvas.updateGeometry(self)

	@staticmethod
	def getMODataList(data: DataWrapper, multi: bool) -> List[MOData]:
		"""
		Helper function to extract data so it can be iterated over.
		"""

		if multi:
			return data.mo_data_list
		else:
			return [data.mo_data]

	def addPlot(
			self, x_label: str = None, y_label: str = None, x_min: float = None, x_max: float = None,
			y_min: float = None, y_max: float = None, legend_style: str = 'None', legend_order: str = 'normal',
			legend_items_per_column: int = 30):
		"""
		Add a new plot to the figure, specifying the x and y labels, minimums and maximums.

		Notes
		-----
		legend_style is 'Annotated', 'Boxed' or 'None'. Default is 'None'.

		legend_order is 'normal' or 'inverse'.
		Order the legend in the order the plots were added, or in inverse order. Default is 'normal'.
		"""

		self.axes += [
			PlotAxisData(x_label, y_label, x_min, x_max, y_min, y_max, legend_style, legend_order, legend_items_per_column)
		]

	def addLine(
		self, x: Union[List[float], ndarray, float], y: Union[List[float], ndarray, float], lines: bool,
		style: str = None, label: str = None, priority: int = 0, draw: bool = True):
		"""
		Add a new line to the plot, specifying the x and y data and plot style.

		Notes
		-----
		priority sets the plot priority. Higher priority means the plot will favour being on top of lower priority plots.

		When draw is true, update the UI after drawing this line.

		Plot style follows pyplot.plot()'s formatting.

		'fmt' overwrites 'lines' when it is specified.
		"""

		if self.axes is None:
			return

		if lines:
			fmt = '-'
		else:
			fmt = '.'

		if style:
			fmt = style

		if label is None:
			self.empty_legends += 1

		self.axes[-1].plot_line_data += [PlotLineData(x, y, fmt, label, priority, draw)]

	def addAnnotation(self, text: str, x: float, y: float):
		"""
		Add an annotation in the specified location.
		"""

		if self.axes is None:
			return

		self.axes[-1].annotations += [PlotAnnotationData(text, x, y)]

	def addAxisLegend(self, text: str, color: str, marker: str, lw: float):
		"""
		Add a legend item to the current axis.
		"""

		legend = Line2D([0], [0], color=color, marker=marker, label=text, lw=lw)

		self.axes[-1].axis_legends += [legend]

	@staticmethod
	def findYExtremal(lines: List[PlotLineData], extremum: str = "max") -> Union[None, float]:
		"""
		Find the extremal y value in PlotLineData list.

		Notes
		-----
		extremum is either "max" or "min"
		"""

		if len(lines) == 0:
			return None

		# Set the operators to use later:
		if extremum == "max":
			extremal = max
			comparison = gt
		elif extremum == "min":
			extremal = min
			comparison = lt
		else:
			raise ValueError("Extremum type not recognised.")

		# If the y value
		y_extremum = None
		for line in lines:
			try:
				local_y_extremum = extremal(line.y)
			except TypeError:
				local_y_extremum = line.y
			except ValueError:
				continue

			if y_extremum is None:
				y_extremum = local_y_extremum
			elif comparison(local_y_extremum, y_extremum):
				y_extremum = local_y_extremum
		return y_extremum

	def showPlots(self, clear: bool = True, auto_x: bool = True, isCanceled: Callable[[None], bool] = None):
		"""
		Render the plots.

		Notes
		-----
		If bool is True, clear plot data after rendering.

		If auto_x is True, automatically set the x axis. Assumes ordered x arrays.

		isCanceled is a function that is checked regularly to see if the plot should be canceled.
		"""

		# clear old figure:
		self.fig.clear()

		# determine the shape of the subplots:
		plots = len(self.axes)
		if plots == 1:
			rows = 1
			columns = 1
			axes = [[self.fig.subplots(1, 1)]]
		elif plots == 2:
			rows = 1
			columns = 2
			axes = [self.fig.subplots(2, 1)]
		elif plots > 2:
			rows = ceil(plots/2)
			columns = 2
			axes = self.fig.subplots(rows, columns)
		else:
			return

		handles = []
		# go through each subplot and add the requested lines, annotations and legends:
		for row in range(rows):
			for column in range(columns):
				# unpack all values:
				element = row + column

				axis_data = self.axes[element]
				line_data = self.axes[element].plot_line_data
				annotations = self.axes[element].annotations
				legends = self.axes[element].axis_legends

				axis = axes[row][column]

				y_min = axis_data.y_min
				y_max = axis_data.y_max

				if y_min is not None and y_max is None:
					y_max = self.findYExtremal(line_data, "max")
					y_max = 1.02*y_max if y_max is not None else y_max
				if y_min is None and y_max is not None:
					y_min = self.findYExtremal(line_data, "min")
					y_min = 0.98*y_min if y_min is not None else y_min

				# to inverse the legend we simply invert the plotting order:
				if axis_data.legend_order == 'inverse':
					axis_data.plot_line_data = axis_data.plot_line_data[::-1]
					axis_data.axis_legends = axis_data.axis_legends[::-1]

				# Set default right border used by legend types 'None' and 'Annotated'.

				# Set axes labels:
				axis.set_xlabel(axis_data.x_label)
				axis.set_ylabel(axis_data.y_label)

				x_min_auto = None
				x_max_auto = None

				for line in line_data:
					if isCanceled is not None and isCanceled():
						self.clearPlotData()
						return

					# Add the label only if we want a boxed style legend:
					axis.plot(line.x, line.y, line.fmt, label=line.label, zorder=line.priority)

					# for the annotation legend style, we use axis.annotate() and place the annotation all the way to the right:
					if axis_data.legend_style == 'Annotated':
						if line.label is not None:
							axis.annotate(
								line.label, xy=(1, line.y[-1]), xytext=(-50, 10), xycoords=axis.get_yaxis_transform(),
								textcoords="offset points", va="center", ha="left", fontweight="bold"
							)

					# If the user did not specify an x_min and x_max,
					# track the minimum and maximum x values to set the x-axis limits with:
					if auto_x and axis_data.x_min is None and axis_data.x_max is None:
						if type(line.x) in [float, int]:
							x = [line.x]
						else:
							x = line.x
						if (type(x) is ndarray and x.size != 0) or (type(x) is list and len(x) != 0):
							if x_min_auto is None:
								x_min_auto = x[0]
							elif x_min_auto > x[0]:
								x_min_auto = x[0]
							if x_max_auto is None:
								x_max_auto = x[-1]
							elif x_max_auto < x[-1]:
								x_max_auto = x[-1]

					# Set axis limits:
					if axis_data.x_min is None and axis_data.x_max is None:
						axis.set_xlim(x_min_auto, x_max_auto)
					else:
						axis.set_xlim(axis_data.x_min, axis_data.x_max)
					if y_min is not None or y_max is not None:
						axis.set_ylim(y_min, y_max)

					# Extract line legends:
					line_handles, __ = axis.get_legend_handles_labels()

					if axis_data.legend_style == 'Boxed':
						all_handles = line_handles + handles
					else:
						all_handles = handles

					# Render the legends only if there's something to render:
					if axis_data.legend_style == 'Boxed' or len(legends) > 0:
						# Count the number of legend items:
						n_elements = len(legends)
						if axis_data.legend_style == 'Boxed':
							n_elements += len(line_data) - self.empty_legends
						# Calculate the number of columns:
						n_cols = ceil(n_elements/axis_data.legend_items_per_column)
						n_cols = 1 if n_cols == 0 else n_cols
						axis.legend(loc='upper left', bbox_to_anchor=(1.005, 1), borderaxespad=0., handles=all_handles, ncol=n_cols)

					# Draw
					if line.draw:
						self.draw()
						QCoreApplication.processEvents()  # Force the UI to update.

				# add the annotations that were added by addAnnotation():
				for annotation in annotations:
					axis.annotate(annotation.text, (annotation.x, annotation.y))

		# Plot and clean up:
		self.draw()
		if clear:
			self.clearPlotData()

	def clearPlotData(self):
		"""
		Clear plot data so that a new plot can be created.
		"""

		self.axes = []
		self.empty_legends = 0

	def drawMainPlot(self, data: DataWrapper, lines: bool, multi: bool, offset: float, legend_style: str):
		pass

	def drawAlternativePlot(self, data: DataWrapper, lines: bool, multi: bool, offset: float, legend_style: str):
		pass


class PlotMagneticField(PlotCanvas):
	"""
	Helper functions for magnetic field plotting.
	"""

	def addMagneticFieldPlot(self, x_axis: str, y_axis: str, offset: float, legend_style: str):
		"""
		Adds a new magnetic field plot.

		Notes
		-----
		offset sets the spacing between each plot when multi is True.

		legend_style is 'Annotated', 'Boxed' or 'None'.
		"""

		if offset != 0:
			y_axis += " (arb. u.)"
			legend_order = 'inverse'
		else:
			legend_order = 'normal'

		self.addPlot(x_axis, y_axis, legend_style=legend_style, legend_order=legend_order)


class PlotFFT(PlotCanvas):
	"""
	Helper functions for FFT plotting.
	"""

	def addFFTPlot(
		self, data: DataWrapper, lines: bool, multi: bool, offset: float, legend_style: str, x_axis: str = None,
		draw: bool = True
	):
		"""
		Adds a new FFT plot.

		Notes
		-----
		If lines is True, plot lines rather than markers.

		if multi is True, plot all mo_data objects in data.

		offset gives the offset between each plot when multi is True.

		legend_style is 'Annotated', 'Boxed' or 'None'.
		"""

		# Retrieve fft_min and fft_max of currently displayed data set.
		fft_min, fft_max = data.mo_data.fft_frequency_limits()

		if offset != 0:
			legend_order = 'inverse'
		else:
			legend_order = 'normal'
		self.addPlot(
			x_axis, "Amplitude (arb. u.)", legend_style=legend_style,
			legend_order=legend_order, x_min=fft_min, x_max=fft_max, y_min=0
		)

		_ = 0
		for mo_data in self.getMODataList(data, multi):
			amplitudes = mo_data.fft_amplitude_range + _*offset
			if data.mode == "temperature":
				annotation = str(mo_data.temperature) + '[K]'
			else:
				annotation = str(mo_data.angle)
			self.addLine(mo_data.fft_frequency_range, amplitudes, lines, label=annotation, draw=draw)
			_ += 1

	def addOscillations(self, data: DataWrapper, multi: bool, offset: float):
		"""
		Adds oscillations to the current plot.

		Notes
		-----
		if multi is True, plot all mo_data objects in data.

		offset gives the offset between each plot when multi is True.
		"""

		_ = 0
		if multi:
			oscillation_data_list = data.list_of_lkf_parameters_lists
		else:
			oscillation_data_list = [data.lkf_parameters_list]

		for oscillation_data in oscillation_data_list:
			for oscillation in oscillation_data:
				amplitude = oscillation.raw_amplitude + _*offset
				self.addLine(oscillation.frequency, amplitude, lines=False, style='r*', priority=10, draw=False)
			_ += 1
