from qtpy import QtWidgets
from UI.PageElements import ContainerElements
from matplotlib.backends.backend_qt5agg import NavigationToolbar2QT as NavigationToolBar
from qtpy.QtCore import QCoreApplication
from typing import List, Union, Callable, Optional
from Backend.DataObjects.DataWrapper import DataWrapper
from UI.PageElements.SimulationForm import Ui_Form as sim_ui
from UI.PageElements.InterpolationForm import Ui_Form as int_ui
from UI.PageElements.FFTAnalysisForm import Ui_Form as fft_ui
from UI.PageElements.OscillationForm import Ui_Form as osc_ui
from UI.PageElements.OnsetFieldForm import Ui_Form as ons_ui
from UI.PageElements.CyclotronMassForm import Ui_Form as cyc_ui
from UI.PageElements.AngleAnalysisForm import Ui_Form as ang_ui
from UI.PageElements.DingleFactorForm import Ui_Form as din_ui
from UI.PageElements.FilterForm import Ui_Form as fil_ui
from UI.PlotUI import PlotCanvas


class MagneticOscillationPage(QtWidgets.QWizardPage):
	"""
	Magnetic oscillation page parent class.

	Notes
	-----
	Implements functions that are shared between all the pages.
	"""

	# Max size of scroll area UI of the widest child page:
	max_size = 0

	# States of the shared buttons:
	auto_update = True
	plot_lines = True
	plot_inverse_H = False
	legend_style = 'None'

	# Dict of legend styles:
	legend_styles = {'None': 0, 'Annotated': 1, 'Boxed': 2}

	# Pages that should be displayed:
	onset_field_detection = True
	cyclotron_mass_detection = True
	dingle_temp_calculation = True

	# Parent wizard page back and next button setEnabled() functions:
	back_button_enabled: QtWidgets.QAbstractButton.isEnabled = None
	next_button_enabled: QtWidgets.QAbstractButton.isEnabled = None

	def __init__(self, data: DataWrapper, ui_elements: Callable, plot_canvas: Callable, title: str):
		super().__init__()

		# initialise variables:
		self.spinboxes: List[QtWidgets.QAbstractSpinBox] = []
		self.data = data
		self.old_focus: Optional[QtWidgets.QWidget] = None
		self.is_plotting = False

		# initialise UI:
		self.setTitle(title)
		self.finished_init = False
		self.resized = False
		self.container_ui = ContainerElements.Ui_ContainerElements()
		self.container_ui.setupUi(self)
		self.container_ui.abort_button.setVisible(False)
		self.container_ui.abort_plot_button.setVisible(False)
		self.ui: Union[sim_ui, int_ui, fft_ui, osc_ui, ons_ui, cyc_ui, ang_ui, din_ui, fil_ui] = ui_elements()
		self.ui.setupUi(self.container_ui.ui_scroll_area)
		self.error = QtWidgets.QErrorMessage(self)
		self.plot_widget: PlotCanvas = plot_canvas(self.container_ui.plot_widget)
		self.initializePlot()
		self.initializeButtons()
		self.initializeSpinBoxes()

		# Check if page's UI is the widest, if so save its width.
		if self.container_ui.ui_scroll_area.sizeHint().width() > MagneticOscillationPage.max_size:
			MagneticOscillationPage.max_size = self.container_ui.ui_scroll_area.sizeHint().width()

	# Initialisation methods:
	def initializePage(self):
		"""
		Calls all initialisation methods in the class and sets the currently displayed set.

		Notes
		-----
		Called automatically when the page is first displayed.
		"""

		self.finished_init = False
		self.initializeChangeSet()
		self.initializeFieldValues()
		self.initializePageElements()
		self.toggleDefaultStates()
		self.resizeScrollArea()
		self.finished_init = True
		self.renderUI()

	def initializeFieldValues(self):
		pass

	def initializeButtons(self):
		"""
		Connect button methods to their buttons.
		"""

		# Data buttons:
		self.ui.save_data_button.clicked.connect(self.buttonSaveData)
		self.ui.save_all_data_button.clicked.connect(self.buttonSaveAllData)

		# Plot buttons:
		self.container_ui.plot_button.clicked.connect(self.renderUI)
		self.ui.plot_lines.clicked.connect(self.buttonPlotLines)
		self.ui.field_legend.currentIndexChanged.connect(self.buttonLegend)
		self.ui.plot_all.clicked.connect(self.buttonPlotAllFits)
		self.ui.auto_update.clicked.connect(self.autoUpdate)
		self.ui.field_change_set.currentIndexChanged.connect(self.changeSet)
		self.container_ui.abort_plot_button.clicked.connect(self.plot_widget.plot_thread.cancel)

	def initializePageElements(self):
		pass

	def initializePlot(self):
		"""
		Initialise the plot widget.
		"""

		toolbar = NavigationToolBar(self.plot_widget, self)
		self.container_ui.plot.addWidget(toolbar)
		self.container_ui.plot.addWidget(self.plot_widget)

	def initializeSpinBoxes(self):
		"""
		Set default values for all spinboxes
		"""

		# Extract all ScientificSpinBox and ScientificDoubleSpinBox types and set default behaviour:
		for name in dir(self.ui):
			if type(getattr(self.ui, name)).__name__ in ["ScientificSpinBox", "ScientificDoubleSpinBox"]:
				spinbox = getattr(self.ui, name)
				spinbox.setKeyboardTracking(False)
				spinbox.setCorrectionMode(QtWidgets.QAbstractSpinBox.CorrectToNearestValue)
				spinbox.valueChanged.connect(self.autoUpdate)
				self.spinboxes += [spinbox]

	def initializeChangeSet(self):
		"""
		Adds each data set to the drop down menu so they can be selected.
		"""

		# Create a list of all pocket types that should be added to change_set:
		names: List[str] = []
		for mo_data in self.data.mo_data_list:
			if self.data.mode == "temperature":
				names += [str(mo_data.temperature) + "[K] - " + mo_data.name]
			else:
				names += [str(mo_data.angle) + " - " + mo_data.name]

		# Add the names to the UI:
		self.ui.field_change_set.blockSignals(True)
		self.ui.field_change_set.clear()
		self.ui.field_change_set.addItems(names)
		self.ui.field_change_set.blockSignals(False)

# Validator methods:
	def validatePage(self) -> bool:
		return self.validateFields()

	def validateFields(self) -> bool:
		return True

# Data methods:
	def saveData(self, filename):
		pass

	def saveAllData(self, foldername):
		pass

	def updateData(self):
		pass

	def updateAllData(self):
		pass

# UI methods:
	def autoUpdate(self):
		"""
		Updates the UI if auto updating is selected.
		"""

		MagneticOscillationPage.auto_update = self.ui.auto_update.isChecked()
		if self.ui.auto_update.isChecked():
			if self.validateFields():
				self.renderUI()

	def renderUI(self):
		"""
		Render the UI.
		"""

		if not self.finished_init:
			return

		if self.validateFields():
			self.plot()

	def plot(self, alternative: bool = False):
		"""
		Updates the on-screen plot.

		Notes
		-----
		Assumes the following exist:
		self.ui.plot_lines, self.ui.plot_all, self.ui.field_offset, self.ui.field_legend
		"""

		# prevent multiple plots running at the same time:
		if self.is_plotting:
			return
		self.is_plotting = True

		multi = self.ui.plot_all.isChecked()

		if multi:
			self.enableUI(False)
			self.container_ui.plot_button.setVisible(False)
			self.container_ui.abort_plot_button.setVisible(True)

		QCoreApplication.processEvents()  # Force the UI to update.

		marker_style = self.ui.plot_lines.isChecked()
		offset = self.ui.field_offset.value()
		legend = self.ui.field_legend.currentText()

		if alternative:
			self.plot_widget.drawAlternativePlot(self.data, marker_style, multi, offset, legend)
		else:
			self.plot_widget.drawMainPlot(self.data, marker_style, multi, offset, legend)

		if multi:
			self.enableUI(True)
			self.container_ui.plot_button.setVisible(True)
			self.container_ui.abort_plot_button.setVisible(False)

		self.is_plotting = False

	def changeSet(self):
		pass

	def resizeScrollArea(self):
		"""
		Resize the scroll area so it's consistent between pages.
		"""

		if self.resized:
			return
		# Resize the scroll area to fit the contents:
		self.container_ui.ui_scroll_area.adjustSize()
		self.container_ui.ui_scroll_area.resize(self.container_ui.ui_scroll_area.sizeHint())
		# Set the scroll area's maximum width equal to that of the scroll area plus spacing:
		width = (
			MagneticOscillationPage.max_size + 2 * self.container_ui.scrollArea.frameWidth() +
			self.container_ui.scrollArea.verticalScrollBar().width())
		self.container_ui.plot_button.setMinimumWidth(width)
		self.container_ui.abort_button.setMinimumWidth(width)
		self.container_ui.abort_plot_button.setMinimumWidth(width)
		self.container_ui.scrollArea.setMaximumWidth(width)
		self.resized = True

	def toggleDefaultStates(self):
		"""
		Toggles the offset state based on the plot all state.
		"""

		# Set default state:
		self.ui.auto_update.blockSignals(True)
		self.ui.auto_update.setChecked(MagneticOscillationPage.auto_update)
		self.ui.auto_update.blockSignals(False)

		self.ui.plot_lines.blockSignals(True)
		self.ui.plot_lines.setChecked(MagneticOscillationPage.plot_lines)
		self.ui.plot_lines.blockSignals(False)

		# If the user has enabled the legend, overwrite the default legend state:
		if MagneticOscillationPage.legend_style != 'None':
			self.ui.field_legend.blockSignals(True)
			self.ui.field_legend.setCurrentIndex(MagneticOscillationPage.legend_styles[MagneticOscillationPage.legend_style])
			self.ui.field_legend.blockSignals(False)

		self.ui.plot_all.blockSignals(True)
		if self.ui.plot_all.isChecked():
			self.ui.field_offset.setEnabled(True)
			self.ui.label_offset.setEnabled(True)
		else:
			self.ui.field_offset.setDisabled(True)
			self.ui.label_offset.setDisabled(True)
		self.ui.plot_all.blockSignals(False)

	def enableUI(self, state: bool):
		"""
		Toggles the UI availability.
		"""

		# Don't do anything if we're already in the requested state:
		if self.container_ui.ui_scroll_area.isEnabled() == state:
			return

		# When disabling, save the focus element:
		if not state:
			self.old_focus = self.focusWidget()

		# Toggle UI element state
		MagneticOscillationPage.back_button_enabled(state)
		MagneticOscillationPage.next_button_enabled(state)
		self.container_ui.ui_scroll_area.setEnabled(state)

		# When enabling, restore the focus element:
		if state and self.old_focus is not None:
			self.old_focus.setFocus()

# Button methods:
	def buttonSaveData(self):
		"""
		Saves data to user-selected text file.
		"""

		if not self.validateFields():
			return

		self.updateData()

		filename = QtWidgets.QFileDialog.getSaveFileName(
			caption="Save data", filter="Text files (*.txt)")[0]

		if filename:
			self.saveData(filename)

	def buttonSaveAllData(self):
		"""
		Opens QFileDialog and returns user selected folder.

		Returns
		-------
		foldername : str

		Notes
		-----
		foldername is empty if user did not select any files.
		"""

		if not self.validateFields():
			return

		self.updateAllData()

		foldername = str(QtWidgets.QFileDialog.getExistingDirectory(self, "Select Directory"))

		if foldername:
			self.saveAllData(foldername)

	def buttonPlotLines(self):
		"""
		Toggles lines when plot lines is toggled.
		"""

		MagneticOscillationPage.plot_lines = self.ui.plot_lines.isChecked()
		self.renderUI()

	def buttonPlotAllFits(self):
		"""
		Toggles offset field when plot all is toggled.
		"""

		self.ui.plot_all.blockSignals(True)
		self.ui.field_legend.blockSignals(True)
		if self.ui.plot_all.isChecked():
			self.ui.field_offset.setEnabled(True)
			self.ui.label_offset.setEnabled(True)
			if self.ui.field_legend.currentText() == 'None':
				self.ui.field_legend.setCurrentIndex(
					MagneticOscillationPage.legend_styles['Boxed'])
		else:
			self.ui.field_offset.setDisabled(True)
			self.ui.label_offset.setDisabled(True)
			if self.ui.field_legend.currentText() == 'Boxed':
				self.ui.field_legend.setCurrentIndex(
					MagneticOscillationPage.legend_styles['None'])
		self.ui.plot_all.blockSignals(False)
		self.ui.field_legend.blockSignals(False)

		self.renderUI()

	def buttonLegend(self):
		"""
		Saves currently selected legend style.
		"""

		MagneticOscillationPage.legend_style = self.ui.field_legend.currentText()
		self.renderUI()
