# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'ResultsPageElements.ui'
#
# Created by: PyQt5 UI code generator 5.12.2
#
# WARNING! All changes made in this file will be lost!

from qtpy import QtCore, QtGui, QtWidgets


class Ui_ResultsElements(object):
    def setupUi(self, ResultsElements):
        ResultsElements.setObjectName("ResultsElements")
        ResultsElements.resize(921, 523)
        sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Minimum, QtWidgets.QSizePolicy.Minimum)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(ResultsElements.sizePolicy().hasHeightForWidth())
        ResultsElements.setSizePolicy(sizePolicy)
        ResultsElements.setMinimumSize(QtCore.QSize(520, 200))
        ResultsElements.setBaseSize(QtCore.QSize(800, 600))
        self.horizontalLayout = QtWidgets.QHBoxLayout(ResultsElements)
        self.horizontalLayout.setObjectName("horizontalLayout")
        self.tableWidget = QtWidgets.QTableWidget(ResultsElements)
        self.tableWidget.setObjectName("tableWidget")
        self.tableWidget.setColumnCount(6)
        self.tableWidget.setRowCount(0)
        item = QtWidgets.QTableWidgetItem()
        self.tableWidget.setHorizontalHeaderItem(0, item)
        item = QtWidgets.QTableWidgetItem()
        self.tableWidget.setHorizontalHeaderItem(1, item)
        item = QtWidgets.QTableWidgetItem()
        self.tableWidget.setHorizontalHeaderItem(2, item)
        item = QtWidgets.QTableWidgetItem()
        self.tableWidget.setHorizontalHeaderItem(3, item)
        item = QtWidgets.QTableWidgetItem()
        self.tableWidget.setHorizontalHeaderItem(4, item)
        item = QtWidgets.QTableWidgetItem()
        self.tableWidget.setHorizontalHeaderItem(5, item)
        self.tableWidget.horizontalHeader().setCascadingSectionResizes(False)
        self.tableWidget.horizontalHeader().setDefaultSectionSize(120)
        self.tableWidget.horizontalHeader().setHighlightSections(False)
        self.tableWidget.horizontalHeader().setMinimumSectionSize(0)
        self.tableWidget.verticalHeader().setVisible(False)
        self.tableWidget.verticalHeader().setHighlightSections(False)
        self.horizontalLayout.addWidget(self.tableWidget)
        self.scrollArea = QtWidgets.QScrollArea(ResultsElements)
        sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Maximum, QtWidgets.QSizePolicy.Expanding)
        sizePolicy.setHorizontalStretch(1)
        sizePolicy.setVerticalStretch(1)
        sizePolicy.setHeightForWidth(self.scrollArea.sizePolicy().hasHeightForWidth())
        self.scrollArea.setSizePolicy(sizePolicy)
        self.scrollArea.setFrameShape(QtWidgets.QFrame.NoFrame)
        self.scrollArea.setWidgetResizable(True)
        self.scrollArea.setAlignment(QtCore.Qt.AlignRight|QtCore.Qt.AlignTrailing|QtCore.Qt.AlignVCenter)
        self.scrollArea.setObjectName("scrollArea")
        self.scrollAreaWidgetContents = QtWidgets.QWidget()
        self.scrollAreaWidgetContents.setGeometry(QtCore.QRect(0, 0, 98, 511))
        self.scrollAreaWidgetContents.setObjectName("scrollAreaWidgetContents")
        self.verticalLayout_2 = QtWidgets.QVBoxLayout(self.scrollAreaWidgetContents)
        self.verticalLayout_2.setObjectName("verticalLayout_2")
        self.verticalLayout = QtWidgets.QVBoxLayout()
        self.verticalLayout.setObjectName("verticalLayout")
        self.button_save = QtWidgets.QPushButton(self.scrollAreaWidgetContents)
        self.button_save.setObjectName("button_save")
        self.verticalLayout.addWidget(self.button_save)
        spacerItem = QtWidgets.QSpacerItem(10, 1, QtWidgets.QSizePolicy.Minimum, QtWidgets.QSizePolicy.Expanding)
        self.verticalLayout.addItem(spacerItem)
        self.verticalLayout_2.addLayout(self.verticalLayout)
        self.scrollArea.setWidget(self.scrollAreaWidgetContents)
        self.horizontalLayout.addWidget(self.scrollArea)

        self.retranslateUi(ResultsElements)
        QtCore.QMetaObject.connectSlotsByName(ResultsElements)

    def retranslateUi(self, ResultsElements):
        _translate = QtCore.QCoreApplication.translate
        ResultsElements.setWindowTitle(_translate("ResultsElements", "Magnetic Oscillations Analyser"))
        item = self.tableWidget.horizontalHeaderItem(0)
        item.setText(_translate("ResultsElements", "Data Set"))
        item = self.tableWidget.horizontalHeaderItem(1)
        item.setText(_translate("ResultsElements", "Frequency [T]"))
        item = self.tableWidget.horizontalHeaderItem(2)
        item.setText(_translate("ResultsElements", "Pocket"))
        item = self.tableWidget.horizontalHeaderItem(3)
        item.setText(_translate("ResultsElements", "Cyclo. Mass [m_e]"))
        item = self.tableWidget.horizontalHeaderItem(4)
        item.setText(_translate("ResultsElements", "Dingle Temp. [K]"))
        item = self.tableWidget.horizontalHeaderItem(5)
        item.setText(_translate("ResultsElements", "Onset Field [T]"))
        self.button_save.setText(_translate("ResultsElements", "Save Table"))


