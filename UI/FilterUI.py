from qtpy.QtCore import QThread, Qt
from UI.PageElements import FilterForm
from Backend.SimulationFunctions.FilterFunctions import FIRFiltering
from UI.PlotUI import PlotMagneticField
from UI.SharedUI import MagneticOscillationPage
from math import floor, ceil, log10
from typing import List, Callable, Any, Optional, Union, Tuple, Dict
from Backend.DataObjects.DataWrapper import DataWrapper
from Backend.DataObjects.MOData import MOData
import UI.InitUI
from qtpy import QtWidgets


class PlotFilter(PlotMagneticField):
	"""
	Implements plotting for the Filter page.
	"""

	def drawMagneticFieldPlot(self, mo_data: MOData, lines: bool = False, legend_style: str = 'None'):
		"""
		Plot as function of the magnetic field.

		Notes
		-----
		If lines is True, plot lines rather than markers.

		legend_style is 'Annotated', 'Boxed' or 'None'.
		"""

		self.addMagneticFieldPlot("Inverse Field [1/T]", "Amplitude", 0, legend_style)

		annotation = str(mo_data.temperature) + '[K]'
		self.addLine(
			1/mo_data.interpolation_inverse_magnetic_field, mo_data.interpolation_amplitudes, lines, label=annotation)

		self.plot_thread.run()

	def drawInverseMagneticFieldPlot(self, mo_data: MOData, lines: bool = False, legend_style: str = 'None'):
		"""
		Plot as function of the inverse magnetic field.

		Notes
		-----
		If lines is True, plot lines rather than markers.

		legend_style is 'Annotated', 'Boxed' or 'None'.
		"""

		self.addMagneticFieldPlot("Inverse Field [1/T]", "Amplitude", 0, legend_style)

		annotation = str(mo_data.temperature) + '[K]'
		self.addLine(
			1/mo_data.interpolation_inverse_magnetic_field, mo_data.interpolation_amplitudes, lines, label=annotation)

		self.plot_thread.run()


class FilterCalculation(QThread):
	"""
	QThread child class that handles running the cyclotron mass calculation on its own thread.
	"""

	def __init__(self):
		QThread.__init__(self)
		self.mo_data: Optional[MOData] = None
		self.order: Optional[int] = None
		self.cutoffs: Optional[Union[float, Tuple[float, float]]] = None
		self.trans_width: Optional[float] = None
		self.band_type: Optional[str] = None
		self.algorithm: Optional[str] = None
		self.window: Optional[Union[str, Tuple]] = None
		self.max_order: Optional[int] = None
		self.auto_order: Optional[bool] = None
		self.filtered_data: Optional[MOData] = None
		self.canceled: bool = False
		self.error: bool = False

	def run(self):
		"""
		Method that is called when FilterCalculation.start() is executed.

		Notes
		-----
		Uses Backend.SimulationFunctions.FilterFunctions
		"""

		self.error = False
		self.canceled = False
		try:
			self.filtered_data = FIRFiltering(
				self.mo_data, self.order, self.cutoffs, self.trans_width, self.band_type, self.algorithm, self.window,
				self.max_order, self.auto_order, self.isCanceled
			)
		except:
			self.error = True

	def getResults(self) -> MOData:
		return self.filtered_data

	def errorEncountered(self) -> bool:
		"""
		Returns true if the calculation thread encountered an error.
		"""

		return self.error

	def isCanceled(self) -> bool:
		"""
		Returns whether the calculation is canceled.
		"""

		return self.canceled

	def cancel(self):
		"""
		Cancels the calculation when called.
		"""

		self.canceled = True

	def setParameters(
			self, mo_data: List[MOData], order: int, cutoffs: Union[float, Tuple[float, float]], trans_width: float,
			band_type: str, algorithm: str, window: Union[str, Tuple], max_order: int, auto_order: bool
		):
		"""
		Method that sets the parameters used by the filter calculation.
		"""

		self.mo_data = mo_data
		self.order = order
		self.cutoffs = cutoffs
		self.trans_width = trans_width
		self.band_type = band_type
		self.algorithm = algorithm
		self.window = window
		self.max_order = max_order
		self.auto_order = auto_order


class FilterPage(MagneticOscillationPage):
	"""
	Filter page class.

	Notes
	-----
	On the filter page users can use several filter types to filter out frequencies.
	"""

	# Dicts to look up types for backend.
	fir_algorithms = ['firwin2', 'firls', 'remez']

	def __init__(self, data: DataWrapper):
		super().__init__(data, FilterForm.Ui_Form, PlotFilter, "Signal Filtering")

		# initialize variables:
		self.run: bool = True
		self.data_set: Optional[str] = None
		self.band_type: Optional[str] = None
		self.fft_min: Optional[float] = None
		self.fft_max: Optional[float] = None
		self.trans_width: Optional[float] = None
		self.filter_order: Optional[int] = None
		self.fir_algorithm: Optional[str] = None
		self.window: Optional[str] = None
		self.beta: Optional[float] = None
		self.auto_filter_order: Optional[bool] = None
		self.filter_max_order: Optional[int] = None
		self.callback: Optional[Callable[[None], ...]] = None
		self.filtered_data: Optional[MOData] = None
		self.cutoffs: Optional[Union[float, Tuple[float, float]]] = None
		self.pockets: Dict[str, int] = {}

		# Set up threading:
		self.calculation_thread: QThread = FilterCalculation()
		self.calculation_thread.finished.connect(self.finishFilterCalculation)

	# Initialisation methods:
	def initializePage(self):
		"""
		Calls all initialisation methods in the class and sets the currently displayed set.

		Notes
		-----
		Called automatically when the page is first displayed.
		"""

		self.run = True
		self.container_ui.plot_button.setText("Calculate")
		self.filtered_data = None
		self.data.mo_data.filtered_data_list = []

		# Show "Calculating" during initial plot:
		self.plot_widget.addPlot(
			"Inverse magnetic field [1/T]", "Amplitude")
		# Draw a single white dot as we need to draw something
		self.plot_widget.addLine(0, 0, False, style='w*')
		self.plot_widget.showPlots()

		self.finished_init = False
		self.initializeChangeSet()
		self.initializeFieldValues()
		self.initializePageElements()
		self.initializePockets()
		self.initializeTable()
		self.toggleDefaultStates()
		self.resizeScrollArea()
		self.finished_init = True

		self.toggleBandTypeOptions()
		self.toggleAlgorithmOptions()
		self.toggleFilteredDataButtons()
		self.changePocket()
		self.renderUI()

	def initializeFieldValues(self):
		"""
		Initialises all fields in the class with data from the currently selected MOData object.
		"""

		# Set ranges:
		min_freq = self.data.mo_data.fft_full_frequency_range[0]
		max_freq = self.data.mo_data.fft_full_frequency_range[-1]
		self.ui.field_min_freq.setRange(min_freq, max_freq)
		self.ui.field_max_freq.setRange(min_freq, max_freq)
		self.ui.field_trans_width.setMinimum(0.0000000001)
		self.ui.field_order.setRange(2, 1e7)
		self.ui.field_beta.setMinimum(0.0000000001)
		self.ui.field_max_order.setRange(2, 1e7)

		# Set default values:
		fft_min, fft_max = self.data.mo_data.fft_frequency_limits()
		self.ui.field_min_freq.setValue(floor(fft_min))
		self.ui.field_max_freq.setValue(10**ceil(log10(fft_max)))
		self.ui.field_trans_width.setValue(10)
		order = 10 ** (floor(log10(self.data.mo_data.interpolation_steps) - 1))
		self.ui.field_order.setValue(order)
		self.ui.field_beta.setValue(14)
		self.ui.field_max_order.setValue(20000 if order < 20000 else order)

	def initializeButtons(self):
		"""
		Connect button methods to their buttons.
		"""

		# Data buttons:
		self.ui.save_data_button.clicked.connect(self.buttonSaveData)
		self.ui.save_all_data_button.clicked.connect(self.buttonSaveAllData)
		self.ui.field_change_pocket.currentIndexChanged.connect(self.changePocket)

		# Plot buttons:
		self.container_ui.plot_button.clicked.connect(self.renderUI)
		self.ui.plot_lines.clicked.connect(self.buttonPlotLines)
		self.ui.field_legend.currentIndexChanged.connect(self.buttonLegend)
		self.ui.auto_update.clicked.connect(self.autoUpdate)
		self.ui.field_change_set.currentIndexChanged.connect(self.changeSet)
		self.container_ui.abort_plot_button.clicked.connect(self.plot_widget.plot_thread.cancel)

		# Filter buttons:
		self.ui.field_band_type.currentIndexChanged.connect(self.toggleBandTypeOptions)
		self.ui.field_algorithm.currentIndexChanged.connect(self.toggleAlgorithmOptions)
		self.ui.field_window.currentIndexChanged.connect(self.toggleAlgorithmOptions)
		self.ui.auto_order.clicked.connect(self.toggleAutoOrderOptions)
		self.ui.remove_filtered_data_button.clicked.connect(self.buttonRemoveFilteredData)

	def initializeChangeSet(self):
		"""
		Adds each identified pocket to the drop down menu so they can be selected.
		"""

		# Create a list of all pocket types that should be added to change_set:
		names: List[str] = []
		_ = 0
		for lkf_parameters_list in self.data.list_of_lkf_parameters_lists:
			add = False
			for lkf_parameters in lkf_parameters_list:
				if lkf_parameters.active:
					add = True
			if add:
				names += [self.data.mo_data_list[_].name]
			_ += 1

		# Add the data sets to the UI and (re)connect the plot function to change_set.
		self.ui.field_change_set.blockSignals(True)
		self.ui.field_change_set.clear()
		self.ui.field_change_set.addItems(names)
		self.ui.field_change_set.blockSignals(False)
		self.data.index = 0

	def initializeTable(self):
		"""
		Generate a table containing each filtered data in the currently selected data set.
		"""

		self.ui.tableWidget.blockSignals(True)
		self.ui.tableWidget.clearContents()

		length = len(self.data.mo_data.filtered_data_list)
		self.ui.tableWidget.setRowCount(length)
		self.ui.tableWidget.setAlternatingRowColors(True)

		# Create a QTableWidgetItem for each filtered data and add it to the table:
		for _ in range(length):
			# Create a QTableWidgetItem:
			pocket_item = QtWidgets.QTableWidgetItem()
			band_type_item = QtWidgets.QTableWidgetItem()
			cutoffs_item = QtWidgets.QTableWidgetItem()
			algorithm_item = QtWidgets.QTableWidgetItem()
			filter_order_item = QtWidgets.QTableWidgetItem()

			pocket_item.setFlags(Qt.ItemIsSelectable | Qt.ItemIsEnabled)
			band_type_item.setFlags(Qt.ItemIsSelectable | Qt.ItemIsEnabled)
			cutoffs_item.setFlags(Qt.ItemIsSelectable | Qt.ItemIsEnabled)
			algorithm_item.setFlags(Qt.ItemIsSelectable | Qt.ItemIsEnabled)
			filter_order_item.setFlags(Qt.ItemIsSelectable | Qt.ItemIsEnabled)

			# Set the text:

			pocket, band_type, cutoffs, algorithm, filter_order = self.data.mo_data.filtered_data_list[_].table_values

			pocket_item.setText(pocket)
			band_type_item.setText(band_type)
			cutoffs_item.setText(cutoffs)
			algorithm_item.setText(algorithm)
			filter_order_item.setText(filter_order)

			# Add items to table:
			self.ui.tableWidget.setItem(_, 0, pocket_item)
			self.ui.tableWidget.setItem(_, 1, band_type_item)
			self.ui.tableWidget.setItem(_, 2, cutoffs_item)
			self.ui.tableWidget.setItem(_, 3, algorithm_item)
			self.ui.tableWidget.setItem(_, 4, filter_order_item)

		# Resize table headers to fit width:
		self.ui.tableWidget.horizontalHeader().setSectionResizeMode(QtWidgets.QHeaderView.Stretch)
		self.ui.tableWidget.blockSignals(False)

	def initializePockets(self):
		"""
		Adds each identified pocket to the drop down menu so they can be selected.
		"""

		# Create a list of all pocket types that should be added to change_set:
		self.pockets: Dict[int] = {}
		pocket_types: List[str] = []
		_ = 0
		for lkf_parameters in self.data.lkf_parameters_list:
			if lkf_parameters.active is False:
				_ += 1
				continue
			pocket_type = lkf_parameters.pocket_type + " (" + str(round(lkf_parameters.frequency, 2)) + " [T])"
			if pocket_type not in self.pockets and pocket_type is not None:
				self.pockets[pocket_type] = _
				pocket_types += [pocket_type]
			_ += 1

		# Add the pockets to the UI and (re)connect the plot function to change_set.
		self.ui.field_change_pocket.setEnabled(True)
		self.ui.label_change_pocket.setEnabled(True)
		self.ui.field_change_pocket.blockSignals(True)
		self.ui.field_change_pocket.clear()
		self.ui.field_change_pocket.addItems(pocket_types)
		self.ui.field_change_pocket.blockSignals(False)

	# Validator methods:
	def validatePage(self) -> bool:
		if not super().validateFields():
			return False
		for mo_data in self.data.mo_data_list:
			if len(mo_data.filtered_data_list) != 0:
				return True
		self.error.showMessage("Save at least one filtered data set to continue.")
		return False

	def validateFields(self) -> bool:
		"""
		Validate each field in the page and return true if all are acceptable.
		"""

		# check that fft min frequency is lower than the max frequency:
		if (
			self.ui.field_band_type.currentText() in ["bandpass", "bandstop"] and
			self.ui.field_min_freq.value() >= self.ui.field_max_freq.value() - self.data.mo_data.fft_bin_size
			):
			self.ui.field_min_freq.setStyleSheet('QDoubleSpinBox { background-color: #da4453}')
			self.ui.field_max_freq.setStyleSheet('QDoubleSpinBox { background-color: #da4453}')
			return False
		else:
			self.ui.field_min_freq.setStyleSheet('')
			self.ui.field_max_freq.setStyleSheet('')

		if self.ui.field_band_type.currentText() == "highpass":
			if self.ui.field_min_freq.value() == 0:
				self.ui.field_min_freq.setStyleSheet('QDoubleSpinBox { background-color: #da4453}')
				return False
			else:
				self.ui.field_min_freq.setStyleSheet('')
		if self.ui.field_band_type.currentText() in ["highpass", "bandpass", "bandstop"]:
			if self.ui.field_min_freq.value() - self.ui.field_trans_width.value() <= 0:
				self.ui.field_min_freq.setStyleSheet('QDoubleSpinBox { background-color: #da4453}')
				self.ui.field_trans_width.setStyleSheet('QDoubleSpinBox { background-color: #da4453}')
				return False
			else:
				self.ui.field_trans_width.setStyleSheet('')
				self.ui.field_min_freq.setStyleSheet('')

		return True

	def nextId(self) -> int:
		"""
		Decide which Wizard page to load next based on page state.

		Notes
		-----
		nextId() is an internal PyQt method that can be reimplemented to change which QWizard page is loaded next.
		"""

		return UI.InitUI.Wizard.dingle_calculation

# Data methods:
	def startFilterCalculation(self, custom_callback: Callable[[None], Any] = None):
		"""
		Sets up and starts the filter calculation.

		Notes
		-----
		custom_callback is the function to call when calculation is finished.
		"""

		data_set = self.ui.field_change_set.currentText()
		if self.data_set != data_set:
			self.data_set = data_set
			self.run = True

		band_type = self.ui.field_band_type.currentText()
		if self.band_type != band_type:
			self.band_type = band_type
			self.run = True

		fft_min = self.ui.field_min_freq.value()
		if self.fft_min != fft_min:
			self.fft_min = fft_min
			self.run = True

		fft_max = self.ui.field_max_freq.value()
		if self.fft_max != fft_max:
			self.fft_max = fft_max
			self.run = True

		trans_width = self.ui.field_trans_width.value()
		if self.trans_width != trans_width:
			self.trans_width = trans_width
			self.run = True

		filter_order = self.ui.field_order.value()
		if self.filter_order != filter_order:
			self.filter_order = filter_order
			self.run = True

		fir_algorithm = FilterPage.fir_algorithms[self.ui.field_algorithm.currentIndex()]
		if self.fir_algorithm != fir_algorithm:
			self.fir_algorithm = fir_algorithm
			self.run = True

		window = self.ui.field_window.currentText()
		beta = self.ui.field_beta.value()
		if self.window != window:
			self.window = window if window != "kaiser" else (window, beta)
			self.run = True

		if self.window == "kaiser" and self.beta != beta:
			self.beta = beta
			self.window = (window, beta)
			self.run = True

		auto_filter_order = self.ui.auto_order.isChecked()
		if self.auto_filter_order is auto_filter_order:
			self.auto_filter_order = auto_filter_order
			self.run = True

		filter_max_order = self.ui.field_max_order.value()
		if self.filter_max_order != filter_max_order:
			self.filter_max_order = filter_max_order
			self.run = True

		if custom_callback is None:
			self.callback = self.finishRenderUI
		else:
			self.callback = custom_callback

		if not self.run:
			self.callback()
		else:
			self.setCutoffs()
			self.calculation_thread.setParameters(
				self.data.mo_data, self.filter_order, self.cutoffs, self.trans_width, self.band_type, self.fir_algorithm,
				self.window, self.filter_max_order, self.auto_filter_order
			)
			self.container_ui.abort_button.setVisible(True)
			self.container_ui.plot_button.setVisible(False)
			self.enableUI(False)
			self.calculation_thread.start()

	def terminateFilterCalculation(self):
		"""
		Terminates the filter calculation.
		"""

		self.calculation_thread.cancel()
		self.calculation_thread.quit()
		self.calculation_thread.wait(1000)
		self.container_ui.abort_button.setVisible(False)
		self.container_ui.plot_button.setVisible(True)
		self.enableUI(True)

	def finishFilterCalculation(self):
		"""
		Processes the result of the filter calculation and updates the UI.

		Notes
		-----
		Is called automatically by the filter calculation thread when it finishes its calculations.
		"""

		self.container_ui.abort_button.setVisible(False)
		self.container_ui.plot_button.setVisible(True)

		self.enableUI(True)
		self.callback()
		self.run = False

	def finishRenderUI(self):
		"""
		Update the UI after the filter has been calculated.

		Notes
		-----
		Called automatically by the filter calculation thread.
		"""

		if self.calculation_thread.errorEncountered():
			self.error.showMessage("An error was encountered during the calculation.")
			self.run = False
			return

		self.filtered_data = self.calculation_thread.getResults()

		self.filtered_data.table_values = {}

		self.filtered_data.lkf_parameters = self.data.lkf_parameters_list[
			self.pockets[self.ui.field_change_pocket.currentText()]
		]

		# if the pocket already has filtered_data, overwrite it:
		_ = 0
		while _ < len(self.data.mo_data.filtered_data_list):
			if (
				self.filtered_data.lkf_parameters.pocket_type
				==
				self.data.mo_data.filtered_data_list[_].lkf_parameters.pocket_type
			):
				self.data.mo_data.filtered_data_list[_] = self.filtered_data
				break
			_ += 1
		if _ == len(self.data.mo_data.filtered_data_list):
			self.data.mo_data.filtered_data_list += [self.filtered_data]

		self.addTableRow()
		self.toggleFilteredDataButtons()
		self.plot()

	def setCutoffs(self):
		"""
		Set the cutoffs depending on band_type.
		"""

		if self.ui.field_band_type.currentText() == "lowpass":
			self.cutoffs = self.ui.field_max_freq.value()
		elif self.ui.field_band_type.currentText() == "highpass":
			self.cutoffs = self.ui.field_min_freq.value()
		elif self.ui.field_band_type.currentText() in ["bandpass", "bandstop"]:
			self.cutoffs = [self.ui.field_min_freq.value(), self.ui.field_max_freq.value()]

# UI methods:
	def plot(self, alternative: bool = False):
		"""
		Updates the on-screen plot.
		"""

		# prevent multiple plots running at the same time:
		if self.is_plotting:
			return
		self.is_plotting = True

		self.enableUI(False)
		self.container_ui.plot_button.setVisible(False)
		self.container_ui.abort_plot_button.setVisible(True)

		marker_style = self.ui.plot_lines.isChecked()
		legend = self.ui.field_legend.currentText()

		if self.ui.toggle_alternative_plot.isChecked():
			self.plot_widget.drawInverseMagneticFieldPlot(self.filtered_data, marker_style, legend)
		else:
			self.plot_widget.drawMagneticFieldPlot(self.filtered_data, marker_style, legend)

		self.enableUI(True)
		self.container_ui.plot_button.setVisible(True)
		self.container_ui.abort_plot_button.setVisible(False)

		self.is_plotting = False

	def renderUI(self):
		"""
		Apply the filter and update the UI afterwards.

		Notes
		-----
		The actual rendering of the UI is performed by finishRenderUI(),
		which is called automatically when the filter calculation is finished.
		"""

		if not self.finished_init:
			return

		if not self.validateFields():
			return
		self.startFilterCalculation()

	def enableUI(self, state):
		"""
		Toggles the UI elements so that the UI becomes inactive (except for the abort button) when calculating.
		"""

		super().enableUI(state)
		if state and not self.container_ui.ui_scroll_area.isEnabled():
			self.toggleAutoOrderOptions()
			self.toggleAlgorithmOptions()

	def toggleDefaultStates(self):
		"""
		Sets the default toggle states.
		"""

		# Set default state:
		self.ui.auto_update.blockSignals(True)
		self.ui.auto_update.setChecked(False)
		self.ui.auto_update.blockSignals(False)

		self.ui.plot_lines.blockSignals(True)
		self.ui.plot_lines.setChecked(MagneticOscillationPage.plot_lines)
		self.ui.plot_lines.blockSignals(False)

		# If the user has enabled the legend, overwrite the default legend state:
		if MagneticOscillationPage.legend_style != 'None':
			self.ui.field_legend.blockSignals(True)
			self.ui.field_legend.setCurrentIndex(MagneticOscillationPage.legend_styles[MagneticOscillationPage.legend_style])
			self.ui.field_legend.blockSignals(False)

	def toggleAutoOrderOptions(self):
		"""
		Toggles UI elements when auto_order is toggled.
		"""

		if self.ui.auto_order.isChecked():
			self.ui.field_max_order.setEnabled(True)
		else:
			self.ui.field_max_order.setDisabled(True)
		self.autoUpdate()

	def toggleAlgorithmOptions(self):
		"""
		Toggles UI elements when the FIR algorithm is changed.
		"""

		if self.fir_algorithms[self.ui.field_algorithm.currentIndex()] == "firwin2":
			self.ui.field_window.setEnabled(True)
			if self.ui.field_window.currentText() == "kaiser":
				self.ui.field_beta.setEnabled(True)
			else:
				self.ui.field_beta.setDisabled(True)
		else:
			self.ui.field_window.setDisabled(True)
			self.ui.field_beta.setDisabled(True)
		self.autoUpdate()

	def toggleBandTypeOptions(self):
		"""
		Toggles UI elements when the band type is changed.
		"""

		if self.ui.field_band_type.currentText() == "lowpass":
			self.ui.field_min_freq.setDisabled(True)
			self.ui.field_max_freq.setEnabled(True)
		elif self.ui.field_band_type.currentText() == "highpass":
			self.ui.field_min_freq.setEnabled(True)
			self.ui.field_max_freq.setDisabled(True)
		else:
			self.ui.field_min_freq.setEnabled(True)
			self.ui.field_max_freq.setEnabled(True)
		self.autoUpdate()

	def toggleFilteredDataButtons(self):
		"""
		Toggle the state of the buttons in the filtered data box.
		"""

		if len(self.data.mo_data.filtered_data_list) == 0:
			self.ui.remove_filtered_data_button.setDisabled(True)
			self.ui.save_data_button.setDisabled(True)
			self.ui.save_all_data_button.setDisabled(True)
		else:
			self.ui.remove_filtered_data_button.setEnabled(True)
			self.ui.save_data_button.setEnabled(True)
			self.ui.save_all_data_button.setEnabled(True)

	def changeSet(self):
		"""
		Called when changing set.
		"""

		self.data.index = self.ui.field_change_set.currentIndex()
		self.filtered_data = None
		self.initializeTable()
		self.initializePockets()
		self.changePocket()

	def changePocket(self):
		"""
		Called when changing pocket.
		"""

		pocket = self.data.lkf_parameters_list[self.pockets[self.ui.field_change_pocket.currentText()]]
		self.finished_init = False
		self.ui.field_band_type.setCurrentIndex(2)
		self.ui.field_min_freq.setValue(round(pocket.frequency-self.ui.field_trans_width.value()/2, 1))
		self.ui.field_max_freq.setValue(round(pocket.frequency+self.ui.field_trans_width.value()/2, 1))
		self.finished_init = True

	def addTableRow(self):
		"""
		Add a row to the filter data table, using the data of the currently selected data set.
		"""

		pocket = self.filtered_data.lkf_parameters.pocket_type
		band_type = self.ui.field_band_type.currentText()

		if band_type in ('bandpass', 'bandstop'):
			cutoffs = self.ui.field_min_freq.text() + " - " + self.ui.field_max_freq.text()
		elif band_type == 'highpass':
			cutoffs = self.ui.field_max_freq.text()
		else:
			cutoffs = self.ui.field_min_freq.text()
		algorithm = self.ui.field_algorithm.currentText()
		if self.ui.auto_order.isChecked():
			filter_order = 'auto'
		else:
			filter_order = self.ui.field_order.text()

		self.filtered_data.table_values = (pocket, band_type, cutoffs, algorithm, filter_order)
		self.initializeTable()

# Button methods:
	def buttonRemoveFilteredData(self):
		"""
		Removes the current filtered data from the list.
		"""

		if len(self.data.mo_data.filtered_data_list) > 0:
			self.data.mo_data.filtered_data_list.pop(self.ui.tableWidget.currentRow())
			self.initializeTable()
			self.toggleFilteredDataButtons()

	def saveData(self, filename: str):
		"""
		Saves data to user-selected text file.
		"""

		self.filtered_data.save_interpolated_data(filename)

	def saveAllData(self, foldername: str):
		"""
		Saves all data sets to user-selected folder.
		"""

		for filtered_data in self.data.mo_data.filtered_data_list:
			filename = str(filtered_data.name) + " filtered data.txt"
			filtered_data.save_interpolated_data(foldername + "/" + filename)
